=====
About
=====

DekaMapper is a GUI based tool for Private Dekaron Server developers created with Visual Studio 2010 C#. Its purpose is to allow monster spawn points to be easily mapped out across the Dekaron maps.

Features
	Development environment adaptability.
        	Reads CSV files to populate map and monster lists. 

	GUI environment:
        	Easily add and move monsters with a simple click of a mouse.
        	
		Display allows custom color coding of monsters for ease of differentiation.
        
		Selected monsters are differentiated by highlighted display.
        	
		Status displaying of current amount of each monster on the map and how many of them are selected.
======
Set Up
======

In order to get DekaMapper working properly you need to either:

	Place it in the share folder of your client or server files (preferrably most updated) 

	OR
	
	Copy these folders/files from the share directory to DekaMapper.exe's directory: creature folder, map folder, maplist.csv

=======
How-to:
=======

---------------------------
Insert A New Mini-Map Image
---------------------------

Copy the .dds file(s) for the map(s) you want to edit into the images folder where DekaMapper is located. Now, rename them to the name of the map (include the .dds). You can get the name of the map from maplist.csv or the map list in the DekaMapper.

If you can't figure it out, look in the images folder. I've already placed some examples for you in there.

Mini-map images are optional. However, I suggest you have them when working on the map so you know where monsters are going. Not having them almost defeats the purpose of using DekaMapper.

----------
Open a map
----------

After properly setting up the tool, you can choose the map from the drop down box in the top-center by:

    clicking the arrow and choosing from the drop down list. 

OR

    start typing in the map's name and you will start receiving a list of maps that match the expression that you have typed in, and you may then choose from that list. 

----
Save
----

You can save your map by simply clicking on the File menu and then clicking Save. Once you see the "Save Complete!" message box, you changes have been successfully saved.

---------------
Select Monsters
---------------

In the left grid, simply click the row header (the blank box to the left of the index). Multi-row selection is supported.

----
Edit
----

In the left grid, you can edit a cell by simply clicking on the cell you wish to edit, then clicking on it again to edit it. Changing cells, pressing enter, or tab will validate your changes to the cell. Once your data has been validated the cell will exit edit mode. If the data you have entered is invalid, you'll receive an exclamation point at the row's header, and you will not be able to leave that cell until you enter valid data in the cell. You may hover over the exclamation point to see the error in a tool tip.

---------------
Delete Monsters
---------------

In the left grid, once you have your monster(s) selected, simply press the delete key to delete the monster(s) you have selected.

------------
Add Monsters
------------

There is two ways you can add monsters.

    Adding a new row
        Scroll to the bottom of the list until you find the row with the asterisk next to it.
        Bring up editing in the first column, type the index of the monster you would like to add, and then validate your changes 
    GUI Map
        Select the monster you wish to add from the drop down box above the left grid.
            Note that this drop down box also supports typing, and has auto-suggestion just like the map list. 
        Select the respawn time you wish the monster to have from the increment/decrement box below. You may click the arrows or type in a value.
        Click on the mini-map where you would like the monster to be added. 

------------
Sort Columns
------------

Columns have the ability to be sorted just by clicking on the column header. You can sort ascending or descending. Also, note that in the left data grid, if you sort the columns, that's how the monsters will be listed when you save it. Not that their positions will change, but the order in which they are listed when you re-open the map will be the same.

------------------
Scale The Mini-Map
------------------

You can scale the mini-map up to 5x for greater precision by clicking the up arrow in the input box next to Scale Factor: until it reaches 5. To return it to its normal size, click the down arrow until it reaches 1.

--------------
Color Monsters
--------------

You can color specific monsters on the map clicking on any cell in the row of the monster that wants to be colored, then selecting the color from the drop down box below the mini-map then click the Set button next to the color drop down box.

	Carry Colors - if Carry Colors is checked, then it will save the color of the monster between maps. This means if you switch maps you changed the color of a particular monster, and it also happens to be on another map, it will already be set to this color.
        
	Note that carry colors only works per session because it's values are saved in the RAM. If you close DekaMapper and open it again, all monsters will be reset back to the cyan color. 
