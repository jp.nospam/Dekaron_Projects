﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using DevIL;


namespace DekaronMobEdit
{
    public partial class Form1 : Form
    {
        int isMoving = 0;
        int isSetting = 0;
        int sFactor = 1;
        int mX;
        int mY;
        int bWH = 512;
        ArrayList fPaths = new ArrayList();
        ArrayList mID = new ArrayList();
        ArrayList mList = new ArrayList();
        List<string> MonsterNames = new List<string>();
        Bitmap newimage = new Bitmap(512, 512);
        int dChanged = 0;
        string fPath;
        DataTable newTable = new DataTable();
        DataTable newTable2 = new DataTable();
        SolidBrush brush = new SolidBrush(Color.Cyan);
        SolidBrush brushSel = new SolidBrush(Color.Yellow);
        Graphics g;
        private string MapFile;
        Dictionary<int, string> mColors = new Dictionary<int, string>();

        public Form1()
        {
            InitializeComponent();
            int pWidth = pictureBox1.Width * sFactor;
            int pHeight = pictureBox1.Height * sFactor;
            Bitmap bitmap = new Bitmap(pWidth, pHeight);
            pictureBox1.Image = bitmap;
        }
        public List<string[]> parseCSV(string path, int lineSkip)
        {
            List<string[]> parsedData = new List<string[]>();
            using (StreamReader readFile = new StreamReader(path))
            {
                string line;
                string[] row;
                int skipCount = 0;
                while ((line = readFile.ReadLine()) != null)
                {
                    if (skipCount == lineSkip)
                    {
                        row = line.Split(',');
                        parsedData.Add(row);
                    }
                    else
                    {
                        skipCount++;
                    }
                }
            }

            return parsedData;
        }

        private void pictureBox1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            mX = e.X / sFactor;
            mY = e.Y / sFactor;
            lblX.Text = "X: " + mX.ToString();
            lblY.Text = "Y: " + mY.ToString();
        }

        private void saveCSV(string fileName)
        {
            using (StreamWriter writer = new StreamWriter(fileName))
            {
                string stLine = string.Empty;
                for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                {
                    stLine = string.Empty;
                    for (int j = 0; j < dataGridView1.Rows[i].Cells.Count; j++)
                    {
                        if (j == (dataGridView1.Rows[i].Cells.Count - 1))
                        {
                            stLine = stLine + dataGridView1.Rows[i].Cells[j].Value.ToString();
                        }
                        else
                        {
                            stLine = stLine + dataGridView1.Rows[i].Cells[j].Value.ToString() + ",";
                        }
                    }
                    writer.WriteLine(stLine);
                }
            }
            MessageBox.Show("Save complete!");
        }

        private void dataGridView1_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            int i;
            if (!int.TryParse(Convert.ToString(e.FormattedValue), out i) && dataGridView1[e.ColumnIndex, e.RowIndex].IsInEditMode)
            {
                dataGridView1.Rows[e.RowIndex].ErrorText = "Cell must be numeric values only.";
                e.Cancel = true;
            }
        }

        void dataGridView1_CellValidated(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            dataGridView1.Rows[e.RowIndex].ErrorText = String.Empty;
        }

        private void comboBox1_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            comboBox1.DroppedDown = false;
        }

        private void comboBox2_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            comboBox2.DroppedDown = false;
        }

       private void dataGridView1_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Delete)
            {
                pictureBox_Refresh();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!File.Exists(System.AppDomain.CurrentDomain.FriendlyName.ToString()+".config"))
            {
                string[] lines = { "<configuration>", "<startup useLegacyV2RuntimeActivationPolicy=\"true\">", "<supportedRuntime version=\"v4.0\"/>","</startup>", "</configuration>"};
                System.IO.File.WriteAllLines(@".\" + System.AppDomain.CurrentDomain.FriendlyName.ToString() + ".config", lines);
            }
            if (!File.Exists(".\\creature\\monster.csv"))
            {
                MessageBox.Show("monster.csv not found! Program Exiting!");
                Application.Exit();
            }
            else
            {
                if (!File.Exists(".\\maplist.csv"))
                {
                    MessageBox.Show("maplist.csv not found! Program Exiting!");
                    Application.Exit();
                }
                else
                {
                    this.Text = "DekaMapper";
                    List<string[]> mobParse = parseCSV(".\\creature\\monster.csv", 1);
                    comboBox3.Items.Add("Cyan");
                    comboBox3.Items.Add("Red");
                    comboBox3.Items.Add("Blue");
                    comboBox3.Items.Add("White");
                    comboBox3.Items.Add("Black");
                    comboBox3.Items.Add("Green");
                    comboBox3.Items.Add("Orange");
                    comboBox3.Items.Add("Pink");
                    comboBox3.Items.Add("Purple");
                    comboBox3.SelectedIndex = 0;
                    comboBox3.Enabled = false;
                    button2.Enabled = false;
                    int q;
                    foreach (string[] row in mobParse)
                    {
                        comboBox1.Items.Add(row[1] + " [" + row[0] + "]");
                        int.TryParse(row[0], out q);
                        mID.Add(q);
                        MonsterNames.Add(row[1]);

                    }

                    comboBox1.SelectedIndex = 0;
                    List<string[]> mapListParse = parseCSV(".\\maplist.csv", 1);
                    foreach (string[] row in mapListParse)
                    {
                        if (row[45].ToString() != string.Empty)
                        {
                            comboBox2.Items.Add(row[1]);
                            fPaths.Add(row[45]);
                        }
                    }

                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveCSV(fPath);
            if (dChanged == 1)
            {
                dChanged = 0;
            }
        }

        private void dataGridView1_CellValueChanged(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if (isMoving == 0 && isSetting == 0)
            {
                pictureBox_Refresh();
                dChanged = 1;
            }
        }

        private void dgr2_Update()
        {
            int rowi = 0;
            int coli = 0;
            if (dataGridView2.Rows.Count > 0)
            {
                rowi = dataGridView2.CurrentCell.RowIndex;
                coli = dataGridView2.CurrentCell.ColumnIndex;
            }

            mList.Clear();
            newTable2.Clear();
            foreach (DataGridViewRow dgr in dataGridView1.Rows)
            {
                if (dgr.Cells[0].Value != null && dgr.IsNewRow == false && dgr.Cells[0].Value.ToString() != String.Empty)
                {
                    if (!mList.Contains(dgr.Cells[0].Value))
                    {
                        mList.Add(dgr.Cells[0].Value);
                        if (mColors.ContainsKey((int)dgr.Cells[0].Value) == false)
                        {
                            mColors.Add((int)dgr.Cells[0].Value, "Cyan");
                        }
                        if (mID.IndexOf(dgr.Cells[0].Value) >= 0)
                        {
                            newTable2.Rows.Add(dgr.Cells[0].Value.ToString(), comboBox1.Items[mID.IndexOf(dgr.Cells[0].Value)].ToString(), 0, 0, mColors[(int)dgr.Cells[0].Value]);
                        }
                        else
                        {
                            newTable2.Rows.Add(dgr.Cells[0].Value, "[NULL]", 0, 0, mColors[(int)dgr.Cells[0].Value]);
                        }
                    }
                }
            }
            if (dataGridView2.Rows.Count > 0)
            {
                if ((dataGridView2.Rows.Count - 1) >= rowi)
                {
                    dataGridView2.CurrentCell = dataGridView2[coli, rowi];
                }
                else
                {
                    dataGridView2.CurrentCell = dataGridView2[coli, 0];
                }
            }
        }

        private int dgr2_iSearch(int col, string val)
        {
            foreach (DataGridViewRow dgr in dataGridView2.Rows)
            {
                if (dgr.Cells[col].Value.ToString() == val)
                {
                    return dgr.Index;
                }
            }
            return -1;
        }

        private void dataGridView1_UserDeletedRow(object sender, System.Windows.Forms.DataGridViewRowEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                return;
            }
            pictureBox_Refresh();
            dChanged = 1;
        }

        void dataGridView1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                pictureBox_Refresh();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != -1)
            {
                foreach (DataGridViewRow rows in dataGridView1.SelectedRows)
                {
                    isSetting = 1;
                    rows.Cells[0].Value = mID[comboBox1.SelectedIndex];
                    isSetting = 0;
                }
                pictureBox_Refresh();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex > -1)
            {
                if (dChanged == 1)
                {
                    DialogResult saveRes = MessageBox.Show(this, "The contents have been changed, would you like to save before switching maps?", "Save?", MessageBoxButtons.YesNo);
                    if (saveRes == DialogResult.Yes)
                    {
                        saveCSV(fPath);
                    }
                    dChanged = 0;
                }

                fPath = "." + fPaths[comboBox2.SelectedIndex].ToString().Remove(0, 8);
                if (File.Exists(fPath))
                {
                    this.Text = "DekaMapper - Map: " + comboBox2.SelectedItem.ToString();
                    List<string[]> mapParse = parseCSV(fPath, 0);
                    newTable = new DataTable();
                    dataGridView1.DataSource = newTable;
                    newTable.Columns.Add("Index");
                    newTable.Columns.Add("Respawn (secs)");
                    newTable.Columns.Add("Y");
                    newTable.Columns.Add("X");
                    newTable.Columns[0].DataType = typeof(int);
                    newTable.Columns[1].DataType = typeof(int);
                    newTable.Columns[2].DataType = typeof(int);
                    newTable.Columns[3].DataType = typeof(int);
                    newTable2 = new DataTable();
                    dataGridView2.DataSource = newTable2;
                    newTable2.Columns.Add("Index");
                    newTable2.Columns.Add("Name");
                    newTable2.Columns.Add("Selected");
                    newTable2.Columns.Add("Total");
                    newTable2.Columns.Add("Color");
                    newTable2.Columns[0].DataType = typeof(int);
                    newTable2.Columns[1].DataType = typeof(string);
                    newTable2.Columns[2].DataType = typeof(int);
                    newTable2.Columns[3].DataType = typeof(int);
                    newTable2.Columns[4].DataType = typeof(string);
                    mList.Clear();

                    if (checkBox1.Checked == false)
                    {
                        mColors = new Dictionary<int,string>();
                    }

                    MapFile = ".\\images\\" + comboBox2.SelectedItem.ToString().Replace(" ", "_") + ".dds";

                    if (!File.Exists(MapFile))
                    {
                        MessageBox.Show(String.Format("Cannot find {0} No minimap will be loaded.", MapFile));
                    }

                    foreach (string[] row in mapParse)
                    {
                        try
                        {
                            newTable.Rows.Add(row);
                        }
                        catch
                        {
                        }
                    }
                    if (dataGridView1.Rows.Count > 0)
                    {
                        pictureBox_Refresh();
                    }

                    saveToolStripMenuItem.Enabled = true;
                    button1.Enabled = true;
                    comboBox1.Enabled = true;
                    pictureBox1.Enabled = true;
                    numericUpDown2.Enabled = true;
                    comboBox3.Enabled = true;
                    button2.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Could not find " + fPath);
                }
            }
        }

        private void pictureBox_Refresh()
        {
            dgr2_Update();
            LoadMapImage();
            int x = 0;
            int y = 0;
            g = Graphics.FromImage(pictureBox1.Image);
            foreach (DataGridViewRow dgr in dataGridView1.Rows)
            {
                try
                {
                    x = (int)dgr.Cells[3].Value * sFactor;
                    y = (int)dgr.Cells[2].Value * sFactor;
                    int s = dgr2_iSearch(0, dgr.Cells[0].Value.ToString());
                    if (s != -1)
                    {
                        dataGridView2.Rows[s].Cells[3].Value = (int)dataGridView2.Rows[s].Cells[3].Value + 1;
                    }
                    if (!dgr.Selected)
                    {
                        if (s != -1)
                        {
                            brush = new SolidBrush(Color.FromName(dataGridView2.Rows[s].Cells[4].Value.ToString()));
                        }
                        else
                        {
                            brush = new SolidBrush(Color.Cyan);
                        }
                    }
                    else
                    {
                        dataGridView2.Rows[s].Cells[2].Value = (int)dataGridView2.Rows[s].Cells[2].Value + 1;
                        brush = new SolidBrush(Color.Yellow);
                    }
                    g.FillRectangle(brush, x, y, 4, 4);
                }
                catch
                {
                }
            }
            pictureBox1.Refresh();
        }

        private void LoadMapImage()
        {
            bWH = 512 * sFactor;
            try
            {
                newimage = new Bitmap(DevIL.DevIL.LoadBitmap(MapFile), bWH, bWH);
            }
            catch (Exception)
            {
                newimage = new Bitmap(512, 512);
            }
            pictureBox1.Image = newimage;
        }

        private void pictureBox1_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (comboBox1.SelectedIndex > -1)
            {
                if (e.Button == MouseButtons.Left)
                {
                    g = Graphics.FromImage(pictureBox1.Image);
                    int x = e.X / sFactor;
                    int y = e.Y / sFactor;
                    newTable.Rows.Add(mID[comboBox1.SelectedIndex].ToString(), numericUpDown1.Value.ToString(), y.ToString(), x.ToString());
                    pictureBox_Refresh();
                }
                if (e.Button == MouseButtons.Right)
                {
                    isMoving = 1;
                    foreach (DataGridViewRow dgr in dataGridView1.SelectedRows)
                    {
                        if (dgr.Cells[0].Value.ToString() != String.Empty && dgr.IsNewRow == false)
                        {
                            int x = e.X / sFactor;
                            int y = e.Y / sFactor;
                            dgr.Cells[3].Value = x.ToString();
                            dgr.Cells[2].Value = y.ToString();
                        }
                    }
                    isMoving = 0;
                    pictureBox_Refresh();
                }
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.Show();
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            sFactor = (int)numericUpDown2.Value;
            int bWH = 512 * sFactor;
            pictureBox1.Size = new System.Drawing.Size(bWH, bWH);
            pictureBox_Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mColors[(int)dataGridView2.Rows[dataGridView2.CurrentCell.RowIndex].Cells[0].Value] = comboBox3.SelectedItem.ToString();
            pictureBox_Refresh();
        }

        private void dataGridView1_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {

            try
            {
                var monsterName = GetMonsterName(dataGridView1.Rows[e.RowIndex]);
                e.ToolTipText = String.Format(monsterName);
            }
            catch (Exception)
            {
                e.ToolTipText = "";
            }
        }


        private string GetMonsterName(DataGridViewRow r)
        {
            try
            {
                var id = (int)r.Cells[0].Value;
                return GetMonsterName(id);
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        private string GetMonsterName(int id)
        {
            try
            {
                var index = mID.IndexOf(id);
                return MonsterNames[index];
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

    }
}
