Group of projects pertaining to the development of a game called Dekaron.

Alpha : C++ & Autoit - client and server for updating game files.

DekaMapper : C# - A GUI program for adding and removing NPC spawn locations.

DekaSite : PHP - A PHP website designed using Frameless framework.

EZDekaron : AutoIt - Collection of macros to perform bot fuctions for Dekaron.

dekalog : BASH - Script to parse item drops from the Dekaron server log files.