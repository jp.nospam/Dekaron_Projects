<?php
/*
    Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is furnished to do
    so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

/**
 * \brief Character database model
 */
class CharacterModel
{
   /**
     * \var $args
     * \brief An array of the arguments passed to the class.
     *      
     * Reserved indexes:
     * - [0]: The host IP address or domain name of the database.
     * - [1]: The username to connect to the database with.
     * - [2]: The password to use for the username provided
     */
    private $args;
    /**
     * \var $mssql
     * \brief A PDO connection object
     */
    private $mssql;

    /**
     * \brief Class constructor
     *
     * This constructor establishes a connection to the datatabase. It
     * dynamically gets arguments via func_get_args() and stores them in $args.
     */
    function __construct()
    {
        $this->args =& func_get_args();

        global $con;

        if (!isset($con))
        {
            try
            {
                if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
                {
                    $con = new PDO('odbc:Driver={SQL Server};server='.$this->args[0].';uid='.$this->args[1].';pwd='.$this->args[2].';');
                }
                else
                {
                    $con = new PDO('dblib:host='.$this->args[0].':1433', $this->args[1], $this->args[2]);
                }
            }
            catch (PDOException $e)
            {
                throw new DatabaseException('Unable to connect to the database.');    
            }
        }
        $this->mssql =& $con;
    }

    /**
     * \brief Get all characters and information of chars by an account by name
     *
     * \param $user_data
     *      Data to look for based on $type
     * \param $type
     *      (optional) Integer that specifies what to look for
     *      - 0: Searches by user_no.
     *      - All other integers: Serarches by user_id.
     * \param $info
     *      (optional) List of columns to return separated by a comma. Default
     *      value is character_name.
     * \return
     *      - False: if no characters are found on the account
     *      - Array:
     *          - [0]: Count of characters
     *          - [1]: Array of characters with information
    */
    public function acctChars($user_data, $type = 0, $info = 'character_name')
    {
        $smt = false;
        $stmt = null;
        
        if($type == 0) $stmt = 'SELECT '.$info.' FROM character.dbo.user_character WHERE user_no = ?';
        else $stmt = 'SELECT '.$info.' FROM character.dbo.user_character JOIN account.dbo.user_profile ON account.dbo.user_profile.user_no = character.dbo.user_character.user_no WHERE user_id = ?';
        
        $smt = $this->mssql->prepare($stmt);
        
        if (!$smt) throw new DatabaseException('Statement preparation failed.');
        
        if(!$smt->execute(array($user_data))) throw new DatabaseException('Query failed to execute.');
        
        $smt = $smt->fetchAll();

        if(count($smt) < 1) return false;

        return array(count($smt), $smt);
        
    }

    /**
     * \brief Get equipped items on a character
     *
     * \param $charNo
     *      Character number
     * \param $info
     *      Columns to fetch
     * \return
     *      - False: if items are equipped
     *      - array:
     *          - [0]: Count of items
     *          - [1]: Array of information
    */
    public function getInventory($charNo, $info = 'character_no')
    {
        $smt = $this->mssql->prepare("select ".$info." from character.dbo.user_suit where character_no = ?");

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($charNo))) throw new DatabaseException('Query failed to execute.');

        $items = $smt->fetchAll();
        
        if(count($items) < 1) return false;

        return array(count($items), $items);
    }

    /**
     * \brief Get data for a specific character.
     *
     * \param $char_data
     *      Data to look for based on $type
     * \param $type
     *      - 0: Searches by character_no.
     *      - All other integers: Searches by character_name.
     * \param $info
     *      Columns to return
     * \param $verify
     *      - 1: Verifies by user_no
     *      - All other integers: turn verification off.
     * \param $user_data
     *      Data to based on $verify
    */
    public function charInfo($char_data, $type = 0, $info = 'character_name', $verify = 0,  $user_data = NULL)
    {
        $smt = false;
        $stmt = null;
        
        $data = array($char_data);
    
        if($type == 0) $type = 'character_no';
        else $type = 'character_name';
    
        $stmt = 'SELECT '.$info.' FROM character.dbo.user_character WHERE '.$type.' = ?';

        if($verify == 1 && $user_data != NULL)
        {
            $stmt .= ' AND user_no = ?';

            $data[] = $user_data;
        }
        
        $smt = $this->mssql->prepare($stmt);
    
        if (!$smt) throw new DatabaseException('Statement preparation failed.');
        
        $stuff = array($stmt, $data, $type, $info, $verify, $user_data);

        if(!$smt->execute($data)) throw new DatabaseException('Query failed to execute');
        
        $smt = $smt->fetchAll();

        if(count($smt) != 1) return false;

        return $smt[0];
    }

    /**
     * \brief Get Deadfront times.
     *
     * \return
     *      - false: if no deadfront times have been set
     *      - array:
     *          - [0]: Count of deadfronts
     *          - [1]: Deadfront time information
    */
    public function GetDFs()
    {
        $smt = $this->mssql->query('SELECT sort_cd FROM character.dbo.cm_bcd_item ORDER BY orderby_no ASC');

        if(!$smt) throw new DatabaseException('Query failed to execute.');
        
        $smt = $smt->fetchAll();

        if(count($smt) < 1) return false;

        return array(count($smt), $smt);
    }

    /**
     * \brief Get top PVPers
     *
     * \param $exempt
     *      Exempt guild from being shown
     * \param $amount
     *      Maximum amount of players to select
     * \return
     *      - false: if no characters qualify
     *      - array:
     *          - [0]: count of characters
     *          - [1]: array of information
    */
    public function PVP($exempt, $amount)
    {
        $smt = $this->mssql->prepare("select top ".$amount." character.dbo.user_character.character_name as wChar ,dwPVPpoint, wWinRecord, wLoseRecord, wLevel, character.dbo.guild_info.guild_name, round(((case when wLoseRecord=0 then 9999 else cast(wWinRecord as decimal)/cast(wLoseRecord as decimal)end)),3) as ratio from character.dbo.user_character left join account.dbo.user_profile on account.dbo.user_profile.user_no = character.dbo.user_character.user_no left join character.dbo.guild_char_info on character.dbo.user_character.character_name = character.dbo.guild_char_info.character_name left join character.dbo.guild_info on character.dbo.guild_char_info.guild_code = character.dbo.guild_info.guild_code WHERE (wWinRecord > 0 or wLoseRecord > 0) and account.dbo.user_profile.login_tag = 'Y' and (character.dbo.guild_info.guild_name is null or character.dbo.guild_info.guild_name <> ?) order by dwPVPpoint desc, ratio desc, wLoseRecord asc, wWinRecord desc");

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($exempt))) throw new DatabaseException('Query failed to execute.');
        
        $smt = $smt->fetchAll();

        if(count($smt) < 1) return false;

        return array(count($smt), $smt);
    }

     /**
     * \brief Get top PKers
     *
     * \param $exempt
     *      Exempt guild from being shown
     * \param $amount
     *      Maximum amount of players to select
     * \return
     *      - false: if no characters qualify
     *      - array:
     *          - [0]: count of characters
     *          - [1]: array of information
    */
    public function PK($exempt, $amount)
    {
        $smt = $this->mssql->prepare("select top ".$amount." character.dbo.user_character.character_name as wChar , wPKCount, character.dbo.guild_info.guild_name from character.dbo.user_character left join account.dbo.user_profile on account.dbo.user_profile.user_no = character.dbo.user_character.user_no left join character.dbo.guild_char_info on character.dbo.user_character.character_name = character.dbo.guild_char_info.character_name left join character.dbo.guild_info on character.dbo.guild_char_info.guild_code = character.dbo.guild_info.guild_code WHERE wPKCount > '0' and account.dbo.user_profile.login_tag = 'Y' and (character.dbo.guild_info.guild_name is null or character.dbo.guild_info.guild_name <> ?) order by wPKCount desc");

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($exempt))) throw new DatabaseException('Query failed to execute.');
        
        $smt = $smt->fetchAll();

        if(count($smt) < 1) return false;

        return array(count($smt), $smt);
    
    }

    /**
     * \brief Get the highest levels
     *
     * \param $exempt
     *      Exempt guild from being shown
     * \param $amount
     *      Maximum amount of players to select
     * \return
     *      - false: if no characters qualify
     *      - array:
     *          - [0]: count of characters
     *          - [1]: array of information
     */
    public function hlvls($exempt, $amount)
    {
        $smt = $this->mssql->prepare("select top ".$amount." character.dbo.user_character.character_name, character.dbo.user_character.wLevel, character.dbo.user_character.dwExp, character.dbo.guild_info.guild_name from character.dbo.user_character left join account.dbo.user_profile on character.dbo.user_character.user_no = account.dbo.user_profile.user_no left join character.dbo.guild_char_info on character.dbo.user_character.character_name = character.dbo.guild_char_info.character_name left join character.dbo.guild_info on character.dbo.guild_char_info.guild_code = character.dbo.guild_info.guild_code where account.dbo.user_profile.login_tag <> 'N' and (character.dbo.guild_info.guild_name is null or character.dbo.guild_info.guild_name <> ?) order by character.dbo.user_character.wLevel Desc, character.dbo.user_character.dwExp desc");
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($exempt))) throw new DatabaseException('Query failed to execute.');

        $smt = $smt->fetchAll();

        if(count($smt) < 1) return false;

        return array(count($smt), $smt);
    }

    /**
     * \brief Deletes a character's skills
     *
     * \param $charNo
     *      Character number
     */
    public function deleteSkills($charNo)
    {
        $smt = $this->mssql->prepare('DELETE FROM character.dbo.user_slot WHERE character_no = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($charNo))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Deletes a character's skill bar
     *
     * \param $charNo
     *      Character number
     */
    public function deleteSkillBar($charNo)
    {
        $smt = $this->mssql->prepare('DELETE FROM character.dbo.user_skill WHERE character_no = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($charNo))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Update character information for class change controller
     *
     *  TBD
     */
    public function changeClsUpdate($str, $spr, $con, $dex, $statP, $class, $skillP, $money, $charNo)
    {
        $smt = $this->mssql->prepare("UPDATE character.dbo.user_character SET wStr = ?, wSpr = ?, wCon = ?, wDex = ?, wStatPoint = ?, byPCClass = ?, wSkillPoint = ?, dwMoney = dwMoney - ? where character_no = ?");
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($str, $spr, $con, $dex, $statP, $class, $skillP, $money, $charNo))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Update stats for rebirth controller
     *
     * TBD
     */
    public function rebirthUpdate($str, $spr, $con, $dex, $lvl, $statP, $x, $y, $mID, $charNo)
    {
        $smt = $this->mssql->prepare("UPDATE character.dbo.user_character SET wStr = ?, wSpr = ?, wCon = ?, wDex = ?, wLevel = ?, wStatPoint = ?, wPosX = ?, wPosY = ?, wMapIndex = ?, dwExp = '0' where character_no = ?");

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($str, $spr, $con, $dex, $lvl, $statP, $x, $y, $mID, $charNo))) throw new DatabaseException(var_dump($char_data));
    }

    /**
     * \brief Send mail to a character in-game
     *
     * TBD
     */
    public function sendMail($charNo, $from, $subject, $message, $itemid, $dil)
    {
        $smt = $this->mssql->prepare("EXEC character.dbo.SP_POST_SEND_OP ?,?,1,?,?,?,?,0");

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($charNo, $from, $subject, $message, $itemid, $dil))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Update a character's skill point
     *
     * \param $charNo
     *      Character's number
     * \param $skillP
     *      Value to set skill points to
     */
    public function updateSkillPoint($charNo, $skillP)
    {
        $smt = $this->mssql->prepare("UPDATE character.dbo.user_character SET wSkillPoint = ? WHERE character_no = ?");

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($skillP, $charNo))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Update a character's dil in their inventory
     *
     * TBD
     */
    public function updateDil($charNo, $amount, $type = 0)
    {
        if($type == 0) $type = '+';
        else $type = '-';

        $smt = $this->mssql->prepare('UPDATE character.dbo.user_character SET dwMoney = dwMoney '.$type.' ? where character_no = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($amount, $charNo))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Update a character's experierience
     *
     * TBD
     */
    public function updateExp($charNo, $amount, $type = 0)
    {
        if($type == 0) $type = '+';
        else $type = '-';

        $smt = $this->mssql->prepare('UPDATE character.dbo.user_character SET dwExp = dwExp '.$type.' ? where character_no = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($amount, $charNo))) throw new DatabaseException('Query failed to execute');

    }

    /**
     * \brief Get all online characters
     *
     * TBD
     */
    public function getOnline()
    {
        $smt = $this->mssql->query('select character_name, wmapindex, Cast(Cast(SubString(conn_ip, 1, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 2, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 3, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 4, 1) AS Int) As Varchar(3)) as ip FROM character.dbo.user_character left JOIN character.dbo.char_connlog_key on character.dbo.user_character.character_no = character.dbo.char_connlog_key.character_no left join account.dbo.user_profile on account.dbo.user_profile.user_no = character.dbo.user_character.user_no WHERE character.dbo.char_connlog_key.logout_time is null and character.dbo.user_character.login_time = character.dbo.char_connlog_key.login_time and account.dbo.user_profile.login_flag = \'1100\' order by character.dbo.user_character.character_name asc');
    
        if(!$smt) throw new DatabaseException('Query failed to execute');

        $players = $smt->fetchAll();

        if(count($players) < 1) return false;

        return array(count($players), $players);
    }

    /**
     * \brief Update character information for character modification controller
     *
     * TBD
     */
    public function charModUpdate($newName, $user_no, $dil, $storeDil, $storageDil, $hp, $mp, $str, $dex, $con, $spr, $statP, $skillP, $level, $class, $pk, $shield, $pvpP, $win, $lose, $char)
    {
        $smt = $this->mssql->prepare('UPDATE character.dbo.user_character SET character_name = ?, user_no = ?, dwMoney = ?,dwStoreMoney = ?,dwStorageMoney = ?,nHP = ?,nMP = ?,wStr = ?, wDex = ?, wCon = ?,wSpr = ?,wStatPoint = ?,wSkillPoint = ?,wLevel = ?,byPCClass = ?,wPKCount = ?,nShield = ?,dwPVPPoint = ?,wWinRecord = ?,wLoseRecord = ? where character_name = ?');
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($newName, $user_no, $dil, $storeDil, $storageDil, $hp, $mp, $str, $dex, $con, $spr, $statP, $skillP, $level, $class, $pk, $shield, $pvpP, $win, $lose, $char))) throw new DatabaseException('Query failed to execute');
    }
}
?>
