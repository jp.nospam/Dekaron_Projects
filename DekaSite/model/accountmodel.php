<?php
/*
    Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is furnished to do
    so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

/**
 * \brief Account database model
 */
class AccountModel
{
    /**
     * \var $args
     * \brief An array of the arguments passed to the class.
     *      
     * Reserved indexes:
     * - [0]: The host IP address or domain name of the database.
     * - [1]: The username to connect to the database with.
     * - [2]: The password to use for the username provided
     */
    private $args;
    /**
     * \var $mssql
     * \brief A PDO connection object
     */
    private $mssql;

    /**
     * \brief Class constructor
     *
     * This constructor establishes a connection to the datatabase. It
     * dynamically gets arguments via func_get_args() and stores them in $args.
     */
    function __construct()
    {
        $this->args =& func_get_args();

        global $con;

        if (!isset($con))
        {
            try
            {
                if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
                {
                    $con = new PDO('odbc:Driver={SQL Server};server='.$this->args[0].';uid='.$this->args[1].';pwd='.$this->args[2].';');
                }
                else
                {
                    $con = new PDO('dblib:host='.$this->args[0].':1433', $this->args[1], $this->args[2]);
                }
            }
            catch (PDOException $e)
            {
                throw new DatabaseException('Could not connect to the database.');
            }
        }

        $this->mssql =& $con;
    }

    /**
     * \brief Change password of an account
     *
     * \param $user_data
     *      The user data to search by based on $searchBy.
     * \param $pass
     *      Password as plain text.
     * \param $searchby
     *      (optional) Searches by user_id or user_no based on $user_data.
     *      - 0: (default) Searches by user_no.
     *      - All other integers: Searches by user_id.
     */
    public function ChangePass($user_data, $pass, $searchby = 0)
    {
        if($searchby == 0) $searchby = 'user_no';
        else $searchby = 'user_id';

        $smt = $this->mssql->prepare("UPDATE account.dbo.user_profile SET user_pwd = ?  WHERE $searchby  = ?");

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array(md5($pass), $user_data))) throw new DatabaseException('Query failed to execute.');

    }

    /**
     * \brief Get a list of accounts with information
     *
     * \param $info
     *      (optional) Column(s) to return. Default is user_id.
     * \return
     *      array:
     *      - [0]: Count of accounts
     *      - [1]: Array with fetched information
     */
    public function GetAccts($info = 'user_id')
    {

        $smt = $this->mssql->query("SELECT $info FROM account.dbo.user_profile");
        
        if(!$smt) throw new DatabaseException('Query failed to execute.');

        $smt= $smt->fetchAll();

        return array(count($smt), $smt);
    }

    /**
     * \brief Get information of a specific account
     *
     * \param $userdata
     *      Data to search by baed on $searchBy
     * \param $info
     *      Column(s) to return. Default is user_id.
     * \param $searchBy
     *      (optional) Value to search by based on $userdata:
     *      - 0: (default) Searches by user_id.
     *      - 1: Searches by user_no 
     *      - All other integers: Searches by user_id and pass
     * \param $pass
     *      (optionally required) This is required if $searchBy is not 0 or 1.
     * \return
     *      - false: if the account does not exist.
     *      - array: containing account information
     */
    public function GetAcct($userdata, $searchBy = 0, $info = 'user_id', $pass = NULL)
    {
        $data = array($userdata);

        if($searchBy == 0) $searchBy = 'user_id = ?';
        elseif ($searchBy == 1) $searchBy = 'user_no = ?';
        else
        {
            $searchBy = 'user_id = ? AND user_pwd = ?';
            $data[] = md5($pass);
        }
    
        $stmt = 'SELECT '.$info.' FROM account.dbo.user_profile WHERE '.$searchBy;

        $smt = $this->mssql->prepare($stmt);

        if(!$smt) throw new DatabaseException('Statement preparation failed.');
        
        if(!$smt->execute($data)) throw new DatabaseException('Query failed to execute.');
        
        $smt = $smt->fetchAll();
        
        if(count($smt) != 1) return false;

        return $smt[0];
    }
    
    /**
     * \brief Register an account
     *
     * \param $no
     *      User number.
     * \param $acct
     *      Account ID.
     * \param $pass
     *      Password as plain text.
    */
    public function reg($no, $acct, $pass)
    {
        
        $smt = $this->mssql->prepare("INSERT INTO account.dbo.USER_PROFILE (user_no,user_id,user_pwd,resident_no,user_type,login_flag,login_tag,ipt_time,login_time,logout_time,user_ip_addr,server_id,user_reg_date) VALUES (?,?,?,'801011000000','1','0','Y','1/1/1900',null,null,null,'000',?)");

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($no, $acct, md5($pass), date('n/j/Y g:i:s A')))) throw new DatabaseException('Query failed to execute.');
    }
    
    /**
     * \brief Ban an account
     *
     * \param $data
     *      User data based on $searchby
     * \param $searchby
     *      (optional) Searches by user_id or user_no based on $data
     *      - 0: (default) Searches by user_id.
     *      - All other integers: Searches by user_no
     */
    public function banAcct($data, $searchby = 0)
    {
        if ($searchby == 0) $searchby = 'user_id';
        else $searchby = 'user_no';

        $smt = $this->mssql->prepare('UPDATE account.dbo.USER_PROFILE SET login_tag = \'N\' WHERE '.$searchby.' = ?');
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($data))) throw new DatabaseException('Query failed to execute.');
    }
    
    /**
     * \brief Unban an account
     *
     * \param $data
     *      Data to search by based on $searchby
     * \param $searchby
     *      (optional) Seraches by user_id or user_no.
     *      - 0: (default) Searches by user_id.
     *      - All other integer: Searches by user_no
     */
    public function unbanAcct($data, $searchby = 0)
    {
        if ($searchby == 0) $searchby = 'user_id';
        else $searchby = 'user_no';

        $smt = $this->mssql->prepare('UPDATE account.dbo.USER_PROFILE SET login_tag = \'Y\' WHERE '.$searchby.' = ?');
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($data))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Link accounts by ip
     *
     * \param $data
     *      IP address to get all linked accounts.
     * \return
     *      - false: if no accounts are found
     *      - array:
     *          - [0]: count of accounts
     *          - [1]: array of information
     */
    public function linkAcctsIP($data)
    {
        $smt = $this->mssql->prepare('select distinct user_id from account.dbo.user_connlog_key left join account.dbo.user_profile on account.dbo.user_profile.user_no = account.dbo.user_connlog_key.user_no where Cast(Cast(SubString(conn_ip, 1, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 2, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 3, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 4, 1) AS Int) As Varchar(3)) = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($data))) throw new DatabaseException('Query failed to execute.');

        $smt = $smt->fetchAll();

        if(count($smt) < 1) return false;

        return array(count($smt), $smt);
    }
    
    /**
     * \brief Link IPs
     *
     * \param $data
     *      Account to get all IPs from.
     * \return
     *      - false: if no IPs were found
     *      - array:
     *          - [0]: count of IPs
     *          - [1]: array of information
     */
    public function linkIPs($data)
    {
        $smt = $this->mssql->prepare('select distinct Cast(Cast(SubString(conn_ip, 1, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 2, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 3, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 4, 1) AS Int) As Varchar(3)) as IP from account.dbo.user_connlog_key left join account.dbo.user_profile on account.dbo.user_profile.user_no = account.dbo.user_connlog_key.user_no where user_id = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($data))) throw new DatabaseException('Query failed to execute.');

        $smt = $smt->fetchAll();

        if(count($smt) < 1) return false;
        
        return array(count($smt), $smt);
    }

    /**
     * \brief Get accounts linked to an account by an IP
     *
     * \param $acct
     *      Account to build links to
     * \param $IP
     *      IP to link back
     * \return
     *      - false: if no accounts were found
     *      - array:
     *          - [0]: count of accounts
     *          - [1]: array of information
     */
    public function getAcctLinks($acct, $IP)
    {
        $smt = $this->mssql->prepare('select distinct user_id from account.dbo.user_connlog_key left join account.dbo.user_profile on account.dbo.user_profile.user_no = account.dbo.user_connlog_key.user_no where user_id <> ? and Cast(Cast(SubString(conn_ip, 1, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 2, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 3, 1) AS Int) As Varchar(3)) + \'.\' + Cast(Cast(SubString(conn_ip, 4, 1) AS Int) As Varchar(3)) = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($acct, $IP)));

        $smt = $smt->fetchAll();

        if(count($smt) < 1) return false;

        return array(count($smt), $smt);
    }
}
?>
