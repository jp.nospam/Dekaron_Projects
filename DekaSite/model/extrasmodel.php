<?php
/*
    Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is furnished to do
    so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

/**
 * \brief Extras database model
 */
class ExtrasModel
{
    /**
     * \var $args
     * \brief An array of the arguments passed to the class.
     *      
     * Reserved indexes:
     * - [0]: The host IP address or domain name of the database.
     * - [1]: The username to connect to the database with.
     * - [2]: The password to use for the username provided
     * - [3]: The extras database name
     */
    private $args;
    /**
     * \var $mssql
     * \brief A PDO connection object
     */
    private $mssql;
    /**
     * \var $edb
     * \brief string containing the name of the extras databse
     */
    private $edb;

    /**
     * \brief Class constructor
     *
     * This constructor establishes a connection to the datatabase. It
     * dynamically gets arguments via func_get_args() and stores them in $args.
     */
    function __construct()
    {
        $this->args =& func_get_args();
        global $con;

        if (!isset($con))
        {
            try
            {
                if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
                {
                    $con = new PDO('odbc:Driver={SQL Server};server='.$this->args[0].';uid='.$this->args[1].';pwd='.$this->args[2].';');
                }
                else
                {
                    $con = new PDO('dblib:host='.$this->args[0].':1433', $this->args[1], $this->args[2]);
                }
            }
            catch (PDOException $e)
            {
                throw new DatabaseException('Unable to connect to database');
            }
        }
        $this->mssql =& $con;
        $this->edb =& $this->args[3];
    }

    /**
     * \brief Record account actions into session log
     *
     * \param $action
     *      The action that is recorded
     * \param $sAcct
     *      Account to record into the log
     * \param $check
     *      (optional) Preforms an existing account check. Default is false.
     * \param $extras
     *      (optional) Extras enabled configuration setting. Default is false.
     */
    public function sLog($action, $sAcct, $check = false, $extras = false)
    {
        if($extras == false) return;
        
        if($check == true)
        {
            $smt = $this->mssql->prepare("SELECT user_id FROM account.dbo.user_profile WHERE user_id = ?");

            if(!$smt) throw new DatabaseException('Statement preparation failed.');

            if(!$smt->execute(array($sAcct))) throw new DatabaseException('Query failed to execute.');

            if(count($smt->fetchAll()) != 1) return;
        }
        $smt = $this->mssql->prepare('INSERT INTO '.$this->edb.'.dbo.sessionlog values (?, ?, ?, ?)');
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.'); 
            
        if(!$smt->execute(array(date('n/j/Y g:i:s A'), $sAcct, $_SERVER['REMOTE_ADDR'], $action))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Add account to extras extended table
     *
     * \param $no
     *      User number
     * \param $acct
     *      User ID
     */
    public function regExtras($no, $acct)
    {
        $smt = $this->mssql->prepare("INSERT INTO ".$this->edb.".dbo.userExt (user_no, user_id) values (?, ?)");
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($no, $acct))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Get the news
     *
     * \param $amount
     *      (optional) Amount of news stories to get. Default is null which
     *      returns all stories.
     * \return
     *      - false: if no news is available
     *      - array:
     *          - [0]: Count of news post
     *          - [1]:  array of information
     */
    public function GetNews($amount = null)
    {
        if($amount !== null) $amount = ' TOP '.$amount;
    
        $smt = $this->mssql->query('SELECT'.$amount.' * FROM '.$this->edb.'.dbo.site_news ORDER BY sid DESC');
        
        if(!$smt) throw new DatabaseException('Query failed to execute.');

        $smt = $smt->fetchAll();

        if(count($smt) < 1) return false;

        return array(count($smt), $smt);
    }

    /**
     * \brief Get information on a specific news story
     *
     * \param $sid
     *      The news story $sid
     * \return
     *      - false: if news story does not exist
     *      - array of information
     */
    public function GetNewsStory($sid)
    {
        $smt = $this->mssql->prepare('SELECT * FROM '.$this->edb.'.dbo.site_news WHERE sid = ?');
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');
    
        if(!$smt->execute(array($sid))) throw new DatabaseException('Query failed to executae.');

        $news = $smt->fetchAll();

        if(count($news) != 1) return false;

        return $news[0];
    }

    /**
     * \brief Update a news story
     *
     * TBD
     */
    public function updateNews($sid, $title, $content)
    {
        $smt = $this->mssql->prepare('UPDATE '.$this->edb.'.dbo.site_news SET content = ?,title = ? WHERE sid = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($content, $title, $sid))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Delete a news story
     *
     * TBD
     */
    public function deleteNews($sid)
    {
        $smt = $this->mssql->prepare('DELETE FROM '.$this->edb.'.dbo.site_news WHERE sid = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($sid))) throw new DatabaseException('Query failed to execute.');
    }
    
    /**
     * \brief Add a news story
     *
     * TBD
     */
    public function addNews($title, $by, $content)
    {
        $smt = $this->mssql->prepare('INSERT INTO '.$this->edb.'.dbo.site_news (title,wroteby,wrotedate,content) VALUES (?,?,?,?)');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($title, $by, date('m/d/Y'), $content))) throw new DatabaseException('Query failed to execute.');
    }

   /**
     * \brief Get information from extended user tables 
     *
     * TBD
     */
    public function userExtrasInfo($user_data, $info, $type = 0)
    {
        if($type == 0) $type = 'user_id';
        else $type = 'user_no';

        $smt = $this->mssql->prepare('SELECT '.$info.'  FROM '.$this->edb.'.dbo.userExt WHERE '.$type.' = ?');
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');
        
        if(!$smt->execute(array($user_data))) throw new DatabaseException('Query failed to execute.');
        
        $smt = $smt->fetchAll();

        if(count($smt) < 1) return false;

        return $smt[0];
    }
    
    /**
     * \brief Get the list of downloads
     *
     * \return
     *      - false: if no downloads are available
     *      - array:
     *          - [0]: Count of downloads
     *          - [1]: Array information for each download
     */
    public function GetDownloads()
    {
        $smt = $this->mssql->query('SELECT * FROM '.$this->edb.'.dbo.site_download ORDER BY sid ASC');
        
        if(!$smt) throw new DatabaseException('Query failed to execute.');
    
        $smt = $smt->fetchAll();
        
        if(count($smt) < 1) return false;
        
        return array(count($smt), $smt);
    }

    /**
     * \brief Get infrmation of a specific download
     *
     * TBD
     */
    public function GetDownload($sid)
    {
        $smt = $this->mssql->prepare('SELECT * FROM '.$this->edb.'.dbo.site_download WHERE sid = ?');
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');
    
        if(!$smt->execute(array($sid))) throw new DatabaseException('Query failed to execute.');

        $dl = $smt->fetchAll();

        if(count($dl) != 1) return false;

        return $dl[0];
    }

    /**
     * \brief Update a download's information 
     *
     * TBD
     */
    public function updateDownload($sid, $link, $name, $ver, $desc)
    {
        $smt = $this->mssql->prepare('UPDATE '.$this->edb.'.dbo.site_download SET name = ?,version = ?,link = ?,descr = ? WHERE sid = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($name, $ver, $link, $desc, $sid))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Delete a download
     *
     * TBD
     */
    public function deleteDownload($sid)
    {
        $smt = $this->mssql->prepare('DELETE FROM '.$this->edb.'.dbo.site_download WHERE sid = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($sid))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Add a download
     *
     * TBD
     */
    public function addDownload($link, $name, $ver, $desc)
    {
        $smt = $this->mssql->prepare('INSERT INTO '.$this->edb.'.dbo.site_download (link, name, version, descr) VALUES (?,?,?,?)');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($link, $name, $ver, $desc))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Get all events
     *
     * TBD
     */
    public function GetEvents()
    {
        $smt = $this->mssql->query('SELECT * FROM '.$this->edb.'.dbo.event ORDER BY eStart DESC, eEnd DESC');
        
        if(!$smt) throw new DatabaseException('Query failed to execute.');

        $smt = $smt->fetchAll();

        if(count($smt) < 1) return false;

        return array(count($smt), $smt);
    }

    /**
     * \brief Get information on an event
     *
     * TBD
     */
    public function GetEvent($eid)
    {
        $smt = $this->mssql->prepare('SELECT * FROM '.$this->edb.'.dbo.event WHERE eID = ?');
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');
    
        if(!$smt->execute(array($eid))) throw new DatabaseException('Query failed to execute.');

        $event = $smt->fetchAll();

        if(count($event) != 1) return false;

        return $event[0];
    }

    /**
     * \brief Add an event
     *
     * TBD
     */
    public function addEvent($title, $host, $sDate, $eDate, $desc)
    {
        $smt = $this->mssql->prepare('INSERT INTO '.$this->edb.'.dbo.event (eName,eHost,eStart,eEnd,eDesc) VALUES (?,?,?,?,?)');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($title, $host, $sDate, $eDate, $desc))) throw new DatabaseException('Query failed to execute.');

    }

    /**
     * \brief Delete an event
     *
     * TBD
     */
    public function deleteEvent($eid)
    {
        $smt = $this->mssql->prepare('DELETE FROM '.$this->edb.'.dbo.event WHERE eID = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($eid))) throw new DatabaseException('Query failed to execute.');
    }
 
    /**
     * \brief Update an event
     *
     * TBD
     */   
    public function updateEvent($eid, $name, $sdate, $edate, $desc)
    {
        $smt = $this->mssql->prepare('UPDATE '.$this->edb.'.dbo.event SET eName = ?, eStart = ?,eEnd = ?,eDesc = ? WHERE eID = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($name, $sdate, $edate, $desc, $eid))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Get currently active events
     *
     * TBD
     */
    public function showEvents($amount)
    {
        $smt = $this->mssql->prepare('SELECT TOP '.$amount.' * FROM '.$this->edb.'.dbo.event where eEnd > ? ORDER by eStart ASC');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array(date('n/j/Y g:i:s A')))) throw new DatabaseException('Query failed to execute.');

        $events = $smt->fetchAll();

        if(count($events) == 0) return false;

        return array(count($events), $events);
    }

    /**
     * \brief Build extras tables
     */
    public function BuildTables()
    {
        // create sessionlog table
        $smt = $this->mssql->query("IF NOT EXISTS (SELECT name FROM ".$this->edb.".dbo.sysobjects WHERE name = 'sessionlog' and xtype = 'U') CREATE TABLE ".$this->edb.".dbo.sessionlog (wTime datetime, Account varchar(50), IP varchar(50), wAction varchar(50))");
        if(!$smt) throw new DatabaseException('Table creation failed.');
        
        // create news table
        $smt = $this->mssql->query("IF NOT EXISTS (SELECT name FROM ".$this->edb.".dbo.sysobjects WHERE name = 'site_news' and xtype = 'U') CREATE TABLE ".$this->edb.".dbo.site_news (sid int PRIMARY KEY IDENTITY, title varchar (80) null, wroteby varchar (50), wrotedate varchar(50), content text null)");
        if(!$smt) throw new DatabaseException('Table creation failed.');
        
        // create downloads table
        $smt = $this->mssql->query("IF NOT EXISTS (SELECT name FROM ".$this->edb.".dbo.sysobjects WHERE name = 'site_download' and xtype = 'U') CREATE TABLE ".$this->edb.".dbo.site_download (sid int PRIMARY KEY IDENTITY, link varchar (500) null, name varchar(60) null, version varchar(50) null, descr text null )");
        if(!$smt) throw new DatabaseException('Table creation failed.');
        
        // create ban table
        $smt = $this->mssql->query("IF NOT EXISTS (SELECT name FROM ".$this->edb.".dbo.sysobjects WHERE name = 'banned' and xtype = 'U') CREATE TABLE ".$this->edb.".dbo.banned (wDate datetime null, accountname nvarchar (60) default('<none>'), reason varchar (50) default('no reason'), wBy varchar (50) default('<no one>'), type char (10))");
        if(!$smt) throw new DatabaseException('Table creation failed.');

        // create vote table
        $smt = $this->mssql->query("IF NOT EXISTS (SELECT name FROM ".$this->edb.".dbo.sysobjects WHERE name = 'vote' and xtype = 'U') CREATE TABLE ".$this->edb.".dbo.vote (link varchar (100), account varchar (50), ip varchar (50), wDate datetime)");
        if(!$smt) throw new DatabaseException('Table creation failed.');

        // create events table
        $smt = $this->mssql->query("IF NOT EXISTS (SELECT name FROM ".$this->edb.".dbo.sysobjects WHERE name = 'event' and xtype = 'U') CREATE TABLE ".$this->edb.".dbo.event (eID int PRIMARY KEY IDENTITY, eName varchar (50) null, eHost varchar (50), eStart datetime, eEnd datetime, eDesc text null)");
        if(!$smt) throw new DatabaseException('Table creation failed.');

        // create experience banking table
        $smt = $this->mssql->query("IF NOT EXISTS (SELECT name FROM ".$this->edb.".dbo.sysobjects WHERE name = 'blist' and xtype = 'U') CREATE TABLE ".$this->edb.".dbo.blist (auctionID bigint PRIMARY KEY IDENTITY, aid varchar (50) collate Chinese_PRC_CI_AS, exp bigint, coins int default(0))");
        if(!$smt) throw new DatabaseException('Table creation failed.');

        // create userExt table
        $smt = $this->mssql->query("IF NOT EXISTS (SELECT name FROM ".$this->edb.".dbo.sysobjects WHERE name = 'userExt' and xtype = 'U') CREATE TABLE ".$this->edb.".dbo.userExt (user_no varchar (20) collate Chinese_PRC_CI_AS, user_id varchar (20) collate Chinese_PRC_CI_AS, exp bigint default (0), dil bigint default (0), auth varchar(16) null, webName varchar(30) null)");
        if(!$smt) throw new DatabaseException('Table creation failed.');

        // create rebirth table
        $smt = $this->mssql->query("IF NOT EXISTS (SELECT name FROM ".$this->edb.".dbo.sysobjects WHERE name = 'rebirth' and xtype = 'U') CREATE TABLE ".$this->edb.".dbo.rebirth (character_no varchar (18) collate Chinese_PRC_CI_AS, rbtime datetime)");
        if(!$smt) throw new DatabaseException('Table creation failed.');

        // create tickets table
        $smt = $this->mssql->query("IF NOT EXISTS (SELECT name FROM ".$this->edb.".dbo.sysobjects WHERE name = 'tickets' and xtype = 'U') CREATE TABLE ".$this->edb.".dbo.tickets (tid varchar (50) PRIMARY KEY, type varchar (50), owner varchar (50) collate Chinese_PRC_CI_AS, title varchar (20), status int, topen datetime, lby varchar (50) null)");
        if(!$smt) throw new DatabaseException('Table creation failed.');

        // create ticket_post table
        $smt = $this->mssql->query("IF NOT EXISTS (SELECT name FROM ".$this->edb.".dbo.sysobjects WHERE name = 'ticket_post' and xtype = 'U') CREATE TABLE ".$this->edb.".dbo.ticket_post (tid varchar (50), poster varchar (50) collate Chinese_PRC_CI_AS, post text null, rdate datetime)");
        if(!$smt) throw new DatabaseException('Table creation failed.');
    }

    /**
     * \brief Update authorization privileges
     *
     * \param $acct
     *      The account
     * \param $auth
     *      The authorization level
     * \param $webName
     *      The website display name
     */
    public function authUpdate($acct, $auth, $webName)
    {
        if(empty($auth)) $auth = NULL;
        if(empty($webName)) $webName = NULL;

        $smt = $this->mssql->prepare("UPDATE ".$this->edb.".dbo.userExt set auth = ?, webName = ? WHERE user_id = ?");
            
        if(!$smt) throw new DatabaseException('Statement preparation failed.');
        
        if(!$smt->execute(array($auth, $webName, $acct))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Get all accounts based on an authorization level
     *
     * TBD
     */
    public function getAuthAccts($auth = NULL, $info = '*')
    {
        $array = array();
        if($auth === NULL) $auth = ' WHERE auth != NULL';
        else
        {
            $array[] = $auth;
            $auth = ' WHERE auth = ?';
        }

        $smt = $this->mssql->prepare('SELECT '.$info.' FROM '.$this->edb.'.dbo.userExt'.$auth.' ORDER BY auth ASC, user_id ASC');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute($array)) throw new DatabaseException('Query failed to execute.');

        $accts = $smt->fetchAll();

        if(count($accts) < 1) return false;

        return array(count($accts), $accts);
    }
 
    /**
     * \brief Get extended user table information
     *
     * TBD
     */   
    public function getAuthAcct($acct, $info = '*')
    {
        $smt = $this->mssql->prepare('SELECT '.$info.' FROM '.$this->edb.'.dbo.userExt WHERE user_id = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($acct))) throw new DatabaseException('Query failed to execute.');

        $acct = $smt->fetchAll();

        if(count($acct) != 1) return false;

        return $acct;
    }

    /**
     * \brief Get all active authorization levels
     *
     * TBD
     */
    public function getAuths()
    {
        $smt = $this->mssql->query('SELECT DISTINCT auth FROM '.$this->edb.'.dbo.userExt WHERE auth IS NOT NULL');

        if(!$smt) throw new DatabaseException('Query failed to execute.');

        $auths = $smt->fetchAll();

        if(count($auths) < 1) return false;

        return array(count($auths), $auths);
    }

    /**
     * \brief Import non-existing accounts from Account databse in the Extras Database
     */
    public function importExt()
    {
        $smt = $this->mssql->query("select user_no, user_id from account.dbo.user_profile where user_id not in (select user_id from ".$this->edb.".dbo.userExt)");
        
        if(!$smt) throw new DatabaseException('Query failed to execute.');
        
        $smtFetch = $smt->fetchAll();
        
        if(count($smtFetch) > 0)
        {
            foreach($smtFetch as $fetch)
            {
                $this->regExtras($fetch['user_no'], $fetch['user_id']);
            }
        }
    }

    /**
     * \brief Get a character's rebirths
     *
     * \param $charNo
     *      Character number
     * \return
     *      - false: if character does not have any rebirths
     *      - array:
     *          - [0]: count of rebirths
     *          - [1]: array of information
     */
    public function getRebirth($charNo)
    {
        $smt = $this->mssql->prepare('SELECT * FROM '.$this->edb.'.dbo.rebirth WHERE character_no = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($charNo))) throw new DatabaseException('Query failed to execute');

        $rebirths = $smt->fetchAll();

        if(count($rebirths) < 1) return false;

        return array(count($rebirths), $rebirths);
    }

    /**
     * \brief Get account with characters' rebirths
     *
     * \param $userNo
     *      Account number
     * \param $count
     *      Maximum rebirths allowed
     * \return
     *      - false: TBD
     *      - array: TBD
     */
    public function getAcctRebirth($userNo, $count)
    {
        $smt = $this->mssql->prepare("select c.character_name, count(r.character_no) as rebirths from account.dbo.user_profile up left join character.dbo.user_character c on c.user_no = up.user_no left join ".$this->edb.".dbo.rebirth r on c.character_no = r.character_no where up.user_no = ? group by c.character_name having count(r.character_no) < ?");
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($userNo, $count))) throw new DatabaseException('Query failed to execute.');

        $chars = $smt->fetchAll();
        
        if(count($chars) < 1) return false;

        return array(count($chars), $chars);
    }

    /**
     * \brief Add a rebirth to a character
     *
     * TBD
     */
    public function addRebirth($charNo)
    {
        $smt = $this->mssql->prepare('INSERT INTO '.$this->edb.'.dbo.rebirth VALUES (?,?)');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($charNo, date('n/j/Y g:i:s A')))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Update dil in dil bank
     *
     * TBD
     */
    public function updateDil($userNo, $amount, $type = 0)
    {
        if($type == 0) $type = '+';
        else $type = '-';

        $smt = $this->mssql->prepare('UPDATE '.$this->edb.'.dbo.userExt SET dil = dil '.$type.' ? where user_no = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($amount, $userNo))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Update experience in experience bank
     *
     * TBD
     */
    public function updateExp($userNo, $amount, $type = 0)
    {
        if($type == 0) $type = '+';
        else $type = '-';

        $smt = $this->mssql->prepare('UPDATE '.$this->edb.'.dbo.userExt SET exp = exp '.$type.' ? where user_no = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($amount, $userNo))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Add a listing
     *
     * TBD
     */
    public function addListing($userNo, $exp, $coins)
    {
        $smt = $this->mssql->prepare('INSERT INTO '.$this->edb.'.dbo.blist (aid, exp, coins) values (?,?,?)');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($userNo, $exp, $coins))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Delete a listing
     *
     * TBD
     */
    public function deleteListing($lid)
    {
        $smt = $this->mssql->prepare('DELETE FROM '.$this->edb.'.dbo.blist where auctionID = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($lid))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Get all listings based on a user number
     *
     * TBD
     */
    public function getListings($userNo, $type = 0)
    {
        if($type == 0) $type = '!=';
        else $type = '=';

        $smt = $this->mssql->prepare('SELECT * FROM '.$this->edb.'.dbo.blist WHERE aid '.$type.' ?');
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($userNo))) throw new DatabaseException('Query failed to execute');

        $listings = $smt->fetchAll();

        if(count($listings) < 1) return false;

        return array(count($listings), $listings);
    }

    /**
     * \brief Get an experience listing information based on a user number
     *
     * TBD
     */
    public function getListing($aid, $userNo, $type = 0)
    {
        if($type == 0) $type = '!=';
        else $type = '=';
        
        $smt = $this->mssql->prepare('SELECT * FROM '.$this->edb.'.dbo.blist WHERE aid '.$type.' ? and auctionID = ?');
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($userNo, $aid))) throw new DatabaseException('Query failed to execute');

        $listing = $smt->fetchAll();

        if(count($listing) != 1) return false;

        return $listing[0];
    }

    /**
     * \brief Get the last vote information of a site of a user
     *
     * TBD
     */
    public function getLastVote($userID, $ip, $site)
    {
        $smt = $this->mssql->prepare('SELECT top 1 * FROM '.$this->edb.'.dbo.vote WHERE (ip = ? or account = ?) and link = ? order by wDate desc');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($ip, $userID, $site))) throw new DatabaseException('Query failed to execute');

        $vote = $smt->fetchAll();

        if(count($vote) == 0) return false;

        return $vote[0];
    }

    /**
     * \brief Record a vote in the voting logs
     *
     * TBD
     */
    public function addVote($userID, $site, $ip)
    {
        $smt = $this->mssql->prepare('INSERT INTO '.$this->edb.'.dbo.vote VALUES (?,?,?,?)');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($site, $userID, $ip, date('n/j/Y g:i:s A')))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Get all tickets based on an account number
     *
     * TBD
     */
    public function getTickets($userNo)
    {
        $smt = $this->mssql->prepare('select t.tid, type, user_id, status, title, rdate, (case when poster = t.owner then \'You\' else poster end) as poster from '.$this->edb.'.dbo.tickets t join (select tid, poster,rdate from '.$this->edb.'.dbo.ticket_post tp) tp on tp.tid = t.tid join account.dbo.user_profile a on a.user_no = t.owner where rdate = (select max(rdate) from '.$this->edb.'.dbo.ticket_post where tid = t.tid) and t.owner = ? order by rdate desc');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($userNo))) throw new DatabaseException('Query failed to execute.');

        $tickets = $smt->fetchAll();

        if(count($tickets) < 1) return false;

        return array(count($tickets), $tickets);
    }

    /**
     * \brief Get a ticket
     *
     * TBD
     */
    public function getTicket($tid, $userNo)
    {
        $smt = $this->mssql->prepare('select (case when poster = owner then \'You\' else poster end) as poster, owner, post, rdate from '.$this->edb.'.dbo.ticket_post join '.$this->edb.'.dbo.tickets on '.$this->edb.'.dbo.tickets.tid = '.$this->edb.'.dbo.ticket_post.tid where owner = ? and '.$this->edb.'.dbo.ticket_post.tid = ? order by rdate asc');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($userNo, $tid))) throw new DatabaseException('Query failed to execute.');

        $ticket = $smt->fetchAll();

        if(count($ticket) < 1) return false;

        $smt = $this->mssql->prepare('SELECT status FROM '.$this->edb.'.dbo.tickets where tid = ? and owner = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($tid, $userNo))) throw new DatabaseException('Query failed to execute.');
        
        $status = $smt->fetchAll();
        
        return array(count($ticket), $ticket, $status[0]['status']);
    }

    /**
     * \brief Add a ticket
     *
     * TBD
     */
    public function addTicket($tid, $type, $owner, $title)
    {
        $smt = $this->mssql->prepare('INSERT INTO '.$this->edb.'.dbo.tickets (tid, type, owner, title, status, topen) values (?,?,?,?,\'1\',?)');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($tid, $type, $owner, $title,date('n/j/Y g:i:s A')))) throw new DatabaseException('Query failed to execute.');

    }

    /**
     * \brief Reply to a ticket
     *
     * TBD
     */
    public function replyTicket($tid, $poster, $post)
    {
        $smt = $this->mssql->prepare('INSERT INTO '.$this->edb.'.dbo.ticket_post (tid, poster, post, rdate) values (?,?,?,?)');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($tid, $poster, $post, date('n/j/Y g:i:s A')))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Delete a ticket
     *
     * TBD
     */
    public function deleteTicket($tid)
    {
        $smt = $this->mssql->prepare('DELETE FROM '.$this->edb.'.dbo.tickets where tid = ?; DELETE FROM '.$this->edb.'.dbo.ticket_post where tid = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($tid, $tid)))throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Get tickets for tickets controller
     *
     * TBD
     */
    public function manageGetTickets($category, $status = 1, $sortBy = 0)
    {
        if($status !=0 && $status !=-1) $status = 1;

        if($sortBy == 0) $sortBy = 'topen ASC';
        elseif ($sortBy == 1) $sortBy = 'rdate DESC';
        else $sortBy = 'topen DESC';

        $smt = $this->mssql->prepare('select t.tid, user_id, title, lby, rdate, (case when poster = t.owner then user_id else poster end) as poster from '.$this->edb.'.dbo.tickets t join (select tid, poster,rdate from '.$this->edb.'.dbo.ticket_post tp) tp on tp.tid = t.tid join account.dbo.user_profile a on a.user_no = t.owner where rdate = (select max(rdate) from '.$this->edb.'.dbo.ticket_post where tid = t.tid) and type = ? and status = ? ORDER BY '.$sortBy);
    
        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($category, $status))) throw new DatabaseException('Query failed to execute.');

        $tickets = $smt->fetchAll();

        if(count($tickets) < 1) return false;

        return array(count($tickets), $tickets);
    }

    /**
     * \brief Get tickets for ticket management controller
     *
     * TBD
     */
    public function manageGetTicket($tid)
    {
        $smt = $this->mssql->prepare('select type, status from '.$this->edb.'.dbo.tickets where tid = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($tid))) throw new DatabaseException('Query failed to execute.');

        $info = $smt->fetchAll();

        if(count($info) != 1) return false;

        $smt = $this->mssql->prepare('select case when poster = owner then user_id else poster end as poster, owner, post, rdate, user_id from '.$this->edb.'.dbo.ticket_post tp join '.$this->edb.'.dbo.tickets t on t.tid = tp.tid join account.dbo.user_profile a on a.user_no = t.owner where tp.tid = ? order by rdate asc');
        
        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($tid))) throw new DatabaseException('Query failed to execute.');

        $ticket = $smt->fetchAll();

        if(count($ticket) < 1) return false;

        return array(count($ticket), $ticket, $info[0]['status'], $info[0]['type']);
    }

    /**
     * \brief Get the information of the latest created ticket of a user 
     *
     * TBD
     */
    public function lastOpen($owner)
    {
        $smt = $this->mssql->prepare('select top 1 topen from '.$this->edb.'.dbo.tickets where owner = ? order by topen desc');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($owner))) throw new DatabaseException('Query failed to execute.');

        $open = $smt->fetchAll();

        if(count($open) == 0) return false;

        return $open[0]['topen'];
    }
 
    /**
     * \brief Get the last reply on a ticket
     *
     * TBD
     */   
    public function lastReply($owner, $tid)
    {
        $smt = $this->mssql->prepare('SELECT TOP 1 rdate FROM '.$this->edb.'.dbo.ticket_post WHERE poster = ? AND tid = ? ORDER BY rdate DESC');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($owner, $tid))) throw new DatabaseException('Query failed to execute.');

        $reply = $smt->fetchAll();

        if(count($reply) == 0) return false;

        return $reply[0]['rdate'];
    }
 
    /**
     * \brief Update a ticket's status
     *
     * TBD
     */   
    public function updateStatus($tid, $status)
    {
        $smt = $this->mssql->prepare('update '.$this->edb.'.dbo.tickets set status = ? where tid = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($status, $tid))) throw new DatabaseException('Query failed to execute.');
    }

    /**
     * \brief Get ban logs based on time span
     *
     * TBD
     */
    public function getBanLogs($stime, $etime, $searchBy = 0)
    {
        if($searchBy == 0) $searchBy = 'b';
        else $searchBy = 'u';

        $smt = $this->mssql->prepare('SELECT wDate, accountname, reason, wBy FROM '.$this->edb.'.dbo.banned where type = ? and wDate >= convert(datetime,?) and wDate <= convert(datetime,?) order by wDate desc');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($searchBy, $stime, $etime))) throw new DatabaseException('Query failed to execute.');;

        $logs = $smt->fetchAll();

        if(count($logs) < 1) return false;

        return array(count($logs), $logs);
    }

    /**
     * \brief Record ban action into ban logs
     *
     * TBD
     */
    public function banAcct($userID, $reason, $banBy)
    {
        $smt = $this->mssql->prepare('INSERT INTO '.$this->edb.'.dbo.banned (wDate, accountname, reason, wBy, type) VALUES (?,?,?,?,\'b\')');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array(date('n/j/Y g:i:s A'), $userID, $reason, $banBy)));
    }

    /**
     * \brief Record unban action into unban logs
     *
     * TBD
     */
    public function unbanAcct($userID, $unbanBy)
    {
        $smt = $this->mssql->prepare('UPDATE '.$this->edb.'.dbo.banned SET type = \'u\', wBy = ?, wDate = ? WHERE type = \'b\' AND accountname = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($unbanBy, date('n/j/Y g:i:s A'), $userID)));
    }
}
?>
