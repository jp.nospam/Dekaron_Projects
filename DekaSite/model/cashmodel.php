<?php
/*
    Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is furnished to do
    so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

/**
 * \brief Cash database model
 */
class CashModel
{
    /**
     * \var $args
     * \brief An array of the arguments passed to the class.
     *      
     * Reserved indexes:
     * - [0]: The host IP address or domain name of the database.
     * - [1]: The username to connect to the database with.
     * - [2]: The password to use for the username provided
     */
    private $args;
    /**
     * \var $mssql
     * \brief A PDO connection object
     */
    private $mssql;

    /**
     * \brief Class constructor
     *
     * This constructor establishes a connection to the datatabase. It
     * dynamically gets arguments via func_get_args() and stores them in $args.
     */
    function __construct()
    {
        $this->args =& func_get_args();

        global $con;

        if (!isset($con))
        {
            try
            {
                if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
                {
                    $con = new PDO('odbc:Driver={SQL Server};server='.$this->args[0].';uid='.$this->args[1].';pwd='.$this->args[2].';');
                }
                else
                {
                    $con = new PDO('dblib:host='.$this->args[0].':1433', $this->args[1], $this->args[2]);
                }
            }
            catch (PDOException $e)
            {
                throw new DatabaseException('Could not connect to the database.');    
            }
        }

        $this->mssql =& $con;
    }

    /**
     * \brief Get D-Coins from an account
     *
     * \param $userNo
     *      The user number to search by.
     * \return
     *      - false: if account hasn't visited d-shop
     *      - Amount of coins.
     */
    public function getCoins($userNo)
    {

        $smt = $this->mssql->prepare("SELECT (amount + free_amount) as coins FROM cash.dbo.user_cash where user_no = ?");

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($userNo))) throw new DatabaseException('Query failed to execute.');

        $user = $smt->fetchAll();
        
        if(count($user) != 1) return false;
        
        return $user[0]['coins'];
    }
    
    /**
     * \brief Update d-coins for an account
     *
     * \param $userNo
     *      The user's number
     * \param $amount
     *      The amount of coins to add or subtract
     * \param $type
     *      (optional) Add or subtract coins.
     *      - 0: (default) Adds coins
     *      - All other integers: Subtracts coins
     */
    public function updateCoins($userNo, $amount, $type = 0)
    {
        if($type == 0) $type = '+';
        else $type = '-';

        $smt = $this->mssql->prepare('UPDATE cash.dbo.user_cash SET amount = amount '.$type.' ? where user_no = ?');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute(array($amount, $userNo))) throw new DatabaseException('Query failed to execute');
    }

    /**
     * \brief Get items bought from cash shop based on a time span
     *
     * \param $stime
     *      Starting time
     * \param $etime
     *      Ending time
     */
    public function getCashItems($stime = NULL, $etime = NULL)
    {
        $array = array();
        
        $search = ' ';

        if($stime !== NULL && $etime !== NULL)
        {
            $search = ' WHERE intime >= convert(datetime, ?) AND intime <= convert(datetime, ?) ';

            $array = array($stime, $etime);
        }

        $smt = $this->mssql->prepare('SELECT character_name,product,intime FROM cash.dbo.user_use_log'.$search.'ORDER BY intime DESC');

        if(!$smt) throw new DatabaseException('Statement preparation failed.');

        if(!$smt->execute($array)) throw new DatabaseException('Query failed to execute');
        
        $items = $smt->fetchAll();

        if(count($items) < 1) return false;

        return array(count($items), $items);
    }
}
?>
