<a href="{$thisPage}">Ban Account</a><a href="{$thisPage}log/">Ban Logs</a>

{if !isset($log)}
<form method="post" action="{$thisPage}">
    <div>
        <select name="type">
            <option value="char">Character Name</option>
            <option value="acct">Account Name</option>
        </select>
        <input type="text" name="data" /><br/>
        <label>Reason: </label><input type="text" name="reason" /><br/>
        <input type="submit" name="ban" value="Ban Account" />
    </div>
</form>
{else}
<br/>If no end time is given, it will use the current time.
<form action="{$thisPage}log/" method="post">
    <div>
        <input type="radio" name="type" value="ban" checked="checked" /><label>Bans </label><input type="radio" name="type" value="unban" /><label>Unbans</label><br/>
        Start time: <input type="text" name="stime" /><br/>
        End time: <input type="text" name="etime" /><br/>
        <input type="submit" name="search" value="Search" />
    </div>
</form>
{if isset($logs)}
{$stime|date_format:"%b %e %Y %H:%M"} - {$etime|date_format:"%b %e %Y %H:%M"} 
<table>
    <tr>
        <th>Date</th>
        <th>Account</th>
        {if isset($typeBan)}<th>Reason</th>{/if}
        <th>Issued By</th>
    </tr>
    {foreach $logs as $log}
    <tr>
        <td>{$log.wDate|date_format:"%b %e %Y %H:%M"}</td>
        <td>{if isset($typeBan)}<a href="{$thisPage}unban/{$log.accountname}/">{/if}{$log.accountname}{if isset($typeban)}</a>{/if}</td>
        {if isset($typeBan)}<td>{$log.reason}</td>{/if}
        <td>{$log.wBy}</td>
    </tr>
    {/foreach}
</table>
{/if}
{/if}
