If no end time is given, it will use the current time.
<form method="post" action="{$thisPage}">
<div>
    <label>Start time: </label><input type="text" name="stime" /><br/>
    <label>End time: </label><input type="text" name="etime" /><br/>
    <input type="submit" name="search" value="Search" />
</div>
</form>
{if isset($items)}
{$stime|date_format:"%b %e %Y %H:%M"} - {$etime|date_format:"%b %e %Y %H:%M"}
<table>
    <tr>
        <th>Character Name</th>
        <th>Item Name</th>
        <th>Date</th>
    </tr>
    {foreach $items as $item}
    <tr>
        <td>{$item.character_name}</td>
        <td>{$item.product}</td>
        <td>{$item.intime|date_format:"%b %e %Y %H:%M"}</td>
    </tr>
    {/foreach}
</table>
{/if}
