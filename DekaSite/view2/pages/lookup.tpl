{if !isset($type)}{assign 'type' 'account'}{/if}
<form method="get" action="{$thisPage}">
    <div>
        <select name="type">
            <option value="account" {if $type == 'account'}selected="selected"{/if}>Account</option>
            <option value="ip" {if $type == 'ip'}selected="selected"{/if}>IP</option>
            <option value="char" {if $type == 'char'}selected="selected"{/if}>Character</option>
        </select>
        <input name="data" type="text" /><br/>
        <input type="submit" value="Search!" />
    </div>
</form>
<hr/>

{if isset($lAccts)}
<form method="get" action="{$thisPage}"> 
    <div>
        <label>Account links:</label><br/>
        <input type="hidden" name="type" value="account" />
        <select name="data" size="15">
            {foreach $lAccts as $account}
            <option value="{$account}">{$account}</option>
            {/foreach}
        </select><br/>
        <input type="submit" value="Search!" />
    </div>
</form>
{/if}

{if isset($chars)}
<form method="get" action="{$thisPage}">
    <div>
        <label>Characters:</label><br/>
        <input type="hidden" name="type" value="char" />
        <select name="data" size="6">
            {foreach $chars as $achar}
            <option value="{$achar.character_name}">{$achar.character_name}</option>
            {/foreach}
        </select><br/>
        <input type="submit" value="Search!" />
    </div>
</form>
{/if}

{if isset($IPs)}
<form method="get" action="{$thisPage}">
    <div>
       <label>IP links:</label><br/>
        <input type="hidden" name="type" value="ip" />
        <select name="data" size="15">
            {foreach $IPs as $ip}
            <option value="{$ip.IP}">{$ip.IP}</option>
            {/foreach}
        </select><br/>
        <input type="submit" value="Search!" />
    </div>
</form>
{/if}

{if isset($char)}
<div>
    Account: <a href="{$thisPage}?type=account&amp;data={$char.user_id}">{$char.user_id}</a><br/>
    Character: {$char.character_name}<br/>
    Money: {$char.dwMoney}<br/>
    Storage money: {$char.dwStorageMoney}<br/>
    Store money: {$char.dwStoreMoney}<br/>
    Class: 
    {if $char.byPCClass == '0'}Knight{/if}
    {if $char.byPCClass == '1'}Hunter{/if}
    {if $char.byPCClass == '2'}Mage{/if}
    {if $char.byPCClass == '3'}Summoner{/if}
    {if $char.byPCClass == '4'}Segnale{/if}
    {if $char.byPCClass == '5'}Bagi{/if}
    {if $char.byPCClass == '6'}Aloken{/if}<br/>
    Level: {$char.wLevel}<br/>
    Shield: {$char.nShield}<br/>
    HP: {$char.nHP}<br/>
    MP: {$char.nMP}<br/>
    Stat points: {$char.wStatPoint}<br/>
    Str: {$char.wStr}<br/>
    Dex: {$char.wDex}<br/>
    Con: {$char.wCon}<br/>
    Spr: {$char.wSpr}<br/>
    Skill points: {$char.wSkillPoint}<br/>
    PKs: {$char.wPKCount}<br/>
    PVP Points: {$char.dwPVPPoint}<br/>
    Wins: {$char.wWinRecord}<br/>
    Losses: {$char.wLoseRecord}
</div>
{/if}
