<form  method="post" action="{$thisPage}">
    <div>
        <label>Character: </label><input type="text" name="char" /> <input type="submit" name="search" value="Search"/>
    </div>
</form>
<hr/>
{if isset($char)}
<form method="post" action="{$thisPage}">
    <div>
        <label>Account: </label><input type="text" name="acct" value="{$char.user_id}" /><br/>
        <label>Character: </label><input type="text" name="char" value="{$char.character_name}"/><input type="hidden" name="oChar" value="{$char.character_name}"/><br/>
        <label>Money: </label><input type="text" name="dwMoney" value="{$char.dwMoney}"/><br/>
        <label>Storage money: </label><input type="text" name="dwStorageMoney" value="{$char.dwStorageMoney}"/><br/>
        <label>Store money: </label><input type="text" name="dwStoreMoney" value="{$char.dwStoreMoney}"/><br/>
        <label>Class: </label><select name="byPCClass">
            <option value="0" {if $char.byPCClass == '0'}selected="selected"{/if}>Knight</option>
            <option value="1" {if $char.byPCClass == '1'}selected="selected"{/if}>Hunter</option>
            <option value="2" {if $char.byPCClass == '2'}selected="selected"{/if}>Mage</option>
            <option value="3" {if $char.byPCClass == '3'}selected="selected"{/if}>Summoner</option>
            <option value="4" {if $char.byPCClass == '4'}selected="selected"{/if}>Segnale</option>
            <option value="5" {if $char.byPCClass == '5'}selected="selected"{/if}>Bagi</option>
            <option value="6" {if $char.byPCClass == '6'}selected="selected"{/if}>Aloken</option>
        </select><br/>
        <label>Level: </label><input type="text" name="wLevel" value="{$char.wLevel}"/><br/>
        <label>Shield: </label><input type="text" name="nShield" value="{$char.nShield}"/><br/>
        <label>HP: </label><input type="text" name="nHP" value="{$char.nHP}"/><br/>
        <label>MP: </label><input type="text" name="nMP" value="{$char.nMP}"/><br/>
        <label>Stat points: </label><input type="text" name="wStatPoint" value="{$char.wStatPoint}"/><br/>
        <label>Str: </label><input type="text" name="wStr" value="{$char.wStr}"/><br/>
        <label>Dex: </label><input type="text" name="wDex" value="{$char.wDex}"/><br/>
        <label>Con: </label><input type="text" name="wCon" value="{$char.wCon}"/><br/>
        <label>Spr: </label><input type="text" name="wSpr" value="{$char.wSpr}"/><br/>
        <label>Skill points: </label><input type="text" name="wSkillPoint" value="{$char.wSkillPoint}"/><br/>
        <label>PKs: </label><input type="text" name="wPKCount" value="{$char.wPKCount}"/><br/>
        <label>PVP Points: </label><input type="text" name="dwPVPPoint" value="{$char.dwPVPPoint}"/><br/>
        <label>Wins: </label><input type="text" name="wWinRecord" value="{$char.wWinRecord}"/><br/>
        <label>Losses: </label><input type="text" name="wLoseRecord" value="{$char.wLoseRecord}"/><br/>
        <input type="submit" name="update" value="Update"></input>
    </div>
</form>
{/if}
