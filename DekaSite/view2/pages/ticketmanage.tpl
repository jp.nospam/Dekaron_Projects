{if isset($categories)}
{if !isset($cCat)}{assign 'cCat' '0'}{/if}
<form action="{$thisPage}" method="post">
    <div>
        <label>Category: </label><select name="category">
            {foreach $categories as $cat}
            <option value="{$cat}" {if $cCat == $cat}selected="selected"{/if}>{$cat}</option>
            {/foreach}
        </select><br/>
        {if !isset($status)}{assign 'status' '1'}{/if}
        <label>Status: </label><input type="radio" name="status" value="1" {if $status == '1'}checked{/if} />Open   <input type="radio" name="status" value="0" {if $status == '0'}checked{/if} />Closed   <input type="radio" name="status" value="-1" {if $status == '-1'}checked{/if} />Locked<br/>
        {if !isset($order)}{assign 'order' '0'}{/if}
        <label>Sort: </label><input type="radio" name="order" value="0" {if $order=='0'}checked{/if} />Newest reply   <input type="radio" name="order" value="1" {if $order=='1'}checked{/if} />Newest ticket   <input type="radio" name="order" value="2" {if $order=='2'}checked{/if} />Oldest ticket<br />
        <input type="submit" name="search" value="Search" />
    </div>
</form>
<hr>
{/if}
{if isset($tickets)}
<table>
    <tr>
        <th>By</th>
        <th>Ticket</th>
        <th>Last Reply</th>
        {if $status == '-1'}<th>Locked By</th>{/if}
    </tr>
    {foreach $tickets as $ticket}
    <tr>
        <td>{$ticket.user_id}</td>
        <td><a href="{$thisPage}view/{$ticket.tid}/">[{$ticket.title|escape:'html'}]</a></td>
        <td>{$ticket.poster}<br/>{$ticket.rdate|date_format:"%b %e %Y %H:%M"}</td>
        {if $status == '-1'}<td>{$ticket.lby}</td>{/if}
    </tr>
    {/foreach}
</table>
{/if}
{if isset($view)}
{if isset($reply)}
    {foreach $reply as $post}
    <div>
        {$post.poster}<br/>
        {$post.rdate|date_format:"%b %e %Y %H:%M"}<br/>
        {$post.post|escape:'html'|nl2br}
    </div>
    {/foreach}
    <form method="post">
        <div>
            <textarea cols="75" rows="10" name="reply"></textarea><br/>
            <input type="submit" value="Reply" name="rsub"><br/><br/>
            {if $status == 1}<input type="submit" name="close" value="Close" />{/if}
            {if $status == 0}<input type="submit" name="open" value="Open" />   <input type="submit" name="lock" value="Lock" />{/if}
            {if $status == -1}<input type="submit" name="unlock" value="Unlock" />{/if}
            {if isset($delete)}<input type="submit" name="delete" value="Delete" />{/if}
        </div>
    </form>
{/if}
{/if}
