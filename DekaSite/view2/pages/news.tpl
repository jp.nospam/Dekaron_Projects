{if isset($pagination)}Page: {include 'contentPgs.tpl'}<br/><br/>{/if}
{if isset($news)}
{foreach $news as $post}
<div>
    {$post.title|escape:'html'}<br/>
    Written by {$post.wroteby} at {$post.wrotedate}<br/>
    {$post.content|escape:'html'|nl2br}
</div><br/>
{/foreach}
{/if}
