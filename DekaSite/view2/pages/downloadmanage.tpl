{if isset($type)}

{if $type == 'add'}
<form method="post" action="{$thisPage}add/">
    <div>
        <label>Title: </label><input type="text" name="name" /><br/>
        <label>Version: </label><input type="text" name="ver" /><br/>
        <label>Link: </label><input type="text" name="link" /><br/><br/>
        <textarea cols="75" rows="25" name="desc"></textarea><br/>
        <input type="submit" name="create" value="Create" />
    </div>
</form>
{/if}

{if $type == 'edit'}
<form method="post" action="{$thisPage}edit/{$download.sid}/">
    <div>
        <label>Title: </label><input type="text" name="name" value="{$download.name}" /><br/>
        <label>Version: </label><input type="text" name="ver" value="{$download.version}" /><br/>
        <label>Link: </label><input type="text" name="link" value="{$download.link}" /><br/><br/>
        <textarea cols="75" rows="25" name="desc">{$download.descr}</textarea><br/>
        <input type="submit" name="edit" value="Edit" /> <input type="submit" name ="delete" value="Delete" />
    </div>
</form>
{/if}

{else}
<a href="{$thisPage}add/">Add Download</a><br/>
{if isset($downloads)}
<table>
    <tr>
        <th>Title</th>
        <th>Version</th>
    </tr>
    {foreach $downloads as $dl}
    <tr>
        <td><a href="{$thisPage}edit/{$dl.sid}/">[{$dl.name}]</a></td>
        <td>{$dl.version}</td>
    </tr>
    {/foreach}
</table>
{/if}

{/if}
