{if isset($lMsg)}
{* Login messages *}
{if $lMsg == 1}Please enter a username and password.
{elseif $lMsg == 2}Invalid password and/or username
{/if}
{/if}

{if isset($msg)}
{* All other messages *}
{if $msg == 1}Please fill in all fields.

{elseif $msg == 2 }New passwords do not match.

{elseif $msg == 3 }Password must be at least 3 characters long

{elseif $msg == 4 }Account collision! Please inform the administrator.

{elseif $msg == 5 }Current password does not match.

{elseif $msg == 6 }Password successfully changed.

{elseif $msg == 7 }Passwords do not match.

{elseif $msg == 8 }Account name must be at least 3 characters.

{elseif $msg == 9 }Account already exists.

{elseif $msg == 10 }Registration successful.

{elseif $msg == 11 }No downloads available. Please check back later.

{elseif $msg == 12 }No news has been posted.

{elseif $msg == 13 }Sorry, the page you're looking for could not be found. 

{elseif $msg == 14}Extras content needs to be enabled to display this page.

{elseif $msg == 15}No characters were found on this account.

{elseif $msg == 16}Character not found on this account.

{elseif $msg == 17}You do not have enough dil.

{elseif $msg == 18}You have not visited the d-shop.

{elseif $msg == 20}Account is online.

{elseif $msg == 21}You do not have enough D-coins.

{elseif $msg == 22}Please disequip all items.

{elseif $msg == 23}Class successfully changed.

{elseif $msg == 24}Class not found.

{elseif $msg == 25}The rebirth system has not been setup.

{elseif $msg == 26}Character has hit maximum rebirths.

{elseif $msg == 27}Rebirth successful!

{elseif $msg == 28}Please logout of the game.

{elseif $msg == 29}Character does not meet level requirements.

{elseif $msg == 30}This account does not have any characters to rebirth with.

{elseif $msg == 31}Sorry no ranks are available for this section yet.

{elseif $msg == 32}Buying dil for coins is disabled.

{elseif $msg == 33}Buying coins for dil is disabled.

{elseif $msg == 34}Dil must be greater than 0.

{elseif $msg == 35}You cannot withdraw more than 1 billion dil.

{elseif $msg == 36}You do not have any dil withdraw.

{elseif $msg == 37}You do not have any dil to deposit.

{elseif $msg == 38}Dil successfully withdrawn.

{elseif $msg == 39}Dil sucessfully desposited.

{elseif $msg == 40}You do not have enough coins to buy that much dil.

{elseif $msg == 41}Dil successfully bought.

{elseif $msg == 42}You do not have enough dil to buy that many coins.

{elseif $msg == 43}Coins successfully bought.

{elseif $msg == 44}Experience must be greater than 0.

{elseif $msg == 45}You cannot withdraw more than 2147483648 experience.

{elseif $msg == 46}You do not have any experience withdraw.

{elseif $msg == 47}You do not have any experience to deposit.

{elseif $msg == 48}Experience successfully withdrawn.

{elseif $msg == 49}Experience sucessfully desposited.

{elseif $msg == 50}You do not have that much experience.

{elseif $msg == 51}You do not have enough coins.

{elseif $msg == 52}Character does not exist.

{elseif $msg == 53}Experience sent successfully.

{elseif $msg == 54}Minimum listing price not met.

{elseif $msg == 55}Experience listed successfully.

{elseif $msg == 56}No listings currently available.

{elseif $msg == 57}You cannot buy that listing because you do not have enough coins.

{elseif $msg == 58}You cannot buy that listing because you cannot hold that much experience.

{elseif $msg == 59}Listing does not exist.

{elseif $msg == 60}Listing successfully deleted.

{elseif $msg == 61}Listing successfully bought.

{elseif $msg == 62}You cannot buy that listing because the owner cannot hold that many coins.

{elseif $msg == 63}You cannot delete that listing because you cannot hold that much experience.

{elseif $msg == 64}There are no voting links up yet. Please try again later.

{elseif $msg == 65}Voting link not found.

{elseif $msg == 66}You've already voted there.<br/>Please try again on {$date}.

{elseif $msg == 67}Non-existing tables built. You may enable extras features.

{elseif $msg == 68}You do not have any submitted tickets.

{elseif $msg == 69}Ticket does not exist.

{elseif $msg == 70}Please wait at least {$sec} second(s) before submitting a new ticket.

{elseif $msg == 71}Ticket is closed.

{elseif $msg == 72}Ticket is locked.

{elseif $msg == 73}Your group cannot submit new tickets.

{elseif $msg == 74}Ticket submitted successfully.

{elseif $msg == 75}Invalid category.

{elseif $msg == 76}Please wait at least {$sec} second(s) before posting again.

{elseif $msg == 77}News successfully added.

{elseif $msg == 78}News story not found.

{elseif $msg == 79}News story deleted.

{elseif $msg == 80}News story successfully updated.

{elseif $msg == 81}Download successfully added.

{elseif $msg == 82}Download not found.

{elseif $msg == 83}Download deleted.

{elseif $msg == 84}Download successfully updated.

{elseif $msg == 85}Event successfully added.

{elseif $msg == 86}Event not found.

{elseif $msg == 87}Event deleted.

{elseif $msg == 88}Event successfully updated.

{elseif $msg == 89}End date cannot be earlier than starting date.

{elseif $msg == 90}No upcoming events posted.

{elseif $msg == 91}There aren't any characters online at the moment.

{elseif $msg == 92}No items were found for the time period specified.

{elseif $msg == 93}Character not found.

{elseif $msg == 94}{$char} was given {$add} coin(s) and had {$sub} coin(s) taken.

{elseif $msg == 95}Mail sent successfully.

{elseif $msg == 96}Account does not exist.

{elseif $msg == 97}Character name in use.

{elseif $msg == 98}Character modified successfully.

{elseif $msg == 99}Account is already banned.

{elseif $msg == 100}Unknown error.

{elseif $msg == 101}Account successfully banned.

{elseif $msg == 102}No ban/unbans were found during the time period specified.

{elseif $msg == 103}Account is not banned.

{elseif $msg == 104}Account successfully unbanned.

{elseif $msg == 105}You are not allowed to manage tickets.

{elseif $msg == 106}No tickets available.

{elseif $msg == 107}Ticket deleted.

{elseif $msg == 108}IP not found.
{/if}
{/if}
