<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class events extends BaseController
{
    private $extsMdl;

    public function __construct(&$config, &$args)
    {
        parent::__construct($config, $args);

        $this->view = new SmartyView();
        $this->view->SetTemplate('events.tpl');
        
        $this->extsMdl = new ExtrasModel($config['MSSQL']['host'], $config['MSSQL']['user'], $config['MSSQL']['password'], $config['MSSQL']['extrasDB']); 

        $this->actions = array('default' => 'showEvents');
    }

    public function showEvents()
    {
        
        if($this->config['MSSQL']['extras'] != true)
        {
            $this->view->SetVar('msg', 14);
            $this->view->Draw();
            return;
        }

        $this->view->SetVar('bp', $this->config['site']['basepath']);

        $events = $this->extsMdl->showEvents($this->config['events']['amount']);

        if($events === false)
        {
            $this->view->SetVar('msg', 90);

            $this->view->Draw();

            return;
        }
        
        if(isset($this->args[1])) $pagination = new Pagination($events[0], $this->config['events']['amountPage'], (int)$this->args[1], '{page}/');
        else $pagination = new Pagination($events[0], $this->config['events']['amountPage'], 1, '{page}/');

        $limits = $pagination->GetLimits();

        $list = $pagination->GetList();

        if(count($list) > 1)$this->view->SetVar('pagination', $list);

        $events = array_slice($events[1], $limits[0], $limits[1], true);
        
        $this->view->SetVar('events', $events);

        $this->view->Draw();
    }
}
?>
