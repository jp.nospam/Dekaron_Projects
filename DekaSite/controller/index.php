<?php
/*!
 * \mainpage DekaSite
 *
 * \section intro_sec Introduction
 * DekaSite is a content management system (CMS) for private Dekaron servers.
 * It uses the Frameless framework and utilizes it's native support for the
 * Smarty template engine.
 *
 * \section notice NOTICE
 * Currently there is a bug with the newer PDO ODBC library for windows so 
 * DekaSite only works 100% on linux with DBlib until further notice.
  *
 * \subpage install "Installation"
 *
 * \subpage copyright "DekaSite Licensing"
 *
 * \section issues_sec Issues
 * If you have problems setting this up, feel free to post or PM me on
 * <a href="http://dkunderground.org">DKUnderground</a>. My username is Oishi.
 * Or you can PM me on Gitorious.
 */

 /*!
 * \page install Installation
 *
 * \section require_sec Requirements
 * Apache with .htaccess, rewrite, and PHP modules enabled. If you're using
 * windows you'll need PDO ODBC library enabled, or if you're using linux, you'll
 * need PDO DBLIB (linux) extension enabled.
 *
 * \section setup_sec Setup
 * - Copy contents to your webserver's public (A.K.A htdocs) folder.
 * - If dekasite is placed in a sub-folder of your htdocs edit the .htaccess file
 * in this folder accordingly.
 * - Read and edit data/config.ini accordingly.
 *
 * \section extras_sec Adding extras features
 * - Create a database with the same name specified by extrasDB setting in
 * config.ini. The specified user also needs to be able to have same privileges as
 * the other dekaron tables require.
 * - Uncomment (remove ;) from the update tables page in the white list.
 * - Visit "update tables" page in browser and follow directions from there.
 *
 */

 /*!
 * \page copyright DekaSite Licensing
 * Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Please view COPYING.frameless and COPYING.smarty for further licensing and
 * distribution terms and agreements.
 */
 
/**
 * \brief The master controller for 99.9% of DekaSite.
 *
 * This is the main controller for DekaSite. It can be similarly thought of as
 * DekaSite's "BaseController". The only page not controlled by this is /events
 * which is controlled by the events controller. This controller handles:
 * - Login
 * - Logout
 * - Loading the controllers in controllers/components
 * - Menu generation
 */
class index extends BaseController
{
    private $view;
    private $extsMdl;
    private $actsMdl;
    private $allowed;
    private $bp;

    public function __construct(&$config, &$args)
    {
        parent::__construct($config, $args);
       
        $tplPath = $this->getTemplate($templates);
        
        $this->view = new SmartyView('/../'.$tplPath);
        $this->view->Renew('index.tpl');
        $this->view->SetTemplate('index.tpl');

        $this->extsMdl = new ExtrasModel($config['MSSQL']['host'], $config['MSSQL']['user'], $config['MSSQL']['password'], $config['MSSQL']['extrasDB']);
        $this->actsMdl = new AccountModel($config['MSSQL']['host'], $config['MSSQL']['user'], $config['MSSQL']['password']);

        $this->bp = $config['site']['basepath'];

        $this->view->SetVar('lMsg', $this->login());
        $this->view->SetVar('templates',$templates);

        if(!isset($_SESSION['auth'])) $this->view->SetVar('login', 1);
            
        $this->genMenus($config, $this->allowed);
        
        $this->view->SetVar('allowed',$this->allowed);
        $this->view->SetVar('bp', $this->bp);

        new dfcounter($this->view, $config);    
        new serveronline($this->view, $config['misc']);

        $this->actions = array('default' => 'pages', 'pages'=>'pages', 'logout' => 'logout');

        if(isset($this->args[1]) && !array_key_exists($this->args[1], $this->actions)) $this->actions[$this->args[1]] = 'pages';
    }
    
    /**
     * \brief Page content handler
     */
    public function pages()
    {
        $page = NULL;
        // Default page handler
        if(!isset($this->args[2]))
        {
            if(isset($this->config['site']['default'])) $page = $this->config['site']['default'];
            else
            {
                $this->view->Draw();
                return;
            }
        }

        $section = false;
        if($page == NULL) $page = $this->args[1].'/'.$this->args[2];

        // Check allow list
        foreach($this->allowed as $key => $pages)
        {
            if(array_key_exists($page,$pages))
            {
                $section = $key;
                break;
            }
        }
        
        $template = $page.'.tpl';
        
        // 404 if template doesn't exist or not in allow list
        if($section === false || !file_exists('view/'.$template))
        {

            $this->view->SetVar('msg', 13);
            $this->view->Draw();
            return;
        }

        // Set content page to display
        $this->view->SetVar('content', $template);
        
        // Set this thisPage variable for content pages to use
        $this->view->SetVar('thisPage', $this->bp.'index/'.$page.'/');

        // Draw if no class exists for the page or is not specified
        if($this->allowed[$section][$page][0] === false || !class_exists($this->allowed[$section][$page][0]))
        {    
            $this->view->Draw();
            return;
        }

        new $this->allowed[$section][$page][0]($this->view, $this->config, $this->args);
    
        $this->view->Draw();
    }
      
    public function getTemplate(&$temps)
    {
        $template = NULL;

        $ftemp = NULL;
        
        if(isset($_GET['tpl'])) $_SESSION['tpl'] = $_GET['tpl'];

        if(isset($_SESSION['tpl'])) $template = $_SESSION['tpl'];

        foreach($this->config['site']['templates'] as $temp)
        {
            $tpl = explode(',',$temp);

            $temps[] = $tpl[0];

            if($tpl[0] == $template) $ftemp = $tpl[1];
        }

        if($ftemp !== NULL) return $ftemp;
        
        unset($_SESSION['tpl']);
        $tpl = explode(',',$this->config['site']['templates'][0]);
        return $tpl[1];
    }

    /**
     * \brief Logout handler
     */
    public function logout()
    {
        $tpl='';
        if(isset($_SESSION['tpl'])) $tpl='?tpl='.$_SESSION['tpl'];
        session_unset();
        session_destroy();
        header('location:'.$this->bp.'index/'.$tpl);
    }

    /**
     * \brief Login handler
     */
    private function login()
    {
        if((empty($_POST['user']) || empty($_POST['pass'])) && isset($_POST['login'])) return 1;
        
        $info = false;
        
        // Check if logged in or attempting to login
        if(isset($_SESSION['auth'])) $info = $this->actsMdl->GetAcct($_SESSION['user_no'], 1, 'user_id, user_no, login_tag');
        elseif(!isset($_SESSION['auth']) && (!isset($_POST['login']) || !isset($_POST['user']) || !isset($_POST['pass']))) return;
        else $info = $this->actsMdl->GetAcct($_POST['user'], 3, 'user_id, user_no, login_tag', $_POST['pass']);
        
        if($info === false)
        {
             $this->extsMdl->sLog('Login failure', $_POST['user'], true, $this->config['MSSQL']['extras']);
            return 2;
        }
        
        // Regen id and display default page after login
        if(isset($_POST['login'])) 
        {
            $this->extsMdl->sLog('Login success', $_POST['user'], false, $this->config['MSSQL']['extras']);
            session_regenerate_id(true);
            unset($this->args[2]);
        }

        // Assign session variables
        $_SESSION['auth'] = 'member';
        if($info['login_tag'] == 'N') $_SESSION['bTag'] = 'N';
        $_SESSION['user_no'] = $info['user_no'];
        $_SESSION['user_id'] = $info['user_id'];

        // Check if extras conetent is enabled
        if ($this->config['MSSQL']['extras'] == false) return;

        $info = false;
    
        // Return extras information for user
        if(isset($_POST['user'])) $info = $this->extsMdl->userExtrasInfo($_POST['user'], 'auth, webName');
        else $info = $this->extsMdl->userExtrasInfo($_SESSION['user_id'], 'auth, webName');
        
        if($info === false || $info['auth'] == NULL) return;
        
        // Assign session extras information
        $_SESSION['auth'] = $info['auth'];
        if($info['webName'] != NULL) $_SESSION['webName'] = $info['webName'];
    }
    
    /**
     * \brief Add allowed links to proper subdivisions
     *
     * \param &$settings
     *      Settings file to parse
     * \param &$allow
     *      Array to add allowed links to
     * \param $auth
     *      Authority to check
     * \param $section
     *      Subdivision to look in
     */
    private function addWhitelist(&$settings, &$allow, $auth, $section)
    {    if(array_key_exists($auth, $settings['whitelist.'.$section]))
        {
            foreach ($settings['whitelist.'.$section][$auth] as $val)
            {
                $wlsv = explode(',',$val);
                if(count($wlsv) < 3)
                {
                    $wlsv[2] = $wlsv[1];
                    $wlsv[1] = false;
                }

                $allow[$section][$wlsv[0]][0] = $wlsv[1];
                $allow[$section][$wlsv[0]][1] = $wlsv[2];
            }
        }
    }
    
    /**
     * \brief Generate links subdivisions
     *
     * \param &$settings
     *      Settings to read
     * \param &$allow
     *      Array to add allowlist
    */
    private function genMenus(&$settings, &$allow)
    {
        $whitelist = array();

        // Get whitelist subdivisions
        foreach (array_keys($settings) as $key)
        {
            if(preg_match('/^whitelist\./', $key)) 
            {
                $key = explode('.', $key);
                $whitelist[] = $key[1];
            }
        }

        $allow = array();

        // Generate allowed pages for guests
        if(!isset($_SESSION['auth']))
        {
            foreach($whitelist as $WL)
            {
                $this->addWhiteList($settings,$allow, 'guest', $WL);
            }
            return;
        }

        $auth = 'member';
        
        // Check if banned
        if(isset($_SESSION['bTag'])) $auth = 'ban';
        
        // Generate allowed pages
        foreach ($whitelist as $WL)
        {
            $this->addWhiteList($settings, $allow, $auth, $WL);
            
            // Check for extras content or if member only
            if($settings['MSSQL']['extras'] == false || $_SESSION['auth'] == $auth) continue;
            
            // Generate extras allowed pages
            if(isset($settings['inherit'][$_SESSION['auth']]))
            {
                foreach($settings['inherit'][$_SESSION['auth']] as $i)
                {
                    $this->addWhiteList($settings,$allow, $i, $WL);    
                }
            }
            $this->addWhiteList($settings,$allow,$_SESSION['auth'], $WL);
        }
    }
}
?>
