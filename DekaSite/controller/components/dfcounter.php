<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class dfcounter
{
    private $args;
    private $view;
    private $dfMdl;
    private $config;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];

        $this->dfMdl = new CharacterModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

        $this->main();
    }

    private function main()
    {
        
        $DFs = $this->dfMdl->GetDFs();
        
        if($DFs === false) return;

        $hour_wars = $DFs[1];    

        foreach($hour_wars as $war)
        {
            $hour_wars[] = $war['sort_cd'];
        }

        $search = array_search(0, $hour_wars);
        
        if ($search !== false)
        {
            $hour_wars[$search] = 24;
        }
        
        sort($hour_wars);
        
        $hour_now = date('G');
        $hour_next = 0;
        
        foreach($hour_wars as $hour_war)
        {
            if($hour_war > $hour_now)
            {
                $hour_next = $hour_war;
                break;
            }
        }

        $this->view->SetVar('wartime', mktime($hour_next, 0, 0) - time());

    }
}
?>
