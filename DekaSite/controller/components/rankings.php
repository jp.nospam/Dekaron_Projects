<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class rankings
{
    private $args;
    private $view;
    private $config;
    private $action;
    private $rksMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];    
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];
        
        $this->rksMdl = new CharacterModel($this->config['MSSQL']['host'],$this->config['MSSQL']['user'],$this->config['MSSQL']['password']);
        
        $ranks = $this->main();
        
        if($ranks === false || count($ranks) == 0)
        {
            $this->view->SetVar('msg', 31);
            return;
        }

        $this->view->SetVar('cols', $ranks);
    }

    private function main()
    {
        if(!isset($this->action[3]) || (isset($this->action[3]) && $this->action[3] != 'PK' && $this->action[3] != 'PVP'))
        {
            $this->view->SetVar('type', 'lvl');
            $ranks = $this->rksMdl->hlvls($this->config['rankings']['exempt'], $this->config['rankings']['amount']);
            
            if($ranks === false)return false;

            if(isset($this->action[3])) $pagination = new Pagination($ranks[0], $this->config['rankings']['amountPage'], (int)$this->action[3], '{page}/');
            else $pagination = new Pagination($ranks[0], $this->config['rankings']['amountPage'], 1, '{page}/');
            
            $limits = $pagination->GetLimits();
                        
            $this->view->SetVar('start', $limits[0]);

            $list = $pagination->GetList();
            
            if(count($list) > 1)$this->view->SetVar('pagination', $list);

            return array_slice($ranks[1], $limits[0], $limits[1], true);


        }

        if($this->action[3] == 'PVP')
        {
            $this->view->SetVar('type', 'pvp');
            $ranks = $this->rksMdl->PVP($this->config['rankings']['exempt'], $this->config['rankings']['amount']);

            if($ranks === false)return false;
            
            if(isset($this->action[4])) $pagination = new Pagination($ranks[0], $this->config['rankings']['amountPage'], (int)$this->action[4], 'PVP/{page}/');
            else $pagination = new Pagination($ranks[0], $this->config['rankings']['amountPage'], 1, 'PVP/{page}/');
            
            $limits = $pagination->GetLimits();
                        
            $this->view->SetVar('start', $limits[0]);

            $list = $pagination->GetList();

            if(count($list) > 1)$this->view->SetVar('pagination', $list);

            return array_slice($ranks[1], $limits[0], $limits[1], true);

        }
        
        if($this->action[3] == 'PK')
        {
            $this->view->SetVar('type', 'pk');
            $ranks = $this->rksMdl->PK($this->config['rankings']['exempt'], $this->config['rankings']['amount']);

            if($ranks === false)return false;
            
            if(isset($this->action[4])) $pagination = new Pagination($ranks[0], $this->config['rankings']['amountPage'], (int)$this->action[4], 'PK/{page}/');
            else $pagination = new Pagination($ranks[0], $this->config['rankings']['amountPage'], 1, 'PK/{page}/');
            
            $limits = $pagination->GetLimits();

            $this->view->SetVar('start', $limits[0]);

            $list = $pagination->GetList();

            if(count($list) > 1)$this->view->SetVar('pagination', $list);

            return array_slice($ranks[1], $limits[0], $limits[1], true);
        }
    }
}
?>
