<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class rebirth 
{
    private $args;
    private $view;
    private $config;
    private $charMdl;
    private $extsMdl;
    
    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];

        if($this->config['MSSQL']['extras'] != true)
        {
            $this->view->SetVar('msg', 14);
            return;
        }

        $this->charMdl = new CharacterModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
        $this->extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']);

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        if(!isset($this->config['rebirth']['rebirth']) || !isset($this->config['rebirth']['location'])) return 25; 
        
        if(!isset($_POST['rebirth']))
        {
            $chars = $this->charMdl->acctchars($_SESSION['user_no']);

            if($chars === false) return 30;
            
            $chars = $this->extsMdl->getAcctRebirth($_SESSION['user_no'], count($this->config['rebirth']['rebirth']));

            if($chars === false) return 30;

            $this->view->SetVar('chars', $chars[1]);
        }
        $rebirths = array();

        foreach($this->config['rebirth']['rebirth'] as $r)
        {
            $rebirths[] = explode(',',$r);
        }
            
        $this->view->SetVar('rebirths', $rebirths);
    
        $locs = array();

        foreach($this->config['rebirth']['location'] as $l)
        {
            $locs[] = explode(',', $l);
        }
        
        $this->view->SetVar('locations', $locs);
        
        if(isset($_POST['rebirth']))
        {
            $msg = false;

            if(isset($_POST['loc']) && isset($_POST['char'])) $msg = $this->rebirth($_POST['char'], $_POST['loc'], $rebirths, $locs);
            
            $chars = $this->extsMdl->getAcctRebirth($_SESSION['user_no'], count($this->config['rebirth']['rebirth']));

            if($chars === false && $msg === false) return 30;

            if($chars !== false) $this->view->SetVar('chars', $chars[1]);

            return $msg;
        }
        elseif (!isset($_POST['rebirth'])) return;
        else return 1;
    }

    private function rebirth($char, $loc, &$rInfo, &$lInfo)
    {
        $actMdl = new AccountModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

        $act = $actMdl->GetAcct($_SESSION['user_no'], 1, 'login_flag');

        if($act['login_flag'] != '0') return 28;

        $charInfo = $this->charMdl->charInfo($char, 1, 'character_no, wLevel, byPCClass', 1, $_SESSION['user_no']);

        if($charInfo === false) return 16;

        $rebirths = $this->extsMdl->getRebirth($charInfo['character_no']);

        if($rebirths === false) $rebirths[0] = 0;

        if($rebirths[0] >= count($this->config['rebirth']['rebirth'])) return 26;

        if($charInfo['wLevel'] < $rInfo[$rebirths[0]][0]) return 29;
        
        $clsInfo = $this->charMdl->charInfo('DEKARON'.$charInfo['byPCClass'].'000001', 0, $info = 'wStr, wCon, wDex, wSpr, wLevel, wStatPoint, wSkillPoint');
        
        if($clsInfo === false) return 24;    
        
        if($rInfo[$rebirths[0]][2] > 0)
        {
            $cash = new CashModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

            $coins = $cash->getCoins($_SESSION['user_no']);

            if($coins === false) return 18;

            if($coins < $rInfo[$rebirths[0]][2]) return 21;

            $cash->updateCoins($_SESSION['user_no'], $rInfo[$rebirths[0]][2], 1);

        }
        
        $loc = (int)$loc;
        
        if($loc < 0 || $loc > (count($this->config['rebirth']['location']) - 1)) $loc = 0;
        
        for($i = 0; $i <= $rebirths[0]; $i++)
        {
            $clsInfo['wStatPoint'] += $rInfo[$i][1];    
        }

        $this->extsMdl->addRebirth($charInfo['character_no']);

        $this->charMdl->rebirthUpdate($clsInfo['wStr'], $clsInfo['wSpr'], $clsInfo['wCon'], $clsInfo['wDex'], $clsInfo['wLevel'], $clsInfo['wStatPoint'], $lInfo[$loc][1], $lInfo[$loc][2], $lInfo[$loc][0], $charInfo['character_no']);

        if(isset($this->config['rebirth'][($rebirths[0] + 1).'.send']))
        {
            foreach($this->config['rebirth'][($rebirths[0] + 1).'.send'] as $x)
            {
                $send = explode(',', $x);
                $this->charMdl->sendMail($charInfo['character_no'], 'Rebirth', 'Rebirth bonus', 'Congratulations on your rebirth', $send[0], $send[1]);
            }
        }
        
        if($this->config['rebirth']['SkillPoint'] == true)
        {
            $this->charMdl->updateSkillPoint($charInfo['character_no'], $clsInfo['wSkillPoint']);
        }

        if($this->config['rebirth']['Skill'] == true)
        {
            $this->charMdl->deleteSkillBar($charInfo['character_no']);
            $this->charMdl->deleteSkills($charInfo['character_no']);
        }

        return 27;
    }
}
?>
