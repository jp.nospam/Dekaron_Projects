<?php
class downloadmanage 
{
    private $args;
    private $view;
    private $config;
    private $action;
    private $extsMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];

        $this->extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']); 

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        if(isset($this->action[3]))
        {
            if($this->action[3] == 'add')
            {
                if(isset($_POST['create']) && isset($_POST['link']) && isset($_POST['name']) && isset($_POST['ver']) && isset($_POST['desc']))
                {
                    $this->extsMdl->addDownload($_POST['link'], $_POST['name'], $_POST['ver'], $_POST['desc']);
                    
                    $dls = $this->extsMdl->GetDownloads();

                    if($dls !== false) $this->view->SetVar('downloads', $dls[1]);    

                    return 81;
                }

                $this->view->SetVar('type', 'add');

                return;
            }

            if($this->action[3] == 'edit' && isset($this->action[4]))
            {
                $this->action[4] = (int)$this->action[4];

                $dl = $this->extsMdl->GetDownload($this->action[4]); 
                
                if($dl === false)
                {
                    $dl = $this->extsMdl->GetDownloads();

                    if($dl !== false) $this->view->SetVar('downloads', $dl[1]);

                    return 82;
                }
                
                if(isset($_POST['delete']))
                {
                    $this->extsMdl->deleteDownload($this->action[4]);

                    $dls = $this->extsMdl->GetDownloads();

                    if($dls !== false) $this->view->SetVar('downloads', $dls[1]);

                    return 83;
                }
                
                if(isset($_POST['edit']) && isset($_POST['link']) && isset($_POST['name']) && isset($_POST['ver']) && isset($_POST['desc']))
                {
                    $this->extsMdl->updateDownload($this->action[4], $_POST['link'], $_POST['name'], $_POST['ver'], $_POST['desc']);

                    $dl['name'] = $_POST['name'];
                    $dl['descr'] = $_POST['desc'];
                    $dl['version'] = $_POST['ver'];
                    $dl['link'] = $_POST['link'];
                }

                $this->view->SetVar('download',$dl);

                $this->view->SetVar('type', 'edit');
                if(isset($_POST['edit'])) return 84;
                else
                return;
            }
        }

        $dls = $this->extsMdl->GetDownloads();

        if($dls === false) return;

        $this->view->SetVar('downloads', $dls[1]);
    }
}
?>
