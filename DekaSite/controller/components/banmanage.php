<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class banmanage 
{
    private $args;
    private $view;
    private $action;
    private $config;
    private $extsMdl;
    private $acctMdl;
    private $charMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {

        $this->acctMdl = new AccountModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
        $this->extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']);
        
        if(isset($this->action[3]) && ($this->action[3] == 'log' || $this->action[3] == 'unban')) return $this->logHandle();

        if(isset($_POST['ban']) && isset($_POST['type']) && isset($_POST['data']) && isset($_POST['reason'])) return $this->banHandle($_POST['data'], $_POST['type'], $_POST['reason']);
    }
    
    private function banHandle($data, $type, $reason)
    {
        if($type == 'char')
        {
            $this->charMdl = new CharacterModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

            $char = $this->charMdl->charInfo($data, 1, 'user_no');

            if($char === false) return 93;
            
            $acct = $this->acctMdl->GetAcct($char['user_no'], 1, 'user_id, login_tag');
            
            if($acct === false) return 100;

            if($acct['login_tag'] == 'N') return 99;

            $data = $acct['user_id'];
        }
        else
        {
           $acct = $this->acctMdl->GetAcct($data, 0, 'login_tag');

           if ($acct === false) return 96;

           if($acct['login_tag'] == 'N') return 99;
        }
        
        if(empty($reason)) $reason = '[none]';
       
        $this->extsMdl->banAcct($data, $reason, $_SESSION['webName']);
        $this->acctMdl->banAcct($data);

        return 101;
    }

    private function logHandle()
    {
        $this->view->SetVar('log', 1);

        if($this->action[3] == 'unban' && isset($this->action[4])) return $this->unbanHandle($this->action[4]);

        if(isset($_POST['stime']) && isset($_POST['search']) && isset($_POST['type']))
        {
            if(empty($_POST['etime'])) $_POST['etime'] = date('M j Y g:i A');
            else $_POST['etime'] = date('M j Y g:i A',strtotime($_POST['etime']));

            $_POST['stime'] = date('M j Y g:i A',strtotime($_POST['stime']));

            if($_POST['type'] == 'ban')
            {
                $_POST['type'] = 0;
                $this->view->SetVar('typeBan', 0);
            }
            else $_POST['type'] = 1;
           
           $logs = $this->extsMdl->getBanLogs($_POST['stime'], $_POST['etime'], $_POST['type']);

            if($logs === false) return 102;

            $this->view->SetVar('logs', $logs[1]);
            $this->view->SetVar('stime', $_POST['stime']);
            $this->view->SetVar('etime', $_POST['etime']);
        }
    }

    private function unbanHandle($userID)
    {
        $acct = $this->acctMdl->GetAcct($userID, 0, 'login_tag');
        
        if($acct === false) return 96;
        
        if($acct['login_tag'] == 'Y') return 103;
        
        $this->extsMdl->unbanAcct($userID, $_SESSION['webName']);
        $this->acctMdl->unbanAcct($userID);

        return 104;
    }
}

?>
