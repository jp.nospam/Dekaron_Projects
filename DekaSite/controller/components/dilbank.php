<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class dilbank
{
    private $args;
    private $view;
    private $config;
    private $action;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];
    
        if($this->config['MSSQL']['extras'] != true)
        {
            $this->view->SetVar('msg', 14);
            return;
        }

        $this->charMdl = new CharacterModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
        $this->extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']);


        $this->view->SetVar('msg',$this->main());
        if(isset($_POST['buy']) || isset($_POST['submit'])) 
        {
            $extInfo = $this->extsMdl->userExtrasInfo($_SESSION['user_no'], 'dil', 1);

            if($extInfo === false) 
            {
                $this->view->SetVar('msg', 100);
            }
            $this->view->SetVar('dil',$extInfo['dil']);

            if(isset($_POST['submit']))
            {
                $chars = $this->charMdl->acctChars($_SESSION['user_no'], 0, 'character_no, character_name, dwMoney');
                
                if($chars === false) 
                {
                    $this->view->SetVar('msg', 15);
                    return;
                }
                $this->view->SetVar('chars', $chars[1]);
            }
        }
    }

    private function main()
    {
        $extInfo = $this->extsMdl->userExtrasInfo($_SESSION['user_no'], 'dil', 1);

        if($extInfo === false) return 100;

        if(!isset($_POST['buy']) && !isset($_POST['submit']))$this->view->SetVar('dil',$extInfo['dil']);

        if($this->config['dilbank']['dilEnabled'] == true) $this->view->SetVar('dilEnabled', 1);
        if($this->config['dilbank']['coinEnabled'] == true) $this->view->SetVar('coinEnabled', 1);

        if(isset($this->action[3]) && $this->action[3] == 'buy' && ($this->config['dilbank']['coinEnabled'] == true || $this->config['dilbank']['dilEnabled'] == true))
        {
            $this->view->SetVar('dRatio', $this->config['dilbank']['price']);
            $this->view->SetVar('type', 1);
            if(isset($_POST['buy']) && isset($_POST['bType']) && isset($_POST['amount'])) return $this->buyHandle($_POST['bType'], $_POST['amount'], $extInfo['dil']);
            return;
        }
        
        if(!isset($_POST['submit']))
        {
            $chars = $this->charMdl->acctChars($_SESSION['user_no'], 0, 'character_no, character_name, dwMoney');

            if($chars === false) return 15;
        
            $this->view->SetVar('chars', $chars[1]);
        }
        if(isset($_POST['submit']) && isset($_POST['type']) && isset($_POST['char']) && isset($_POST['dil'])) return $this->bankHandle($_POST['type'], $_POST['char'], $_POST['dil'], $extInfo['dil']);
        return;
        
    }

    private function bankHandle(&$type, &$char, &$dil, &$bankDil)
    {
        $dil = (int)$dil;
        
        if($dil < 1) return 34;

        $type = (int)$type;

        if($type != 1) $type = 0;

        $bankDil = (int)$bankDil;
        
        if($bankDil < 1 && $type == 1) return 36;

        $actMdl = new AccountModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

        $act = $actMdl->GetAcct($_SESSION['user_no'], 1, 'login_flag');

        if($act['login_flag'] != '0') return 28;

        $charInfo = $this->charMdl->charInfo($char, 1, 'character_no, dwMoney', 1, $_SESSION['user_no']);

        if($charInfo === false) return 16;

        if($type ==  1)
        {
            if($charInfo['dwMoney'] == '1000000000') return 35;

            if(($dil + $charInfo['dwMoney']) > '1000000000') $dil = '1000000000' - $charInfo['dwMoney'];

            if($dil > $bankDil) $dil = $bankDil;

            $this->charMdl->updateDil($charInfo['character_no'], $dil);
            $this->extsMdl->updateDil($_SESSION['user_no'], $dil, 1);
            
            return 38;
        }
        else
        {
            if($charInfo['dwMoney'] == '0') return 37;

            if($charInfo['dwMoney'] < $dil) $dil = $charInfo['dwMoney'];    

            $this->charMdl->updateDil($charInfo['character_no'], $dil, 1);
            $this->extsMdl->updateDil($_SESSION['user_no'], $dil);

            return 39;
        }
    }


    private function buyHandle(&$type, &$amount, &$bankDil)
    {
        $cash = new CashModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
        
        $coins = $cash->getCoins($_SESSION['user_no']);

        if($coins === false) return 18;

        $type = (int)$type;

        if($type != 1) $type = 0;

        if($this->config['dilbank']['coinEnabled'] == false && $type == 1) return 33;

        if($this->config['dilbank']['dilEnabled'] == false && $type == 0) return 32;

        $amount = (int)$amount;

        if($type == 1)
        {
            
            if($bankDil < ($amount * $this->config['dilbank']['price'])) return 42;

            $cash->updateCoins($_SESSION['user_no'], $amount);

            $this->extsMdl->updateDil($_SESSION['user_no'],($amount * $this->config['dilbank']['price']), 1);

            return 43;
        }
        else
        {
            if($coins < $amount) return 40;

            $cash->updateCoins($_SESSION['user_no'], $amount, 1);

            $this->extsMdl->updateDil($_SESSION['user_no'],($amount * $this->config['dilbank']['price']));

            return 41;
        }

    }
}
?>
