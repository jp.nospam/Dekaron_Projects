<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class lookup
{
    private $args;
    private $view;
    private $action;
    private $config;
    private $acctMdl;
    private $charMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];
    
        $this->charMdl = new CharacterModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
        $this->acctMdl = new AccountModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        if(isset($_GET['type']) && isset($_GET['data']))
        {
            if($_GET['type'] == 'account')
            {
                $this->view->SetVar('type', 'account');
                return $this->handleAcct($_GET['data']);
            }

            if($_GET['type'] == 'char')
            {
                $this->view->SetVar('type', 'char');
                return $this->handleChar($_GET['data']);
            }

            if($_GET['type'] == 'ip') 
            {
                $this->view->SetVar('type', 'ip');
                return $this->handleIP($_GET['data']);
            }
        }
    }

    private function handleAcct($data)
    {
        $acct = $this->acctMdl->getAcct($data);

        if($acct === false) return 96;

        $chars = $this->charMdl->acctChars($data, 1);

        if($chars !== false)
        {
            $this->view->SetVar('chars', $chars[1]);
        }

        $IPs = $this->acctMdl->linkIPs($data);
        
        if($IPs === false) return;
        
        $this->view->SetVar('IPs', $IPs[1]);

        $linked = array();

        foreach($IPs[1] as $ip)
        {
            $accts = $this->acctMdl->getAcctLinks($data, $ip['IP']);
            if($accts === false) continue;
            foreach($accts[1] as $acct)
            {
                if(!in_array($acct['user_id'], $linked))
                {
                    $linked[] = $acct['user_id'];
                }
            }
        }

        if(count($linked) > 0) $this->view->SetVar('lAccts', $linked);
        
        $extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']);

        $uInfo = $extsMdl->userExtrasInfo($data, "user_no, exp, dil");

        if($uInfo[1] > 0) $this->view->SetVar('eInfo', $uInfo[1]);
        if($uInfo[2] > 0) $this->view->SetVar('dilInfo', $uInfo[2]);

        $cashMdl = new CashModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

        $cInfo = $cashMdl->getCoins($uInfo[0]);

        if($cInfo > 0) $this->view->SetVar('dInfo', $cInfo);
    }

    private function handleChar($data)
    {
        $char = $this->charMdl->charInfo($data, 1, 'user_no, dwMoney, dwStoreMoney, dwStorageMoney, nHP, nMP, wStr, wDex, wCon, wSpr, wStatPoint, wSkillPoint, wLevel, byPCClass, wPKCount, nShield, dwPVPPoint, wWinRecord, wLoseRecord');

        if($char === false) return 93;

        $acct = $this->acctMdl->GetAcct($char['user_no'], 1);

        if($acct === false) return 100;

        $char['character_name'] = $data;
        $char['user_id'] = $acct['user_id'];

        $this->view->SetVar('char', $char);
    }

    private function handleIP($data)
    {
        $accts = $this->acctMdl->linkAcctsIP($data);

        if($accts === false) return 108;
        
        $linked = array();
        
        foreach($accts[1] as $acct)
        {
            $linked[] = $acct[0];
        }
        
        $this->view->SetVar('lAccts', $linked);
    }
}
?>
