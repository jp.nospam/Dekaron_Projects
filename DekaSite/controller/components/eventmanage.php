<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class eventmanage 
{
    private $args;
    private $view;
    private $config;
    private $action;
    private $extsMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];

        $this->extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']); 

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        if(isset($this->action[3]))
        {
            if($this->action[3] == 'add')
            {
                if(isset($_POST['create']) && isset($_POST['name']) && isset($_POST['sdate']) && isset($_POST['edate']) && isset($_POST['desc']))
                {
                    $edate = strtotime($_POST['edate']);
                    $sdate = strtotime($_POST['sdate']);
                    if($edate < $sdate)
                    {
                        $this->view->SetVar('type', 'add');
                        return 89;
                    }

                    $this->extsMdl->addEvent($_POST['name'], $_SESSION['webName'], date("n/j/o g:i A",$sdate), date("n/j/o g:i A",$edate), $_POST['desc']);
                    
                    $events = $this->extsMdl->GetEvents();

                    if($events !== false) $this->view->SetVar('events', $events[1]);    

                    return 85;
                }

                $this->view->SetVar('type', 'add');

                return;
            }

            if($this->action[3] == 'edit' && isset($this->action[4]))
            {
                $this->action[4] = (int)$this->action[4];

                $event = $this->extsMdl->GetEvent($this->action[4]); 
                
                if($event === false)
                {
                    $event = $this->extsMdl->GetEvents();

                    if($event !== false) $this->view->SetVar('events', $event[1]);

                    return 86;
                }
                
                if(isset($_POST['delete']))
                {
                    $this->extsMdl->deleteEvent($this->action[4]);

                    $events = $this->extsMdl->GetEvents();

                    if($events !== false) $this->view->SetVar('events', $events[1]);

                    return 87;
                }
                
                if(isset($_POST['edit']) && isset($_POST['sdate']) && isset($_POST['name']) && isset($_POST['edate']) && isset($_POST['desc']))
                {
                    $edate = strtotime($_POST['edate']);
                    $sdate = strtotime($_POST['sdate']);
                    if($edate < $sdate)
                    {
                        $this->view->SetVar('type', 'edit');
                        $this->view->SetVar('event',$event);
                        return 89;
                    }
                    
                    $this->extsMdl->updateEvent($this->action[4], $_POST['name'], $_POST['sdate'], $_POST['edate'], $_POST['desc']);

                    $event['eName'] = $_POST['name'];
                    $event['eStart'] = $_POST['sdate'];
                    $event['eEnd'] = $_POST['edate'];
                    $event['eDesc'] = $_POST['desc'];
                }

                $this->view->SetVar('event',$event);

                $this->view->SetVar('type', 'edit');
                if(isset($_POST['edit'])) return 88;
                else
                return;
            }
        }

        $events = $this->extsMdl->GetEvents();

        if($events === false) return;

        $this->view->SetVar('events', $events[1]);
    }
}
?>
