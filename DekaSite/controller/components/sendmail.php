<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class sendmail 
{
    private $args;
    private $view;
    private $action;
    private $config;
    private $charMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        if(isset($_POST['char']) && isset($_POST['from']) && isset($_POST['subject']) && isset($_POST['item']) && isset($_POST['dil']) && isset($_POST['message']) && isset($_POST['send']))
        {
            $this->charMdl = new CharacterModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
        
            $char = $this->charMdl->charInfo($_POST['char'],1, 'character_no');

            if($char === false) return 93;
            
            if(empty($_POST['dil'])) $_POST['dil'] = 0;
            else $_POST['dil'] = (int)$_POST['dil'];

             if(empty($_POST['item'])) $_POST['item'] = 0;
            else $_POST['item'] = (int)$_POST['item'];

            if(empty($_POST['from'])) $_POST['from'] = '[Anonymous]';

            $this->charMdl->sendMail($char['character_no'], $_POST['from'], $_POST['subject'], $_POST['message'], $_POST['item'], $_POST['dil']);

            return 95; 
        }
    }
}
?>
