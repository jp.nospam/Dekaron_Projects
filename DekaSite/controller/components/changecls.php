<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class changecls 
{
    private $args;
    private $view;
    private $config;
    private $charMdl;
    private $extsMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];

        if($this->config['MSSQL']['extras'] != true)
        {
            $this->view->SetVar('msg', 14);
            return;
        }

        $this->charMdl = new CharacterModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
        $this->extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']); 
        
        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        $chars = $this->charMdl->acctChars($_SESSION['user_no']);

        if($chars === false) return 15;
        
        $this->view->SetVar('chars', $chars[1]);

        if(isset($this->config['changecls']['dil'])) $this->view->SetVar('dil', $this->config['changecls']['dil']);
        if(isset($this->config['changecls']['coin'])) $this->view->SetVar('coin', $this->config['changecls']['coin']);
        
        if(isset($_POST['change']) && isset($_POST['char']) && isset($_POST['cls'])) return $this->ChangeCls($_POST['char'], $_POST['cls']);
        elseif(!isset($_POST['change'])) return;
        else return 1;
    }

    private function ChangeCls($char, $cls)
    {    
        $actMdl = new AccountModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
        $act = $actMdl->GetAcct($_SESSION['user_no'], 1, 'login_flag');
        
        if($act['login_flag'] != '0') return 28;

        $charInfo = $this->charMdl->charInfo($char, 1, 'character_no, character_name, dwMoney, wLevel', 1, $_SESSION['user_no']);

        if($charInfo === false) return 16;
        
        $dil = 0;

        if(isset($this->config['changecls']['dil']))
        {
            if($charInfo['dwMoney'] < $this->config['changecls']['dil']) return 17;
            
            $dil = $this->config['changecls']['dil'];
        }

        if($this->charMdl->getInventory($charInfo['character_no']) != false) return 22;
        
        $cls = (int)$cls;

        $clsInfo = $this->charMdl->charInfo('DEKARON'.$cls.'000001', 0, $info = 'wStr, wCon, wDex, wSpr');

        if($clsInfo === false) return 24;

        if(isset($this->config['changecls']['coin']))
        {
            $cash = new CashModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
            
            $coins = $cash->getCoins($_SESSION['user_no']);

            if($coins === false) return 18;

            if($coins < $this->config['changecls']['coin']) return 21;

            $cash->updateCoins($_SESSION['user_no'], $this->config['changecls']['coin'], 1);
        }
        
        $this->charMdl->deleteSkills($charInfo['character_no']);
        $this->charMdl->deleteSkillBar($charInfo['character_no']);

        $stats = ($charInfo['wLevel'] - 1) * 5;
        $sp = ($charInfo['wLevel'] - 1) * 1;

        if($this->config['MSSQL']['extras'] == true)
        {
            $rebirths = $this->extsMdl->getRebirth($charInfo['character_no']);
            
            if($rebirths !== false)
            {
                for($i = 0; $i < $rebirths[0]; $i++)
                {
                    $rArray = explode(',',$this->config['rebirth']['rebirth'][$i]);
                    $stats += $rArray[1];
                    if($this->config['rebirth']['SkillPoint'] == false)
                    {
                        $sp += ($rArray[0] - 1) * 1;
                    }
                }
            }
        }
        $this->charMdl->changeClsUpdate($clsInfo['wStr'], $clsInfo['wSpr'], $clsInfo['wCon'], $clsInfo['wDex'], $stats, $cls, $sp, $dil, $charInfo['character_no']);    
        return 23;
    }
}
?>
