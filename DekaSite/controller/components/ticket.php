<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class ticket 
{
    private $args;
    private $view;
    private $config;
    private $action;
    private $extsMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];
        
        if($this->config['MSSQL']['extras'] != true)
        {
            $this->view->setvar('msg', 14);
            return;
        }

        $this->extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']);

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        if(isset($this->action[3]))
        {
            if($this->action[3] == 'new') return $this->newTicket();

            if($this->action[3] == 'view' && isset($this->action[4])) return $this->viewTicket($this->action[4]);
        }
        
        $tickets = $this->extsMdl->getTickets($_SESSION['user_no']);
        
        if($tickets === false) return 68;

        $this->view->SetVar('tickets',$tickets[1]);
    }

    private function newTicket()
    {
        $this->view->SetVar('type', 'new');
        
        $auth = $_SESSION['auth'];

        if(isset($_SESSION['btag'])) $auth = 'ban';

        if(!isset($this->config['ticket']['type.'.$auth])) return 73;
        
        if(isset($_POST['subt']) && isset($_POST['details']) && isset($_POST['title']) && isset($_POST['cat']))
        {
            $_POST['cat'] == (int)$_POST['cat'];

            if(!array_key_exists($_POST['cat'], $this->config['ticket']['type.'.$auth])) $_POST['cat'] = 0;

            $lPost = $this->extsMdl->lastOpen($_SESSION['user_no']);

            if($lPost !== false)
            {
                if(!isset($this->config['ticket']['newWait.'.$auth])) $this->config['ticket']['newWait.'.$auth] = 30;

                $timeleft = strtotime($lPost.' +'.$this->config['ticket']['newWait.'.$auth].' seconds');
                
                if ($timeleft > strtotime(date('Y-m-d G:i')))
                {
                    $this->view->SetVar('sec', $this->config['ticket']['newWait.'.$auth]);
                    return 70;
                }
            }

            $tid = uniqid(substr(session_id(),0,7));

            $this->extsMdl->addTicket($tid, $this->config['ticket']['type.'.$auth][$_POST['cat']], $_SESSION['user_no'], $_POST['title']);

            $this->extsMdl->replyTicket($tid, $_SESSION['user_no'], $_POST['details']);

            return 74;
        }

        $this->view->SetVar('cats', $this->config['ticket']['type.'.$auth]);
    }

    private function viewTicket($tid)
    {
        $this->view->SetVar('type', 'view');

        $ticket = $this->extsMdl->getTicket($tid, $_SESSION['user_no']);

        if($ticket === false) return 69;
        
        if(isset($_POST['open']) && $ticket[2] == 0)
        {
            $this->extsMdl->updateStatus($tid, 1);
            $ticket[2] = 1;
        }

        if(isset($_POST['close']) && $ticket[2] == 1)
        {
            $this->extsMdl->updateStatus($tid, 0);
            $ticket[2] = 0;
        }
    
        $this->view->SetVar('reply', $ticket[1]);
        $this->view->SetVar('status', $ticket[2]);

        if(isset($_POST['rsub']) && isset($_POST['reply']))
        {
            if($ticket[2] == 0) return 71;

            if($ticket[2] == -1) return 72;
            
            $lPost = $this->extsMdl->lastReply($_SESSION['user_no'], $tid);
    
            if($lPost !== false)
            {
                $auth = $_SESSION['auth'];

                if(isset($_SESSION['btag'])) $auth = 'ban';

                if(!isset($this->config['ticket']['replyWait.'.$auth])) $this->config['ticket']['replyWait.'.$auth] = 30;

                $timeleft = strtotime($lPost.' +'.$this->config['ticket']['replyWait.'.$auth].' seconds');
                
                if ($timeleft > strtotime(date('Y-m-d G:i')))
                {
                    $this->view->SetVar('sec', $this->config['ticket']['replyWait.'.$auth]);
                    return 76;
                }
            }

            $this->extsMdl->replyTicket($tid, $_SESSION['user_no'], $_POST['reply']);
            
            $ticket = $this->extsMdl->getTicket($tid, $_SESSION['user_no']);

            $this->view->UnsetVar('reply');
            $this->view->UnsetVar('status');

            if($ticket === false) return 69;
        }

        $this->view->SetVar('reply', $ticket[1]);
        $this->view->SetVar('status', $ticket[2]);
    }
}
?>
