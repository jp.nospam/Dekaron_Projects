<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class expbank
{
    private $args;
    private $view;
    private $config;
    private $action;
    private $extsMdl;
    private $charMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];

        if($this->config['MSSQL']['extras'] != true)
        {
            $this->view->SetVar('msg', 14);
            return;
        }

        $this->charMdl = new CharacterModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
        $this->extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']);

        $this->view->SetVar('msg',$this->main());
    
        if(isset($_POST['gift']) || isset($_POST['submit']) || isset($_POST['list']))
        {
            $extInfo = $this->extsMdl->userExtrasInfo($_SESSION['user_no'], 'exp', 1);

            if($extInfo === false) 
            {
                $this->view->SetVar('msg', 100);
            }
            $this->view->SetVar('exp',$extInfo['exp']);

            if(isset($_POST['submit']))
            {
                $chars = $this->charMdl->acctChars($_SESSION['user_no'], 0, 'character_no, character_name, dwExp');
                
                if($chars === false) 
                {
                    $this->view->SetVar('msg', 15);
                    return;
                }
                $this->view->SetVar('chars', $chars[1]);
            }
        }
    }

    private function main()
    {
        $extInfo = $this->extsMdl->userExtrasInfo($_SESSION['user_no'], 'exp', 1);

        if($extInfo === false) return 100;

        if(!isset($_POST['submit']) && !isset($_POST['gift']) && !isset($_POST['list']))$this->view->SetVar('exp',$extInfo['exp']);
        
        if($this->config['expbank']['giftEnabled'] == true) $this->view->SetVar('giftEnabled',1);

        if($this->config['expbank']['listEnabled'] == true) $this->view->SetVar('listEnabled',1);

        if(isset($this->action[3]))
        {
            if($this->action[3] == 'gift' && $this->config['expbank']['giftEnabled'] == true)
            {
                $this->view->SetVar('cost',$this->config['expbank']['giftprice']);
                $this->view->SetVar('type', 'gift');
                if(isset($_POST['char']) && isset($_POST['exp']) && isset($_POST['gift']))return $this->giftHandle($_POST['char'], $_POST['exp'], $extInfo['exp']);
                return;
            }
            if($this->config['expbank']['listEnabled'] == true)
            {
                if($this->action[3] == 'list')
                {
                    $this->view->SetVar('cost',$this->config['expbank']['giftprice']);
                    $this->view->SetVar('type','list');
                    if(isset($_POST['exp']) && isset($_POST['coins']) && isset($_POST['list'])) return $this->listHandle($_POST['exp'], $_POST['coins'], $extInfo['exp']);
                    return;
                }
                if($this->action[3] == 'listings')
                {
                    $this->view->SetVar('type','listings');
                    return $this->listingsHandle($extInfo['exp']);
                    return;
                }
            }
        }

        if(!isset($_POST['submit']))
        {
            $chars = $this->charMdl->acctChars($_SESSION['user_no'], 0, 'character_no, character_name, dwExp');

            if($chars === false) return 15;
        
            $this->view->SetVar('chars', $chars[1]);
        }
        if(isset($_POST['submit']) && isset($_POST['type']) && isset($_POST['char']) && isset($_POST['exp'])) return $this->bankHandle($_POST['type'], $_POST['char'], $_POST['exp'], $extInfo['exp']);
        return;
        
    }

    private function bankHandle(&$type, &$char, &$exp, &$bankExp)
    {
        $exp = (int)$exp;
        
        if($exp < 1) return 44;

        $type = (int)$type;

        if($type != 1) $type = 0;

        $bankExp = (int)$bankExp;
        
        if($bankExp < 1 && $type == 1) return 46;

        $actMdl = new AccountModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

        $act = $actMdl->GetAcct($_SESSION['user_no'], 1, 'login_flag');

        if($act['login_flag'] != '0') return 28;

        $charInfo = $this->charMdl->charInfo($char, 1, 'character_no, dwExp', 1, $_SESSION['user_no']);

        if($charInfo === false) return 16;

        if($type ==  1)
        {
            if($charInfo['dwExp'] == '2147483648') return 45;

            if(($exp + $charInfo['dwExp']) > '2147483648') $exp = '2147483648' - $charInfo['dwExp'];

            if($exp > $bankExp) $exp = $bankExp;

            $this->charMdl->updateExp($charInfo['character_no'], $exp);
            $this->extsMdl->updateExp($_SESSION['user_no'], $exp, 1);
            
            return 48;
        }
        else
        {
            if($charInfo['dwExp'] == '0') return 47;

            if($charInfo['dwExp'] < $exp) $exp = $charInfo['dwExp'];    

            $this->charMdl->updateExp($charInfo['character_no'], $exp, 1);
            $this->extsMdl->updateExp($_SESSION['user_no'], $exp);

            return 49;
        }
    }

    private function giftHandle(&$char, &$exp, &$bankExp)
    {
        $exp = (int)$exp;
        $bankExp = (int)$bankExp;

        if($exp > $bankExp) return 50;

        $char = $this->charMdl->charInfo($char, $type = 1, $info = 'user_no');

        if($char === false) return 52;

        if($this->config['expbank']['giftprice'] > 0)
        {
            $cash = new CashModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
            
            $coins = $cash->getCoins($_SESSION['user_no']);

            if($coins === false) return 18;
            
            if($coins < $this->config['expbank']['giftprice']) return 51;

            $cash->updateCoins($_SESSION['user_no'], $this->config['expbank']['giftprice'], 1);
        }    

        $this->extsMdl->updateExp($_SESSION['user_no'], $exp, 1);
        $this->extsMdl->updateExp($char['user_no'], $exp);

        return 53;
    }

    private function listHandle(&$exp, &$coins, &$bankExp)
    {
        $exp = (int)$exp;
        $bankExp = (int)$bankExp;

        if($exp > $bankExp) return 50;

        if($exp < 1) return 44;

        if($coins < $this->config['expbank']['giftprice']) return 54;

        $cash = new CashModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
        
        if($cash->getCoins($_SESSION['user_no']) === false) return 18;
    
        $coins = (int)$coins;

        $this->extsMdl->updateExp($_SESSION['user_no'], $exp, 1);
        $this->extsMdl->addListing($_SESSION['user_no'],$exp, $coins);

        return 55;
    }

    private function listingsHandle(&$bankExp)
    {
        $cash = new CashModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

        $coins = $cash->getCoins($_SESSION['user_no']);
    
        if($coins === false) return 18;
        
        $coins = (int)$coins;

        if(isset($this->action[4]) && $this->action[4] == 'self') 
        {
            $listings = $this->extsMdl->getListings($_SESSION['user_no'], 1);
            $this->view->SetVar('aAction', 'self/delete');
        }
        else
        {
            $listings = $this->extsMdl->getListings($_SESSION['user_no']);
            $this->view->SetVar('aAction', 'buy');
        }
        
        $this->view->SetVar('coins', $coins);

        if($listings === false) return 56;
    
        $this->view->SetVar('listings', $listings[1]);    

        if(isset($this->action[4]) && isset($this->action[5]) && $this->action[4] == 'buy')
        {
            $listing = $this->extsMdl->getListing($this->action[5], $_SESSION['user_no']);
            
            if($listing === false) return 59;

            if($listing['coins'] > $coins) return 57;
        
            if(($bankExp + $listing['exp']) > 9223372036854775807) return 58;

            $oCoins = $cash->getCoins($listing['aid']);

            if(($oCoins + $listing['coins']) > 2147483647) return 62;

            $this->extsMdl->deleteListing($listing['auctionID']);

            $this->extsMdl->updateExp($_SESSION['user_no'], $listing['exp']);

            $cash->updateCoins($_SESSION['user_no'], $listing['coins'], 1);

            $cash->updateCoins($listing['aid'], $listing['coins']);
    
            $coins = $cash->getCoins($_SESSION['user_no']);

            $this->view->SetVar('coins', $coins);

            $listings = $this->extsMdl->getListings($_SESSION['user_no']);
            
            if($listings === false) $this->view->UnsetVar('listings');

            if($listings !== false) 
            {
                $this->view->SetVar('aAction', 'buy');

                $this->view->SetVar('listings', $listings[1]);
            }

            return 61;

        }
        
        if(isset($this->action[4]) && isset($this->action[5]) && isset($this->action[6]) && $this->action[4] == 'self' && $this->action[5] == 'delete')
        {
            $listing = $this->extsMdl->getListing($this->action[6], $_SESSION['user_no'], 1);

            if($listing === false) return 59;

            if(($bankExp + $listing['exp']) > 9223372036854775807) return 63;
            
            $this->extsMdl->deleteListing($listing['auctionID']);

            $this->extsMdl->updateExp($_SESSION['user_no'], $listing['exp']);
            
            $listings = $this->extsMdl->getListings($_SESSION['user_no'], 1);

            if($listings === false) $this->view->UnsetVar('listings');

            if($listings !== false) 
            {
                $this->view->SetVar('aAction', 'self/delete');

                $this->view->SetVar('listings', $listings[1]);
            }
            
            return 60;
        }
    
        return;
    }
}
?>
