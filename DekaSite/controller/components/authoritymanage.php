<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class authoritymanage 
{
    private $args;
    private $view;
    private $action;
    private $config;
    private $extsMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];

        $this->extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']);

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        $filter = NULL;
        
        if(isset($_POST['filter']) && $_POST['filter'] != 'none') 
        {
            $filter = $_POST['filter'];

            $this->view->SetVar('filter', $filter);
         }

        if(isset($_POST['acct']) && isset($_POST['webName']) && isset($_POST['auth']) && isset($_POST['update']))
        {
            $acct = $this->extsMdl->getAuthAcct($_POST['acct']);

            if($acct !== false)$this->extsMdl->authUpdate($_POST['acct'], $_POST['auth'], $_POST['webName']);
            else
            {
                $accts = $this->extsMdl->getAuthAccts($filter);

                if($accts !== false) $this->view->SetVar('accts', $accts[1]);

                return 96;
            }
        }

        $auths = $this->extsMdl->getAuths();

        if($auths !== false) $this->view->SetVar('auths', $auths[1]);

        $accts = $this->extsMdl->getAuthAccts($filter);
        
        if($accts !== false) $this->view->SetVar('accts', $accts[1]);
    }
}
?>
