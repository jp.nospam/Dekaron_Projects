<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class charmod 
{
    private $args;
    private $view;
    private $action;
    private $config;
    private $charMdl;
    private $acctMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];

        $this->charMdl = new CharacterModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);
        $this->acctMdl = new AccountModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        if(isset($_POST['char']) && isset($_POST['search'])) return $this->fetchReturn($_POST['char']);

        if(isset($_POST['update']) && isset($_POST['oChar']) && isset($_POST['char']))
        {
            $char = $this->charMdl->charInfo($_POST['oChar'], 1, 'user_no');
            
            if($char === false) return $this->fetchReturn($_POST['oChar'], 93);

            $charName = false;

            if($_POST['oChar'] != $_POST['char'])
            {
                $charName = $this->charMdl->charInfo($_POST['char'], 1, 'user_no');

                if($charName !== false) return $this->fetchReturn($_POST['oChar'], 97);

                $charName = true;
            }
            
            $acct = $this->acctMdl->GetAcct($char['user_no'], 1, 'login_flag, user_id');

            if($acct === false) return $this->fetchReturn($_POST['oChar'], 100);

            if($acct['login_flag'] != '0') return $this->fetchReturn($_POST['oChar'], 20);
            
            $acctMove = false;

            if($_POST['acct'] != $acct['user_id'])
            {
                $acctMove = $this->acctMdl->GetAcct($_POST['acct'], 0, 'login_flag, user_no');
                
                if($acctMove === false) return $this->fetchReturn($_POST['oChar'], 96);

                if($acctMove['login_flag'] != '0') return $this->fetchReturn($_POST['oChar'], 20);
            }
            
            if($acctMove !== false) $char['user_no'] = $acctMove['user_no'];
            
            if($charName === false) $charName = $_POST['oChar'];
            else $charName = $_POST['char'];

            $this->charMdl->charModUpdate($charName, $char['user_no'], (int)$_POST['dwMoney'], (int)$_POST['dwStoreMoney'], (int)$_POST['dwStorageMoney'], (int)$_POST['nHP'], (int)$_POST['nMP'], (int)$_POST['wStr'], (int)$_POST['wDex'], (int)$_POST['wCon'], (int)$_POST['wSpr'], (int)$_POST['wStatPoint'], (int)$_POST['wSkillPoint'], (int)$_POST['wLevel'], (int)$_POST['byPCClass'], (int)$_POST['wPKCount'], (int)$_POST['nShield'], (int)$_POST['dwPVPPoint'], (int)$_POST['wWinRecord'], (int)$_POST['wLoseRecord'], $_POST['oChar']);
        
            return $this->fetchReturn($charName, 98);
        }
    }

    private function fetchReturn($data, $error = NULL)
    {
            $char = $this->charMdl->charInfo($data, 1, 'user_no, dwMoney, dwStoreMoney, dwStorageMoney, nHP, nMP, wStr, wDex, wCon, wSpr, wStatPoint, wSkillPoint, wLevel, byPCClass, wPKCount, nShield, dwPVPPoint, wWinRecord, wLoseRecord');

            if($char === false) return 93;

            $acct = $this->acctMdl->GetAcct($char['user_no'], 1);

            if($acct === false) return 100;

            $char['user_id'] = $acct['user_id'];

            $char['character_name'] = $data;

            $this->view->SetVar('char', $char);

            return $error;
    }
}
?>
