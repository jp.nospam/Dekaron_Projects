<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class playeronline 
{
    private $args;
    private $view;
    private $action;
    private $charMdl;
    private $config;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];

        $this->charMdl = new CharacterModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']); 

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        $chars = $this->charMdl->getOnline();
        
        if($chars === false) return 91;

        $this->view->SetVar('amount', $chars[0]);

        $fd = @fopen($this->config['misc']['maplist'], "r");
        $maps = array();
        if ($fd !== false)
        {
            fgetcsv($fd, 1024);
            while ($buffer = fgetcsv($fd, 1024))
            {
                $maps[$buffer[0]] = $buffer[1];
            }
            $x = 0;
            foreach($chars[1] as $char)
            {
                if(array_key_exists($char['wmapindex'], $maps))
                {
                    $chars[1][$x]['wmapindex'] = $maps[$char['wmapindex']].' ('.$char['wmapindex'].')';
                }
                $x++;
            }
            fclose ($fd);
        }

        $this->view->SetVar('chars', $chars[1]);
    }
}
?>
