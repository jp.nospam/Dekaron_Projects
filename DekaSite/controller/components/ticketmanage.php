<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class ticketmanage
{
    private $args;
    private $view;
    private $action;
    private $config;
    private $extsMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        if(!isset($this->config['ticket']['manage.'.$_SESSION['auth']])) return 105;
        
        $this->view->SetVar('categories', $this->config['ticket']['manage.'.$_SESSION['auth']]);

        $this->extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']);

        if(isset($_POST['search']) && isset($_POST['category']) && isset($_POST['status']) && isset($_POST['order'])) return $this->searchHandle($_POST['category'], $_POST['status'], $_POST['order']);

        if(isset($this->action[3]) && $this->action[3] == 'view' && isset($this->action[4])) return $this->viewHandle($this->action[4]);
    }

    private function searchHandle($cat, $stat, $order)
    {
       $stat = (int)$stat;
       $this->view->SetVar('status', $stat);

       $order = (int)$order;
       $this->view->SetVar('order', $order);

       $this->view->SetVar('cCat', $cat);

       $tickets = $this->extsMdl->manageGetTickets($cat, $stat, $order);

       if($tickets === false) return 106;

       $this->view->SetVar('tickets', $tickets[1]);

       if($stat == -1) $this->view->SetVar('lock',0);
    }

    private function viewHandle($tid)
    {
        $this->view->SetVar('view', 0);

        $ticket = $this->extsMdl->manageGetTicket($tid);

        if($ticket === false) return 69;

        if(!in_array($ticket[3], $this->config['ticket']['manage.'.$_SESSION['auth']])) return 69;
      
        if(in_array($_SESSION['auth'], $this->config['ticket']['delete']))
        {
            $this->view->SetVar('delete', 0);

            if(isset($_POST['delete']))
            {
                
                $this->view->SetVar('status', $ticket[2]);
                $this->view->SetVar('cCat', $ticket[3]);

                $this->extsMdl->deleteTicket($tid);

               return 107;
            }
        }

        if(isset($_POST['open']))
        {
            $this->extsMdl->updateStatus($tid, 1);
            $ticket[2] = 1;
        }

        if(isset($_POST['lock']))
        {
            $this->extsMdl->updateStatus($tid, -1);
            $ticket[2] = -1;
        }

        if(isset($_POST['close']) || isset($_POST['unlock']))
        {
            $this->extsMdl->updateStatus($tid, 0);
            $ticket[2] = 0;
        }

        if(isset($_POST['reply']) && isset($_POST['rsub']))
        {
            $this->extsMdl->replyTicket($tid, $_SESSION['webName'], $_POST['reply']);
            
            $ticket = $this->extsMdl->manageGetTicket($tid);
            
            if($ticket === false) return 100;
        }
        $this->view->SetVar('reply', $ticket[1]);
        $this->view->SetVar('status', $ticket[2]);
        $this->view->SetVar('cCat', $ticket[3]);
    }
}
?>
