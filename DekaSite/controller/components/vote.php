<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class vote 
{
    private $args;
    private $view;
    private $action;
    private $config;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];

        if($this->config['MSSQL']['extras'] != true)
        {
            $this->view->SetVar('msg', 14);
            return;
        }

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        if(!isset($this->config['vote']['vote'])) return 64;
    
        $links = array();

        foreach ($this->config['vote']['vote'] as $link)
        {
            $links[] = explode(',',$link);
        }
        
        $this->view->SetVar('links', $links);
    
        if(isset($this->action[3]))
        {
            $this->action[3] = (int)$this->action[3];
        
            if($this->action[3] >= count($links) ||  $this->action[3] < 0) return 65;
        
            $cash = new CashModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

            if($cash->getCoins($_SESSION['user_no']) === false) return 18;

            $exts = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']);

            $lVote = $exts->getLastVote($_SESSION['user_id'], $_SERVER['REMOTE_ADDR'], $links[$this->action[3]][2]);

            if($lVote === false)
            {
                $exts->addVote($_SESSION['user_id'], $links[$this->action[3]][2], $_SERVER['REMOTE_ADDR']);
                
                $cash->updateCoins($_SESSION['user_no'], $links[$this->action[3]][0]);

                header('Location: '.$links[$this->action[3]][2]);
            }
            else
            {
                $timeleft = strtotime($lVote['wDate']." +".$links[$this->action[3]][1]." seconds");

                if ($timeleft <= strtotime(date("Y-m-d G:i")))
                {
                    $exts->addVote($_SESSION['user_id'], $links[$this->action[3]][2], $_SERVER['REMOTE_ADDR']);

                    $cash->updateCoins($_SESSION['user_no'], $links[$this->action[3]][0]);

                    header('Location: '.$links[$this->action[3]][2]);
                }
                else
                {
                    $this->view->SetVar('date', date('F jS H:i (\G\M\TO)',$timeleft));
                    return 66;
                }
            }
        }
    }
}
?>
