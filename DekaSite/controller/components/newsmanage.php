<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class newsmanage 
{
    private $args;
    private $view;
    private $config;
    private $action;
    private $extsMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];
        $this->action =& $this->args[2];

        $this->extsMdl = new ExtrasModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password'], $this->config['MSSQL']['extrasDB']); 

        $this->view->SetVar('msg',$this->main());
    }

    private function main()
    {
        if(isset($this->action[3]))
        {
            if($this->action[3] == 'add')
            {
                if(isset($_POST['create']) && isset($_POST['title']) && isset($_POST['content']))
                {
                    $this->extsMdl->addNews($_POST['title'], $_SESSION['webName'], $_POST['content']);
                    
                    $news = $this->extsMdl->GetNews();

                    if($news !== false) $this->view->SetVar('news', $news[1]);    

                    return 77;
                }

                $this->view->SetVar('type', 'add');

                return;
            }

            if($this->action[3] == 'edit' && isset($this->action[4]))
            {
                $this->action[4] = (int)$this->action[4];

                $story = $this->extsMdl->GetNewsStory($this->action[4]); 
                
                if($story === false)
                {
                    $news = $this->extsMdl->GetNews();

                    if($news !== false) $this->view->SetVar('news', $news[1]);

                    return 78;
                }
                
                if(isset($_POST['delete']))
                {
                    $this->extsMdl->deleteNews($this->action[4]);

                    $news = $this->extsMdl->GetNews();

                    if($news !== false) $this->view->SetVar('news', $news[1]);

                    return 79;
                }
                
                if(isset($_POST['edit']) && isset($_POST['title']) && isset($_POST['content']))
                {
                    $this->extsMdl->updateNews($this->action[4], $_POST['title'], $_POST['content']);

                    $story['title'] = $_POST['title'];
                    $story['content'] = $_POST['content'];
                }

                $this->view->SetVar('story',$story);

                $this->view->SetVar('type', 'edit');
                if(isset($_POST['edit'])) return 80;
                else
                return;
            }
        }

        $news = $this->extsMdl->GetNews();

        if($news === false) return;

        $this->view->SetVar('news', $news[1]);
    }
}
?>
