<?php
/*
Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class changepass
{
    private $args;
    private $view;
    private $config;
    private $actMdl;

    function __construct()
    {
        $this->args =& func_get_args();
        $this->view =& $this->args[0];
        $this->config =& $this->args[1];

        $this->actMdl = new AccountModel($this->config['MSSQL']['host'], $this->config['MSSQL']['user'], $this->config['MSSQL']['password']);

        $this->view->SetVar('msg', $this->main());
    }

    private function main()
    {
        if(!isset($_POST['upass'])) return;

        if(empty($_POST['old']) || empty($_POST['pass']) || empty($_POST['pass2'])) return 1;

        if($_POST['pass'] != $_POST['pass2']) return 2;

        if(strlen($_POST['pass']) < 3) return 3;

        $udata = $this->actMdl->GetAcct($_SESSION['user_id'], 0, $info = 'user_pwd');

        if($udata === false) return 4;

        if($udata['user_pwd'] != md5($_POST['old'])) return 5;

        $this->actMdl->ChangePass($_SESSION['user_no'], $_POST['pass']);

        return 6;
    }
}
?>
