countdown_x1500 = 0;
function convert_to_time_x1500(secs_x1500)
{
    secs_x1500 = parseInt(secs_x1500);
    hh_x1500 = secs_x1500 / 3600;
    hh_x1500 = parseInt(hh_x1500);
    mmt_x1500 = secs_x1500 - (hh_x1500 * 3600);
    mm_x1500 = mmt_x1500 / 60;
    mm_x1500 = parseInt(mm_x1500);
    ss_x1500 = mmt_x1500 - (mm_x1500 * 60);
    if (hh_x1500 > 23)
    {
        dd_x1500 = hh_x1500 / 24;
        dd_x1500 = parseInt(dd_x1500);
        hh_x1500 = hh_x1500 - (dd_x1500 * 24);
    }
    else dd_x1500 = 0;
   
    if (ss_x1500 < 10) ss_x1500 = "0"+ss_x1500;
    if (mm_x1500 < 10) mm_x1500 = "0"+mm_x1500;
    if (hh_x1500 < 10) hh_x1500 = "0"+hh_x1500;

    if (dd_x1500 == 0) return (hh_x1500+" hour(s) "+mm_x1500+" minute(s) "+ss_x1500)+" second(s)";
    else
    {
        if (dd_x1500 > 1) return (dd_x1500+" days "+hh_x1500+" hour(s) "+mm_x1500+" minute(s) "+ss_x1500+" second(s)");
        else return (dd_x1500+" day "+hh_x1500+" hour(s) "+mm_x1500+" minute(s) "+ss_x1500+" second(s)");
    }
}

function do_cd_x1500(id, message, progress)
{
    if (countdown_x1500 < 0) document.getElementById(id).innerHTML = message.concat(progress);
    else
    {
        document.getElementById(id).innerHTML = message.concat(convert_to_time_x1500(countdown_x1500));
        setTimeout(function(){do_cd_x1500(id, message, progress)}, 1000);
    }

    countdown_x1500 = countdown_x1500 - 1;
}

function writeDF(time, id, message, progress)
{
    countdown_x1500 = time;
    do_cd_x1500(id, message, progress);
}
