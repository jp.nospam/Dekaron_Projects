<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-gb" xml:lang="en-gb">
    <!--
    Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is furnished to do
    so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
    -->
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="{$bp}media/default.css" />
        <title>DekaSite</title>
        <script type="text/javascript" src="{$bp}media/dfcounter.js"></script>
    </head>
    <body>
        <div id="banner"></div>
        {include file='login.tpl'}
        <ul id="nav">
        <!-- BEGIN MENU GENERATION -->
            {include 'menuGen.tpl' sect='static'  pre='<li>' post='</li>'}
            <li>
                <a href="#">Skin</a>
                <ul>
                    {foreach $templates as $tpl}
                    <li><a href="{$thisPage}?tpl={$tpl}">{$tpl}</a></li>
                    {/foreach}
                </ul>
            </li>
            {include 'menuGen.tpl' sect='acct' preheader='<li><a href="#">My Account</a><ul>' pre='<li>' post='</li>' postheader='</ul></li>'}
            {include 'menuGen.tpl' sect='gm' preheader='<li><a href="#">GM Tools</a><ul>' pre='<li>' post='</li>' postheader='</ul></li>'}
            {include 'menuGen.tpl' sect='admin' preheader='<li><a href="#">Admin Tools</a><ul>' pre='<li>' post='</li>' postheader='</ul></li>'}
            {include 'menuGen.tpl' sect='end' pre='<li>' post='</li>'}
        <!-- END MENU EGENERATION -->
        </ul>
        <div id="content">
        <!-- BEGIN CONTENT GENERATION -->
            {include file='content.tpl'}
        <!-- END CONTENT GENERATION -->
        </div>
        <div id="footer">
            Powered by <a href="https://gitorious.org/thunked-org/frameless">Frameless</a> and <a href="http://www.smarty.net/">Smarty</a>
            {if isset($wartime)}
            <span id="deadfront">
                <script type="text/javascript">writeDF({$wartime},'deadfront', 'Next Deadfront: ' ,'is in progress!');</script>
            </span>
            {/if}
            <span id="serverstatus">{include file='pages/serveronline.tpl'}</span>
        </div>
    </body>
</html>
