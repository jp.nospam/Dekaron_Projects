{if isset($sect) && array_key_exists($sect, $allowed)}
    {if isset($preheader)}{$preheader}{/if}
    {foreach from=$allowed.$sect key=k item=i}
        {if $k == 'pages/bank'}
            {include 'menuGen.tpl' sect=bank  preheader='<li><a href="#">Bank</a><ul>' pre='<li>' post='</li>' postheader='</ul></li>'}
        {else} 
            {if isset($pre)}{$pre}{/if}<a href="{$bp}index/{$k}/">{$i.1}</a>{if isset($post)}{$post}{/if}
        {/if}
    {/foreach}
    {if isset($postheader)}{$postheader}{/if}
{/if}
