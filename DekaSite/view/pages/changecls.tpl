{if isset($chars)}
<form method="post" action="{$thisPage}">
    <div>
        {if isset($dil)}Dil cost: {$dil}<br/>{/if}
        {if isset($coin)}D-Coin cost: {$coin}<br/>{/if}
        <label>Character: </label><select name="char">
            {foreach $chars as $char}
            <option value="{$char.character_name}">{$char.character_name}</option>
        {/foreach}
        </select><br/>
        <label>Class: </label><select name="cls">
            <option value="0">Knight</option>
            <option value="1">Hunter</option>
            <option value="2">Mage</option>
            <option value="3">Summoner</option>
            <option value="4">Segnale</option>
            <option value="5">Bagi</option>
        </select><br/>
        <input type="submit" name="change" value="Change" />
    </div>
</form>
{/if}
