<a href="{$thisPage}">Vault</a>
{if isset($giftEnabled)}<a href="{$thisPage}gift/">Gift</a>{/if}
{if isset($listEnabled)}<a href="{$thisPage}list/">List</a><a href="{$thisPage}listings/">Listings</a>{/if}
<br/>
{if isset($type)}

{if $type == 'gift' && isset($giftEnabled)}
Experience in vault: {$exp}<br/>
Cost: {$cost} coin(s)<br/>
<form action="{$thisPage}" method="post">
    <div>
        <label>Character: </label><input type="text" name="char" /><br/>
        <label>Experience: </label><input type="text" name="exp" /><br/>
        <input type="submit" name="gift" value="Gift" />
    </div>
</form>
{/if}

{if $type == 'list' && isset($listEnabled)}
Experience in vault: {$exp}<br/>
Minimum listing price: {$cost} coin(s)<br/>
<form method="post" action="{$thisPage}">
    <div>
        <label>Experience: </label><input type="text" name="exp" /><br/>
        <label>Coins: </label><input type="text" name="coins" /><br/>
        <input type="submit" name="list" value="List" />
    </div>
</form>
{/if}

{if $type == 'listings' && isset($listEnabled)}
{if isset($coins)}
Your D-Coins: {$coins}<br/>
<a href="{$thisPage}listings/">Listings</a>
<a href="{$thisPage}listings/self/">My Listings</a><br/>
{if isset($listings)}
<table>
    <tr>
        <th>Experience</th>
        <th>Price</th>
    </tr>
    {foreach $listings as $au}
    <tr>
        <td><a href="{$thisPage}listings/{$aAction}/{$au.auctionID}/">{$au.exp}</a></td>
        <td>{$au.coins}</td>
    </tr>
    {/foreach}
</table>
{/if}
{/if}
{/if}
{else}
{if isset($chars)}
Experience in vault: {$exp}
<form method="post" action="{$thisPage}">
    <div>
        <label>Character (exp): </label><select name="char">
            {foreach $chars as $char}
            <option value="{$char.character_name}">{$char.character_name} ({$char.dwExp})</option>
            {/foreach}
        </select><br/>
        <label>Experience: </label><input type="text" name="exp" /><br/>
        <input name="type" type="radio" value="0" checked="checked" />Deposit <input name="type" type="radio" value="1"/>Withdraw<br/>
        <input type="submit" name="submit" value="Submit" />
    </div>
</form>
{/if}
{/if}
