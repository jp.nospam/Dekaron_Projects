{if isset($coinEnabled) ||  isset($dilEnabled)}
<a href="{$thisPage}">Vault</a>
<a href="{$thisPage}buy/">Buy</a><br/>{/if}
{if isset($type)}
<div>
    Banked dil: {$dil}<br/>
    Dil to coin ratio: {$dRatio}:1<br/>
</div>
<form method="post" action="{$thisPage}">
    <div>
        <input type="text" name="amount" />
        <select name="bType">
            {if isset($dilEnabled)}<option value="0">Dil(x{$dRatio})</option>{/if}
            {if isset($coinEnabled)}<option value="1">Coins</option>{/if}
        </select><br/>
        <input type="submit" name="buy" value="Buy" />
    </div>
</form>
{else}
{if isset($chars)}
Dil in vault: {$dil}
<form method="post" action="{$thisPage}">
    <div>
        <label>Character (dil): </label><select name="char">
            {foreach $chars as $char}
            <option value="{$char.character_name}">{$char.character_name} ({$char.dwMoney})</option>
            {/foreach}
        </select><br/>
        Dil: <input type="text" name="dil" /><br/>
        <input name="type" type="radio" value="0" checked="checked" /><label>Deposit </label><input name="type" type="radio" value="1"/><label>Withdraw</label><br/>
        <input type="submit" name="submit" value="Submit" />
    </div>
</form>
{/if}
{/if}
