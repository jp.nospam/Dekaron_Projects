<a href="{$thisPage}">Highest Levels</a>
<a href="{$thisPage}PK/">PKers</a>
<a href="{$thisPage}PVP/">PVPers</a><br/>

{if isset($pagination)}{include 'contentPgs.tpl'}{/if}

{if $type == 'pk' && isset($cols)}
<table>
   <tr>
        <th>Rank</th>
        <th>Name</th>
        <th>Guild</th>
        <th>PK Points</th>
    </tr>
    {counter start=$start print=false}
    {foreach $cols as $col}
    <tr>
        <td>{counter}</td>
        <td>{$col.wChar}</td>
        <td>{$col.guild_name|escape:'html'}</td>
        <td>{$col.wPKCount}</td>
    </tr>
    {/foreach}
</table>
{/if}

{if $type == 'pvp' && isset($cols)}
<table>
    <tr>
        <th>Rank</th>
        <th>Name</th>
        <th>Guild</th>
        <th>Points</th>
        <th>Wins</th>
        <th>Losses</th>
        <th>W/L Ratio</th>
    </tr>
    {counter start=$start print=false}
    {foreach $cols as $col}
    <tr>
        <td>{counter}</td>
        <td>{$col.wChar}</td>
        <td>{$col.guild_name|escape:'html'}</td>
        <td>{$col.dwPVPpoint}</td>
        <td>{$col.wWinRecord}</td>
        <td>{$col.wLoseRecord}</td>
        <td>{if $col.wLoseRecord == '0'}Undefeated!{else}{math equation="round(x,2)" x=$col.ratio format='%.2f'}{/if}</td>
    </tr>
    {/foreach}
</table>
{/if}

{if $type == 'lvl' && isset($cols)}
<table>
   <tr>
        <th>Rank</th>
        <th>Name</th>
        <th>Guild</th>
        <th>Level</th>
    </tr>
    {counter start=$start print=false}
    {foreach $cols as $col}
    <tr>
        <td>{counter}</td>
        <td>{$col.character_name}</td>
        <td>{$col.guild_name|escape:'html'}</td>
        <td>{$col.wLevel}</td>
    </tr>
    {/foreach}
</table>
{/if}

