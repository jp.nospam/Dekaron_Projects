{if isset($cstats)}
<table>
    <tr>
        <th>Character</th>
        <th>Points</th>
        <th>Wins</th>
        <th>Losses</th>
    </tr>
{foreach $cstats as $stat}
    <tr>
        <td>{$stat.character_name}</td>
        <td>{$stat.dwPVPpoint}</td>
        <td>{$stat.wWinRecord}</td>
        <td>{$stat.wLoseRecord}</td>
    </tr>
{/foreach}
</table>
{/if}
