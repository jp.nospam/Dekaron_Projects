<a href="{$thisPage}">My Tickets</a> <a href="{$thisPage}new/">New Ticket</a><br/>
{if isset($type)}

{if $type == 'new'}
{if isset($cats)}
<form method="post">
    Category: <select name="cat">
        {counter start=-1 print=false}
        {foreach $cats as $cat}
        <option value="{counter}">{$cat}</option>
        {/foreach}
    </select>
    <br/>Title: <input type="text" maxlength="20" name="title" />
    <br/>Details:
    <br/><textarea cols="75" rows="10" name="details"></textarea>
    <br/><input type="submit" name="subt" value="Submit Ticket">
</form>
{/if}
{/if}

{if $type == 'view'}
{if isset($reply)}
    {foreach $reply as $post}
    <div>
        {$post.poster}<br/>
        {$post.rdate|date_format:"%b %e %Y %H:%M"}<br/>
        {$post.post|escape:'html'|nl2br}
    </div>
    {/foreach}
    {if $status == 1}
    <form method="post">
        <textarea cols="75" rows="10" name="reply"></textarea>
        <br/><input type="submit" value="Reply" name="rsub"><input type="submit" value="Close" name="close" />
    </form>
    {/if}
    {if $status == 0}
    <form method="post">
        <input type="submit" name="open" value="open" />
    </form>
    {/if}
{/if}
{/if}    
{else}
    {if isset($tickets)}
    <table>
        <tr>
            <th>Title</th>
            <th>Type</th>
            <th>Status</th>
            <th>Last reply</th>
        </tr>
        {foreach $tickets as $ticket}
        <tr>
            <td><a href="{$thisPage}view/{$ticket.tid}/">[{$ticket.title|escape:'html'}]</a></td>
            <td>{$ticket.type}</td>
            {assign 'status' 'open'}
            {if $ticket.status == '0'}{assign 'status' 'closed'}{/if}
            {if $ticket.status == '-1'}{assign 'status' 'locked'}{/if}
            <td>{$status}</td>
            <td>{$ticket.poster}<br/>{$ticket.rdate|date_format:"%b %e %Y %H:%M"}</td>
        </tr>
        {/foreach}
    </table>
    {/if}
{/if}
