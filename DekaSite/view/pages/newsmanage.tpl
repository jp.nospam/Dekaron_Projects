{if isset($type)}

{if $type == 'add'}
<form method="post" action="{$thisPage}add/">
    <div>
        <label>Title: </label><input type="text" name="title" />
        <br/><br/><textarea cols="75" rows="25" name="content" ></textarea>
        <br/><input type="submit" name="create" value="Create" />
    </div>
</form>
{/if}

{if $type == 'edit'}
<form method="post" action="{$thisPage}edit/{$story.sid}/">
    <div>
        <label>Title: </label><input type="text" name="title" value="{$story.title}" />
        <br/><br/><textarea cols="75" rows="25" name="content">{$story.content}</textarea>
        <br/><input type="submit" name="edit" value="Edit" /> <input type="submit" name ="delete" value="Delete" />
    </div>
</form>
{/if}

{else}
<a href="{$thisPage}add/">Add News</a><br/>
{if isset($news)}
<table>
    <tr>
        <th>Title</th>
        <th>Written By</th>
    </tr>
    {foreach $news as $story}
    <tr>
        <td><a href="{$thisPage}edit/{$story.sid}/">[{$story.title}]</a></td>
        <td>{$story.wroteby}</td>
    </tr>
    {/foreach}
</table>
{/if}

{/if}
