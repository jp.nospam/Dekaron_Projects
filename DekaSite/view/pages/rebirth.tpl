{if isset($chars)}

<table>
    <tr>
        <th>Rebirth</th>
        <th>Required level</th>
        <th>Points acquired</th>
        <th>Cost (D-Coins)</th>
    </tr>
    {counter start=0 print=false}
    {foreach $rebirths as $r}
    <tr>
        <td>{counter}</td>
        <td>{$r.0}</td>
        <td>{$r.1}</td>
        <td>{$r.2}</td>
    </tr>
    {/foreach}
</table>

<form method="post">
    Character (rebirths):<select name="char">
        {foreach $chars as $char}
        <option value="{$char.character_name}">{$char.character_name} ({$char.rebirths})</option>
        {/foreach}
    </select><br/>
    Location: <select name="loc">
        {counter start=-1 print=false}
        {foreach $locations as $loc}
        <option value="{counter}">{$loc.3}</option>
        {/foreach}
    </select><br/>
    <input type="submit" name="rebirth" value="Rebirth"/>
</form>

{/if}
