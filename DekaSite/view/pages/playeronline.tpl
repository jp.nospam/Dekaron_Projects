{if isset($chars)}

Players online: {$amount}
<table>
        <tr>
            <th>Character</th>
            <th>Map</th>
            <th>IP</th>
        </tr>
        {foreach $chars as $char}
        <tr>
            <td>{$char.character_name}</td>
            <td>{$char.wmapindex}</td>
            <td>{$char.ip}</td>
        </tr>
        {/foreach}
</table>

{/if}
