{if isset($auths)}
<form method="post" action="{$thisPage}">
    <div>
        <select name="filter">
            <option value="none">None</option>
            {foreach $auths as $auth}
            <option value="{$auth.auth}" {if $auth.auth == $filter}selected="selected"{/if}>{$auth.auth}</option>
            {/foreach}
        </select>
        <input type="submit" value="Filter" />
    </div>
</form>
{/if}
{if isset($accts)}
<table>
    <tr>
        <th>Account</th>
        <th>Authority</th>
        <th>Web Name</th>
    </tr>
    {foreach $accts as $acct}
    <tr>
        <td>{$acct.user_id}</td>
        <td>{$acct.auth}</td>
        <td>{$acct.webName}</td>
    </tr>
    {/foreach}
</table>
{/if}
<form method="post" action="{$thisPage}">
    <div>
        Account: <input type="text" name="acct" /><br/>
        Authority: <input type="text" name="auth" {if isset($filter)}value="{$filter}"{/if} /><br/>
        Web name: <input type="text" name="webName" /><br/>
        <input name="update" type="submit" value="Add/Update"/>
        {if isset($filter)}<input type="hidden" name="filter" value="{$filter}" />{/if}
    </div>
</form>
