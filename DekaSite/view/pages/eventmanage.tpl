{if isset($type)}

{if $type == 'add'}
<form method="post" action="{$thisPage}add/">
    <div>
        <label>Title: </label><input type="text" name="name" /><br/>
        <label>Start date: </label><input type="text" name="sdate" /><br/>
        <label>End date: </label><input type="text" name="edate" /><br/><br/>
        <textarea cols="75" rows="25" name="desc"></textarea><br/>
        <input type="submit" name="create" value="Create" />
    </div>
</form>
{/if}

{if $type == 'edit'}
<form method="post" action="{$thisPage}edit/{$event.eID}/">
    <div>
        Title: <input type="text" name="name" value="{$event.eName}" /><br/>
        <label>Start date: </label><input type="text" name="sdate" value="{$event.eStart|date_format:"%b %e %Y %H:%M"}" /><br/>
        <label>End date: </label><input type="text" name="edate" value="{$event.eEnd|date_format:"%b %e %Y %H:%M"}" /><br/><br/>
        <textarea cols="75" rows="25" name="desc">{$event.eDesc}</textarea><br/>
        <input type="submit" name="edit" value="Edit" /> <input type="submit" name ="delete" value="Delete" />
    </div>
</form>
{/if}

{else}
<a href="{$thisPage}add/">Add Event</a><br/>
{if isset($events)}
<table>
    <tr>
        <th>Event</th>
        <th>Host</th>
        <th>Start Date</th>
        <th>End Date</th>
    </tr>
    {foreach $events as $event}
    <tr>
        <td><a href="{$thisPage}edit/{$event.eID}/">[{$event.eName}]</a></td>
        <td>{$event.eHost}</td>
        <td>{$event.eStart|date_format:"%b %e %Y %H:%M"}</td>
        <td>{$event.eEnd|date_format:"%b %e %Y %H:%M"}</td>
    </tr>
    {/foreach}
</table>
{/if}

{/if}
