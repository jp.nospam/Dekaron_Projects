<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-gb" xml:lang="en-gb">
    <!--
    Copyright (C) 2012 Oishi (https://gitorious.org/~oishi/)

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is furnished to do
    so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
    -->
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <title>DekaSite Events</title>
    </head>
    <body style="text-align:center; background-color:black; color:white;">
        <div>
            Current time: {$smarty.now|date_format:"%b %e %Y %H:%M"}
        </div>
        {if isset($pagination)}
        <div>
            Page: {foreach $pagination as $item}{if not $item.active}<a href="{$bp}events/{$item.link}">{/if}{$item.page}{if not $item.active}</a>{/if}{/foreach}
            <br/>
        </div>
        {/if}
        {if isset($events)}
        {foreach $events as $event}
        <div>
            {$event.eName}<br/>
            Hosted by: {$event.eHost}<br/>
            {$event.eStart|date_format:"%b %e %Y %H:%M"} - {$event.eEnd|date_format:"%b %e %Y %H:%M"}<br/>
            {$event.eDesc|escape:'html'|nl2br}
        </div>
        {/foreach}
        {/if}
        <div>
            {include file='errorCode.tpl'}
        </div>
    </body>
</html>
