#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_icon=..\..\..\Icons\My icon.ico
#AutoIt3Wrapper_outfile=ezdekaron.exe
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_Res_Comment=EZDekaron
#AutoIt3Wrapper_Res_Description=EZDekaron
#AutoIt3Wrapper_Res_Fileversion=0.0.0.16
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <EditConstants.au3>
#include <WindowsConstants.au3>
#Include <Constants.au3>

bootup()
hotkeyset("{F7}", "spam")
hotkeyset("{F8}", "fish")
hotkeyset("{F9}", "autoctrl")
hotkeyset("^{F9}", "autoctrl")
hotkeyset("{F10}", "autospace")
hotkeyset("{F11}", "autoclick")
hotkeyset("{Delete}", "Terminate")
hotkeyset("^{Delete}", "Terminate")

global $toggle = 0
global $ctrlToggle = 0
global $spaceToggle = 0
global $clickToggle = 0
Opt("MouseClickDownDelay", 100)
Opt("SendKeyDelay", 1)
Opt("SendKeyDownDelay", 1)

func bootup()
	#notrayicon
	local $pversion = FileGetVersion("ezdekaron.exe")
	msgbox(0, "EZDekaron", "Written by Oishi. Version: " & $pversion)
	Opt("TrayMenuMode",1)
global $menuExit   = TrayCreateItem("Exit")
traysetstate()
traysettooltip("F7-Spam. F8-AutoFish. F9-AutoCtrl. F10-AutoSpace. F11-AutoClick. Delete-SelfKill")
endfunc

func autospace()
if $ctrltoggle = 0 and $toggle = 0 and $spacetoggle = 0 then 
	$spacetoggle = 1
	TraySetState(4)
elseif $spacetoggle = 1 then
$spacetoggle = 0
TraySetState(8)
endif
while $spacetoggle = 1
	send("{space}")
wend
endfunc

func spam()
if $toggle = 0 then 
	$toggle = 1
	TraySetState(4)
else 
$toggle = 0
TraySetState(8)
endif
while $toggle = 1
winactivate("Dekaron")
	send("{Enter}")
	send("{UP}")
	send("{Enter}")
	send("{Enter}")
	send("{UP}")
	send("{UP}")
	send("{Enter}")
wend
endfunc

func autoclick()
if  $toggle = 0 and $clicktoggle = 0 then 
	$clicktoggle = 1
	TraySetState(4)
	Opt("MouseClickDownDelay", 1)
	Opt("MouseClickDelay", 1)
elseif $clicktoggle = 1 then
$clicktoggle = 0
Opt("MouseClickDownDelay", 100)
Opt("MouseClickDelay", 10)
TraySetState(8)
endif
while $clicktoggle = 1
mouseclick("left")
wend
endfunc

func terminate()
Exit 0
endfunc

func fish()
if $toggle = 0 then 
	$pos = mousegetpos()
	$toggle = 1
	TraySetState(4)
else 
$toggle = 0
TraySetState(8)
endif
while $toggle = 1
$active = winactive("Dekaron")
if $active = 1 then
	send("{l}")
	mouseclick("left", $pos[0], $pos[1])
Else
	winactivate("Dekaron")
endif
wend
endfunc

func autoctrl()
if $ctrltoggle = 0 and $toggle = 0 and $spacetoggle = 0 then 
	$ctrltoggle = 1
	TraySetState(4)
	send("{ctrldown}")
elseif $ctrltoggle = 1 then
$ctrltoggle = 0
TraySetState(8)
send("{ctrlup}")
endif
endfunc

While 1
	 $msg = TrayGetMsg()
    Select
        Case $msg = 0
            ContinueLoop
        Case $msg = $menuexit
            ExitLoop
		EndSelect

WEnd