#include <iostream>
#include <vector>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <fstream>
#include <Direct.h>

using namespace std;

/*
TODO:
1. Finish map rip
    1. Parse .mol for map objects
    ?. Study .map files for texture/maptile

2. Finish skill rip
    1. ???
*/


string texture,map,mObject,mKeepOut,mEffect,mPack,bone,bMesh,bAnim,prop,tPack,script,preload;
vector <string> sAct(7);
int is[5],cs[13];

/*
Function to determine if a file exists

Arguments:
    fName       Absolute path of the file to be checked

Return value:
    True/False      True if it exists. False if it doesn't.
*/
bool fExists(string fName)
{
    FILE *fp = fopen(fName.c_str(),"r");
    if(fp)
    {
        fclose(fp);
        return true;
    }
    else return false;
}

/*
Function to split a string

Arguments:
    str             String to be split
    storage         Stores split up values
    delimiters      Delimiters that the string is split by
*/
void stringSplit(const string& str, vector<string>& storage, const string& delimiters)
{
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    string::size_type pos = str.find_first_of(delimiters, lastPos);
    while (string::npos!=pos || string::npos!=lastPos)
    {
        storage.push_back(str.substr(lastPos, pos - lastPos));
        lastPos=str.find_first_not_of(delimiters, pos);
        if(lastPos-pos > 1)
        {
            for(unsigned int q=0; q<((lastPos-pos)-1); q++)
            {
                storage.push_back("NULL");
            }
        }
        pos = str.find_first_of(delimiters, lastPos);
    }
}

/*
Function to the get the paths from shadow.txt. All variables must be global!

Arguments:
    texture         Path for path_texture
    map             Path for path_map
    mObject         Path for path_mapobject
    mKeepout        Path for path_mapkeepout
    mEffect         Path for path_mapeffect
    mPack           Path for path_mappack
    bone            Path for path_bone
    bMesh           Path for path_bonemesh
    bAnim           Path for path_boneanimation
    prop            Path for path_property
    tPack           Path for path_texturepack
    script          Path for path_script
    preload         path for path_preload
*/
void readShadow(string &texture, string &map, string &mObject, string &mKeepOut, string &mEffect, string &mPack, string &bone, string &bMesh, string &bAnim, string &prop, string &tPack, string &script, string &preload)
{
    string line;
    ifstream sF ("./shadow.txt");
    if (sF.is_open())
    {
        vector<string> store;
        while (sF.good())
        {
            getline (sF,line);
            stringSplit(line, store, "\t");
            if(store[0]=="path_texture")texture=store[2];
            if(store[0]=="path_map")map=store[2];
            if(store[0]=="path_mapobject")mObject=store[2];
            if(store[0]=="path_mapkeepout")mKeepOut=store[2];
            if(store[0]=="path_mapeffect")mEffect=store[2];
            if(store[0]=="path_mappack")mPack=store[2];
            if(store[0]=="path_bone")bone=store[2];
            if(store[0]=="path_bonemesh")bMesh=store[2];
            if(store[0]=="path_boneanimation")bAnim=store[1];
            if(store[0]=="path_property")prop=store[2];
            if(store[0]=="path_texturepack")tPack=store[1];
            if(store[0]=="path_script")script=store[2];
            if(store[0]=="path_preload")preload=store[2];
            store.clear();
        }
        sF.close();
    }
    else
    {
        cout<<"Unable to open ./shadow.txt! DekaRipper will now exit...\nPress enter to continue.\n";
        cin.get();
        exit(EXIT_SUCCESS);
    }

}

/*
Function to read and search a particular column in a CSV file

Arguments:
    fPath       Absolute path of the file to be read
    store       A vector string passed by reference to store the split up strings
    sLine       String passed by reference to hold the last line of text read
    search      Boolean value that decides if it should search for strCmp at the position of
                split[intCmp]. Default is set to False
    intCmp      The location in split to compare strCmp to. This only applies if stop is true. Default
                value is 0
    strCmp      The string to be compared to the string in the position specified in split. This only
                applies if stop is true. Default is set to NULL
*/
void readCSV(string fPath, vector<string> &store, string &sLine, bool search=false, int intCmp=0, string strCmp="")
{
    char str[100000];
    sLine = "";
    bool found = false;
    FILE *fp = fopen(fPath.c_str(), "r");
    while(fgets(str,sizeof(str),fp) != NULL)
    {
        if(search == true)
        {
            strcat(str, "\n");
            stringSplit(str, store, ",");
            if(store[intCmp] == strCmp)
            {
                sLine = str;
                found = true;
                break;
            }
            store.clear();
        }
        else
        {
            printf("\n%s", str);
        }
    }
    fclose(fp);
}


/*
Function to read and search imagetable.txt

Arguments:
    store       A vector string passed by reference to store the split up strings
    sLine       String passed by reference to hold the last line of text read
    intCmp      The location in split to compare strCmp to. This only applies if stop is true. Default
                value is 0
    strCmp      The string to be compared to the string in the position specified in split. This only
                applies if stop is true. Default is set to NULL
    intCmp2     The location in split to compare strCmp2 to. This only applies if stop is true. Default
                value is 0
    strCmp2     The string to be compared to the string in the position specified in split. This only
                applies if stop is true. Default is set to NULL
*/
void readIT(vector<string> &store, string &sLine, int intCmp=0, string strCmp="", int intCmp2=0, string strCmp2="")
{
    char str[100000];
    sLine="";
    bool found = false;
    FILE *fp = fopen((".\\"+script+"ui\\com\\imagetable.txt").c_str(), "r");
    while(fgets(str,sizeof(str),fp) != NULL)
    {
        strcat(str, "\n");
        stringSplit(str, store, ",");
        if(store[intCmp] == strCmp && store[intCmp2] == strCmp2)
        {
            sLine = str;
            found = true;
            break;
        }
        store.clear();
    }
    fclose(fp);
}

/*
Function to read and search skilltable.csv

Arguments:
    store       A vector string passed by reference to store the split up strings
    sLine       String passed by reference to hold the last line of text read
    strCmp      The string to be compared to the string in the position specified in split. This only
                applies if stop is true. Default is set to NULL
*/
void readST(vector<string> &store, string &sLine, string strCmp="")
{
    char str[100000];
    sLine="";
    bool found = false;
    FILE *fp = fopen(".\\share\\skill\\skilltable.csv", "r");
    while(fgets(str,sizeof(str),fp) != NULL)
    {
        strcat(str, "\n");
        stringSplit(str, store, ",");
        for(int n=0; n<41; n+=3)
        {
            if(store[n]==strCmp)
            {
                sLine = store[n]+","+store[n+1]+","+store[n+2];
                store[0]=store[n];
                store[1]=store[n+1];
                store[2]=store[n+2];
                found = true;
                break;
            }
        }
        if(found==true)
        {
            break;
        }
        else
        {
            store.clear();
        }
    }
    fclose(fp);
}


/*
Function to automate xCopy

Arguments:
    src         Source of directory/file
    dest        Destination of directory/file
    opt         Options to be applied
    isDir       Is the file to be copied a fie or directory? True
                if directory. False if file. Default is false
*/
void sysX(string src, string dest, string opt="/E /Q /Y", bool isDir=false)
{
    if (isDir == true)
    {
        system((string("echo D | xcopy ")+src+string(" ")+dest+string(" ")+opt).c_str());
    }
    else
    {
        if(fExists(src))
        {
            system((string("echo F | xcopy ")+src+string(" ")+dest+string(" ")+opt).c_str());
        }
    }
}

/*
Function to read .tpacks to find the textures and copies them to ./Ripped/

Arguments:
    fPath       Path of the .tpack file
*/
void getTPACK(string fPath)
{
    if(fExists(fPath))
    {
        char str[100000];
        string base_P;
        vector<string> store;
        FILE *fp = fopen(fPath.c_str(), "r");
        fgets(str,sizeof(str),fp);
        strcat(str, "\n");
        stringSplit(str, store, "\"");
        base_P=store[1];
        store.clear();
        while(fgets(str,sizeof(str),fp) != NULL)
        {
            strcat(str, "\n");
            stringSplit(str, store, "\"");
            sysX(".\\"+texture+base_P+"\\"+store[1],".\\Ripped\\"+texture+base_P+"\\"+store[1]);
            store.clear();
        }
        fclose(fp);
    }
}

/*
Function to write extracted data to an individual files. Placed in ./Ripped/

Arguments:
    data        Data to be written to the file
    info        Informational wrapper
*/
void cfgWrite(string data, string info)
{
    mkdir("./Ripped/");
    vector<string> iStore;
    ofstream cfg;
    size_t found;
    found=info.find_first_of("\\");
    while (found!=string::npos)
    {
        info[found]='/';
        found=info.find_first_of("\\",found+1);
    }
    stringSplit(info, iStore, "/");
    string file = "./Ripped/"+iStore[(iStore.size()-1)];
    if(!fExists(file))
    {
        cfg.open(file.c_str());
        cout<<file<<" created!\n";
        cfg<< "#BEGIN NOTICE\nThis is an automatically generated file that contains the strings ripped from the configuration file.\nThese strings must be placed in the appropriate files for it to work. Also note, that these are direct\nrips, so you may have to apply your own modifications to get them to work correctly for you.\n#END NOTICE\n\n";
    }
    else
    {
        cfg.open(file.c_str(),ios::out|ios::app);
    }
    data.erase(std::remove(data.begin(), data.end(), '\n'), data.end());
    cfg<<data<<"\n";
    cfg.close();
}

/*
Function to rip all of a map's files based on its index. All files are placed
in .\Ripped\

Arguments:
    i           Map's index value as integer
    str_i       Map's index value as string
*/
void ripMap(int i, string str_i)
{
    if(!fExists("./share/maplist.csv"))
    {
        cout<<"./share/maplist.csv could not be found.\nDekaRipper cannot continue ripping this map.\n";
    }
    else
    {
        cout<<"Searching for map index "<<i<<"...\n";
        vector<string> storage;
        string sL;
        readCSV("./share/maplist.csv", storage, sL, true, 0, str_i);
        if(sL=="")
        {
            cout<<"Map index "<<i<<" not found!\n";
        }
        else
        {
            cout<<storage[1]<<" found.\nPreparing to rip all relevant files...\n";
            sysX(".\\"+map+storage[2], ".\\Ripped\\"+map+storage[2]);
            sysX(".\\"+mObject+storage[3], ".\\Ripped\\"+mObject+storage[3]);
            sysX(".\\"+mEffect+storage[4], ".\\Ripped\\"+mEffect+storage[4]);
            sysX(".\\"+mKeepOut+storage[5], ".\\Ripped\\"+mKeepOut+storage[5]);
            if(storage[6]!="NULL")sysX(".\\share\\"+storage[6], ".\\Ripped\\share\\"+storage[6]);
            sysX(".\\"+mPack+storage[18], ".\\Ripped\\"+mPack+storage[18]);
            if(storage[21]!="NULL")sysX(".\\"+preload+storage[21], ".\\Ripped\\"+preload+storage[21]);
            if(storage[44]!="NULL")sysX(".\\share\\"+storage[44],".\\Ripped\\share\\"+storage[44]);
            if(storage[45]!="NULL")sysX(".\\share\\"+storage[45],".\\Ripped\\share\\"+storage[45]);
            if(storage[46]!="NULL")sysX(".\\share\\"+storage[46],".\\Ripped\\share\\"+storage[46]);
            cfgWrite(sL, "./share/maplist.csv");
            storage.clear();
            readIT(storage, sL,0,"minimap", 1, str_i);
            sysX(".\\"+texture+storage[2], ".\\Ripped\\"+texture+storage[2]);
            cfgWrite(sL, ".\\"+script+"ui\\com\\imagetable.txt");
            storage.clear();
            readIT(storage, sL,0,"loadingmap", 1, str_i);
            sysX(".\\"+texture+storage[2], ".\\Ripped\\"+texture+storage[2]);
            cfgWrite(sL, ".\\"+script+"ui\\com\\imagetable.txt");
            storage.clear();
            readIT(storage, sL,0,"maptitle", 1, str_i);
            sysX(".\\"+texture+storage[2], ".\\Ripped\\"+texture+storage[2]);
            cfgWrite(sL, ".\\"+script+"ui\\com\\imagetable.txt");
        }
    }
}

/*
Function to rip all of an item's files based on its index. All files are
placed in .\Ripped\

Arguments:
    i           Item's index value
    str_i       Item's index value as string
*/
void ripItem(int i, string str_i)
{

    bool itemfound=false;
    bool isCostume=false;
    cout<<"Searching for item index "<<i<<"...\n";
    vector<string> storage;
    string sL;
    string sF;
    if(fExists(".\\"+script+"cos_shape.csv"))
    {
        readCSV(".\\"+script+"cos_shape.csv", storage, sL, true, 0, str_i);
        if(sL!="")
        {
            isCostume=true;
        }
        sL="";
        storage.clear();
    }
    else
    {
        cout<<".\\"+script+"cos_shape.csv was not found!\nDekaRipper will continue searching other files but, results could be misleading.\nPress enter to continue.\n";
        cin.sync();
        cin.get();
    }
    if(fExists("./share/item/itemarmor.csv"))
    {
        readCSV("./share/item/itemarmor.csv", storage, sL, true, 0, str_i);
        if(sL!="")
        {
            itemfound=true;
            sF="./share/item/itemarmor.csv";
        }
    }
    else
    {
        cout<<"./share/item/itemarmor.csv was not found!\nDekaRipper will continue searching other files but, results could be misleading.\nPress enter to continue.\n";
        cin.sync();
        cin.get();
    }
    if(fExists("./share/item/itemweapon.csv"))
    {
        if(itemfound==false)
        {
            storage.clear();
            readCSV("./share/item/itemweapon.csv", storage, sL, true, 0, str_i);
            if(sL!="")
            {
                itemfound=true;
                sF="./share/item/itemweapon.csv";
            }
        }
    }
    else
    {
        cout<<"./share/item/itemweapon.csv was not found!\nDekaRipper will continue searching other files but, results could be misleading.\nPress enter to continue.\n";
        cin.sync();
        cin.get();
    }
    if(fExists("./share/item/itemetc.csv"))
    {
        if(itemfound==false)
        {
            storage.clear();
            readCSV("./share/item/itemetc.csv", storage, sL, true, 0, str_i);
            if(sL!="")
            {
                itemfound=true;
                sF="./share/item/itemetc.csv";
            }
        }
    }
    else
    {
        cout<<"./share/item/itemetc.csv was not found!\nDekaRipper will continue searching other files but, results could be misleading.\nPress enter to continue.\n";
        cin.sync();
        cin.get();
    }
    if(fExists("./share/item/itemcash.csv"))
    {
        if(itemfound==false)
        {
            storage.clear();
            readCSV("./share/item/itemcash.csv", storage, sL, true, 0, str_i);
            if(sL!="")
            {
                itemfound=true;
                sF="./share/item/itemcash.csv";
            }
        }
    }
    else
    {
        cout<<"./share/item/itemcash.csv was not found!\nDekaRipper will continue searching other files but, results could be misleading.\nPress enter to continue.\n";
        cin.sync();
        cin.get();
    }
    if(itemfound!=true)
    {
        cout<<"Item index not found.\n";
    }
    else if(isCostume==false)
    {
        cfgWrite(sL,sF);
        string trimmedINV;
        string trimmedEQU;
        if(fExists(".\\"+script+"item_shape.csv"))
        {
            storage.clear();
            sL="";
            readCSV(".\\"+script+"item_shape.csv", storage, sL, true, 0, str_i);
            if(storage[is[0]]!="NULL")
            {
                sysX(".\\"+bMesh+storage[is[0]], ".\\Ripped\\"+bMesh+storage[is[0]]);
                sysX(".\\"+bMesh+"lod"+storage[is[0]], ".\\Ripped\\"+bMesh+"lod"+storage[is[0]]);
            }
            trimmedINV=storage[1].erase(storage[is[0]].find_last_of("."), 5);
            if(storage[is[1]]!="NULL")
            {
                sysX(".\\"+tPack+storage[is[1]], ".\\Ripped\\"+tPack+storage[is[1]]);
                getTPACK(".\\"+tPack+storage[is[1]]);
                sysX(".\\"+tPack+"lod"+storage[is[1]], ".\\Ripped\\"+tPack+"lod"+storage[is[1]]);
                getTPACK(".\\"+tPack+"lod"+storage[is[1]]);
            }
            if(storage[is[2]]!="NULL")
            {
                sysX(".\\"+bMesh+storage[is[2]], ".\\Ripped\\"+bMesh+storage[is[2]]);
                sysX(".\\"+bMesh+"lod"+storage[is[2]], ".\\Ripped\\"+bMesh+"lod"+storage[is[2]]);
            }

            trimmedEQU=storage[is[2]].erase(storage[is[2]].find_last_of("."), 5);
            if(storage[is[3]]!="NULL")
            {
                sysX(".\\"+tPack+storage[is[3]], ".\\Ripped\\"+tPack+storage[is[3]]);
                getTPACK(".\\"+tPack+storage[is[3]]);
                sysX(".\\"+tPack+"lod"+storage[is[3]], ".\\Ripped\\"+tPack+"lod"+storage[is[3]]);
                getTPACK(".\\"+tPack+"lod"+storage[is[3]]);
            }
            else
            {
                sysX(".\\"+tPack+trimmedINV+".tpack", ".\\Ripped\\"+tPack+trimmedINV+".tpack");
                getTPACK(".\\"+tPack+trimmedINV+".tpack");
                sysX(".\\"+tPack+trimmedEQU+".tpack", ".\\Ripped\\"+tPack+trimmedEQU+".tpack");
                getTPACK(".\\"+tPack+trimmedEQU+".tpack");
                sysX(".\\"+tPack+"lod"+trimmedINV+".tpack", ".\\Ripped\\"+tPack+"lod"+trimmedINV+".tpack");
                getTPACK(".\\"+tPack+"lod"+trimmedINV+".tpack");
                sysX(".\\"+tPack+"lod"+trimmedEQU+".tpack", ".\\Ripped\\"+tPack+"lod"+trimmedEQU+".tpack");
                getTPACK(".\\"+tPack+"lod"+trimmedEQU+".tpack");
            }
            if(storage[is[4]]!="NULL")
            {
                sysX(".\\"+bone+storage[is[4]], ".\\Ripped\\"+bone+storage[is[4]]);
            }
            else
            {
                sysX(".\\"+bone+trimmedEQU+".skel", ".\\Ripped\\"+bone+trimmedEQU+".skel");
            }
            if(storage[is[5]]!="NULL")
            {
                sysX(".\\"+bAnim+storage[is[5]], ".\\Ripped\\"+bAnim+storage[is[5]]);
            }
            else
            {
                sysX(".\\"+bAnim+trimmedEQU+".anim", ".\\Ripped\\"+bAnim+trimmedEQU+".anim");
            }
            cfgWrite(sL,".\\"+script+"item_shape.csv");
        }
        else
        {
            cout<<".\\"+script+"item_shape.csv was not found!\nDekaRipper will continue searching other files but, results could be misleading.";
        }
    }
    else
    {
        cfgWrite(sL,sF);
        storage.clear();
        sL="";
        readCSV(".\\"+script+"cos_shape.csv", storage, sL, true, 0, str_i);
        cfgWrite(sL,".\\"+script+"cos_shape.csv");
        if(storage[cs[0]]!="NULL")sysX(".\\"+bMesh+storage[cs[0]], ".\\Ripped\\"+bMesh+storage[cs[0]]);
        if(storage[cs[1]]!="NULL")
        {
            sysX(".\\"+tPack+storage[cs[1]], ".\\Ripped\\"+tPack+storage[cs[1]]);
            getTPACK(".\\"+tPack+storage[cs[1]]);
        }
        if(storage[cs[2]]!="NULL")sysX(".\\"+bMesh+storage[cs[2]], ".\\Ripped\\"+bMesh+storage[cs[2]]);
        if(storage[cs[3]]!="NULL")
        {
            sysX(".\\"+tPack+storage[cs[3]], ".\\Ripped\\"+tPack+storage[cs[3]]);
            getTPACK(".\\"+tPack+storage[cs[3]]);
        }
        if(storage[cs[4]]!="NULL")sysX(".\\"+bMesh+storage[cs[4]], ".\\Ripped\\"+bMesh+storage[cs[4]]);
        if(storage[cs[5]]!="NULL")
        {
            sysX(".\\"+tPack+storage[cs[5]], ".\\Ripped\\"+tPack+storage[cs[5]]);
            getTPACK(".\\"+tPack+storage[cs[5]]);
        }
        if(storage[cs[6]]!="NULL")sysX(".\\"+bMesh+storage[cs[6]], ".\\Ripped\\"+bMesh+storage[cs[6]]);
        if(storage[cs[7]]!="NULL")
        {
            sysX(".\\"+tPack+storage[cs[7]], ".\\Ripped\\"+tPack+storage[cs[7]]);
            getTPACK(".\\"+tPack+storage[cs[7]]);
        }
        if(storage[cs[8]]!="NULL")sysX(".\\"+bMesh+storage[cs[8]], ".\\Ripped\\"+bMesh+storage[cs[8]]);
        if(storage[cs[9]]!="NULL")
        {
            sysX(".\\"+tPack+storage[cs[9]], ".\\Ripped\\"+tPack+storage[cs[9]]);
            getTPACK(".\\"+tPack+storage[cs[9]]);
        }
        if(storage[cs[10]]!="NULL")sysX(".\\"+bMesh+storage[cs[10]], ".\\Ripped\\"+bMesh+storage[cs[10]]);
        if(storage[cs[11]]!="NULL")
        {
            sysX(".\\"+tPack+storage[cs[11]], ".\\Ripped\\"+tPack+storage[cs[11]]);
            getTPACK(".\\"+tPack+storage[cs[11]]);
        }
        if(storage[cs[12]]!="NULL" && storage[cs[12]]!="0")sysX(".\\"+bMesh+storage[cs[12]], ".\\Ripped\\"+bMesh+storage[cs[12]]);
        if(storage[cs[13]]!="NULL" && storage[cs[13]]!="0")
        {
            sysX(".\\"+tPack+storage[cs[13]], ".\\Ripped\\"+tPack+storage[cs[13]]);
            getTPACK(".\\"+tPack+storage[cs[13]]);
        }
    }
}

/*
Function to rip all of a skill's files based on its index. All files are
placed in .\Ripped\

Arguments:
    i           Skill's index value
    str_i       Skill's index value as string
*/
void ripSkill(int i, string str_i)
{
    cout<<"Searching for skill index "<<i<<"...\n";
    vector<string> storage;
    string sL, sF;
    readST(storage, sL, str_i);
    cfgWrite(sL+"\n", ".\\share\\skill\\skilltable.csv");
    if(storage[2]!="NULL")
    {
        sysX(".\\share\\skill\\"+storage[2], ".\\Ripped\\share\\skill\\"+storage[2]);
        sF=storage[2];
    }
    storage.clear();
    sL="";
    readIT(storage, sL,0,"skill_icon", 1, str_i);
    sysX(".\\"+texture+storage[2], ".\\Ripped\\"+texture+storage[2]);
    cfgWrite(sL, ".\\"+script+"ui\\com\\imagetable.txt");
    storage.clear();
    sL="";
    stringstream blackout;
    blackout << (i+10000);
    readIT(storage, sL,0,"skill_icon", 1, blackout.str());
    sysX(".\\"+texture+storage[2], ".\\Ripped\\"+texture+storage[2]);
    cfgWrite(sL, ".\\"+script+"ui\\com\\imagetable.txt");
}


/*
Function to read DekaRippers settings file, settings.dkr
*/
void loadMain()
{
again:
    if(!fExists("./settings.dkr"))
    {
        cout<<"settings.dkr was not found! Default settings will be generated.\n\n";
        ofstream mF;
        mF.open("./settings.dkr");
        mF<<"#Default settings file\n#Columns are zero index based\n\n#item_shape.csv columns\nis_gameMesh=1\nis_tpack=2\nis_invMesh=4\nis_tpack2=5\nis_skel=6\nis_anim=7\n\n";
        mF<<"#cos_shape.csv columns\ncs_swdMesh=1\ncs_swdTP=2\ncs_arcMesh=4\ncs_arcTP=5\ncs_sorMesh=7\ncs_sorTP=8\ncs_sumMesh=10\ncs_sumTP=11\ncs_segMesh=13\ncs_segTP=14\ncs_bgwMesh=16\ncs_bgwTP=17\ncs_aloMesh=19\ncs_aloTP=20\n\n";
        mF<<"#Skill action files\naloAct=share/action/pc/alo.act.6.csv\nbagiAct=share/action/pc/war.act.5.csv\nsegAct=share/action/pc/hil.act.4.csv\nsumAct=share/action/pc/sum.act.3.csv\nsorAct=share/action/pc/sor.act.2.csv\nhunAct=share/action/pc/arc.act.1.csv\nkniAct=share/action/pc/swd.act.0.csv\n";
        mF.close();
        goto again;
    }
    else
    {
        string line;
        ifstream mF("./settings.dkr");
        if(mF.is_open())
        {
            vector<string> store;
            while(mF.good())
            {
                getline(mF,line);
                if(line[0]!='#' && line[0]!='\0')
                {
                    stringSplit(line,store,"=");
                    if(store[0]=="is_gameMesh")is[0]=atoi(store[1].c_str());
                    if(store[0]=="is_tpack")is[1]=atoi(store[1].c_str());
                    if(store[0]=="is_invMesh")is[2]=atoi(store[1].c_str());
                    if(store[0]=="is_tpack2")is[3]=atoi(store[1].c_str());
                    if(store[0]=="is_skel")is[4]=atoi(store[1].c_str());
                    if(store[0]=="is_anim")is[5]=atoi(store[1].c_str());
                    if(store[0]=="cs_swdMesh")cs[0]=atoi(store[1].c_str());
                    if(store[0]=="cs_swdTP")cs[1]=atoi(store[1].c_str());
                    if(store[0]=="cs_arcMesh")cs[2]=atoi(store[1].c_str());
                    if(store[0]=="cs_arcTP")cs[3]=atoi(store[1].c_str());
                    if(store[0]=="cs_sorMesh")cs[4]=atoi(store[1].c_str());
                    if(store[0]=="cs_sorTP")cs[5]=atoi(store[1].c_str());
                    if(store[0]=="cs_sumMesh")cs[6]=atoi(store[1].c_str());
                    if(store[0]=="cs_sumTP")cs[7]=atoi(store[1].c_str());
                    if(store[0]=="cs_segMesh")cs[8]=atoi(store[1].c_str());
                    if(store[0]=="cs_segTP")cs[9]=atoi(store[1].c_str());
                    if(store[0]=="cs_bgwMesh")cs[10]=atoi(store[1].c_str());
                    if(store[0]=="cs_bgwTP")cs[11]=atoi(store[1].c_str());
                    if(store[0]=="cs_aloMesh")cs[12]=atoi(store[1].c_str());
                    if(store[0]=="cs_aloTP")cs[13]=atoi(store[1].c_str());
                    if(store[0]=="aloAct")sAct[6]=store[1];
                    if(store[0]=="bagiAct")sAct[5]=store[1];
                    if(store[0]=="segAct")sAct[4]=store[1];
                    if(store[0]=="sumAct")sAct[3]=store[1];
                    if(store[0]=="sorAct")sAct[2]=store[1];
                    if(store[0]=="hunAct")sAct[1]=store[1];
                    if(store[0]=="kniAct")sAct[0]=store[1];
                }
                store.clear();
            }
            mF.close();
            for(unsigned int t=0; t<=(sizeof(is)/sizeof(int)); t++)
            {
                if(is[t]==0)cout<<"\n--Warning!--\nA null setting value was detected!\nIt is highly advised that you delete settings.dkr and restart DekaRipper.\n------------\n\n";
            }
            for(unsigned int t=0; t<=(sizeof(cs)/sizeof(int)); t++)
            {
                if(cs[t]==0)cout<<"\n--Warning!--\nA null setting value was detected!\nIt is highly advised that you delete settings.dkr and restart DekaRipper.\n------------\n\n";
            }
        }
    }
}

/*
Function main
*/
int main(int argc, char *argv[])
{
    try
    {
        if (argc > 3 && (atoi(argv[1]) != 1 && atoi(argv[1]) != 3))
        {
            cout<<"Usage: DekaRipper <option> <index>\n\nOptions:\n1 Rip items - supports multiple index parameters\n2 Rip a map\n3 Rip skills - supports multiple index parameters\n";
            return EXIT_SUCCESS;
        }
        cout<<"----------\nDekaRipper\n----------\n";
        loadMain();
        readShadow(texture,map,mObject,mKeepOut,mEffect,mPack,bone,bMesh,bAnim,prop,tPack,script,preload);
again:
        cout<<"[1] Rip an item\n[2] Rip a map - incomplete!\n[3] Rip a skill - incomplete!\n[4] Exit\nWhat would you like to do? ";
        int a;
        string i;
        if ((argc >= 3 && atoi(argv[1]) == 1) || (argc >= 3 && atoi(argv[1]) == 3) || (argc == 2 && atoi(argv[1]) == 4))
        {
            a = atoi(argv[1]);
            cout<<a<<"\n";
        }
        else
        {
            cin>>i;
            a = atoi(i.c_str());
        }
        stringstream ia;
        switch (a)
        {
        case 1:
            if (argc >= 3)
            {
                for(int c=2; c<argc; c++)
                {
                    ia.str(string());
                    a = atoi(argv[c]);
                    ia << a;
                    cout<<"Item index: "<<a<<"\n";
                    ripItem(a, ia.str());
                }
            }
            else
            {
                cout<<"Enter the item's index: ";
                cin>>i;
                a = atoi(i.c_str());
                ia << a;
                ripItem(a, ia.str());
            }
            break;
        case 2:
            cout<<"Enter the map's index: ";
            if (argc==3)
            {
                a = atoi(argv[2]);
                cout<<a<<"\n";
            }
            else
            {
                cin>>i;
                a = atoi(i.c_str());
            }
            ia << a;
            ripMap(a, ia.str());
            break;
        case 3:
            if (argc >= 3)
            {
                for(int c=2; c<argc; c++)
                {
                    ia.str(string());
                    a = atoi(argv[c]);
                    ia << a;
                    cout<<"Skill index: "<<a<<"\n";
                    ripSkill(a, ia.str());
                }
            }
            else
            {
                cout<<"Enter the skill's index: ";
                cin>>i;
                a = atoi(i.c_str());
                ia << a;
                ripSkill(a, ia.str());
            }
            break;
        case 4:
            return EXIT_SUCCESS;
            break;
        default:
            cout<<"Invalid Entry!\n\n";
            goto again;
            break;
        }
        cout<<"---------------------------------------------\nRipping complete!\nAny extracted files are in ./Ripped/\n---------------------------------------------\n";
    }
    catch (int e)
    {
        cout<<"An error occurred. Exception: "<<e<< "\n";
    }
    cout<<"Press enter to continue.";
    cin.sync();
    cin.get();
    return EXIT_SUCCESS;
}
